
module type watchtower =
  sig
	type hour = int
	val zero : hour
	val add : hour -> hour -> hour
	val sub : hour -> hour -> hour
  end

module Watchtower  =
  struct
	type hour = int
	let zero = 12

	let rec add (hour1 : hour) (hour2 : hour) =
		if hour2 = 0 then hour1
		else match hour1 with
			| 12 -> add 1 (hour2 - 1)
			| _ -> add (hour1 + 1) (hour2 - 1)

	let rec sub (hour1 : hour) (hour2 : hour) =
		if hour2 = 0 then hour1
		else match hour1 with
			| 1 -> sub 12 (hour2 - 1)
			| _ -> sub (hour1 - 1) (hour2 - 1)
  end

let () =
	print_int (Watchtower.add 11 1);
	print_char '\n';

	print_int (Watchtower.add 11 3);
	print_char '\n';

	print_int (Watchtower.add 11 Watchtower.zero);
	print_char '\n';

	print_int (Watchtower.sub 1 1);
	print_char '\n';

	print_int (Watchtower.sub 1 3);
	print_char '\n';

	print_int (Watchtower.sub 11 Watchtower.zero);
	print_char '\n'
