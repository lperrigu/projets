
module type INT =
sig
type element
val zero1 : element
val zero2 : element
val mul : element -> element -> element
val add : element -> element -> element
val div : element -> element -> element
val sub : element -> element -> element
end

module INT =
  struct
	type element = int
	let zero1 = 0
	let zero2 = 1

	let mul e1 e2 = e1 * e2
	let add e1 e2 = e1 + e2
	let div e1 e2 = e1 / e2
	let sub e1 e2 = e1 - e2
  end

module type MONOID =
sig
type element
val zero1 : element
val zero2 : element
val mul : element -> element -> element
val add : element -> element -> element
val div : element -> element -> element
val sub : element -> element -> element
end


module FLOAT =
  struct
	type element = float
	let zero1 = 0.
	let zero2 = 1.

	let mul e1 e2 = e1 *. e2
	let add e1 e2 = e1 +. e2
	let div e1 e2 = e1 /. e2
	let sub e1 e2 = e1 -. e2
  end


module type CALC =
  functor (M : MONOID) ->
sig
val add : M.element -> M.element -> M.element
val sub : M.element -> M.element -> M.element
val mul : M.element -> M.element -> M.element
val div : M.element -> M.element -> M.element
val power : M.element -> int -> M.element
val fact : M.element -> M.element

end

module Calc : CALC =
  functor (M : MONOID) ->
	struct
		let add = M.add
		let sub = M.sub
		let mul = M.mul
		let div = M.div
		let rec power elem pow =
			if pow = 0 then M.zero2
			else
				M.mul (power elem (pow - 1)) elem
		let rec fact elem =
			if elem <= M.zero2 then M.zero2
			else
				M.mul elem (fact (M.sub elem M.zero2))
	end

module Calc_int = Calc(INT)
module Calc_float = Calc(FLOAT)

let () =
	print_endline (string_of_int (Calc_int.power 3 3));
	print_endline (string_of_float (Calc_float.power 3.0 3));

	print_endline (string_of_int (Calc_int.mul (Calc_int.add 20 1) 2));
	print_endline (string_of_float (Calc_float.mul (Calc_float.add 20.0 1.0) 2.0));

	print_endline (string_of_int (Calc_int.mul (Calc_int.add 20 1) 2));
	print_endline (string_of_float (Calc_float.mul (Calc_float.add 20.0 1.0) 2.0));

	print_endline (string_of_int (Calc_int.sub 20 1));
	print_endline (string_of_float (Calc_float.sub 20. 1.));

	print_endline (string_of_int (Calc_int.div 20 3));
	print_endline (string_of_float (Calc_float.div 20. 3.));


	print_endline (string_of_int (Calc_int.fact 5));
	print_endline (string_of_float (Calc_float.fact 5.0))
