
module type App =
  sig
	type project = string * string * int
	val zero : project
	val combine : project -> project -> project
	val fail : project -> project
	val success : project -> project
  end


let get_left (left, _, _) = left
let get_mid (_, mid, _) = mid
let get_right (_, _, right) = right

module App =
  struct
	type project = string * string * int
	let (zero : project) = ("", "", 0)

	let combine (project1 : project) (project2 : project) =
		(
		(get_left project1)^(get_left project2),
		(get_mid project1)^(get_mid project2),
		( (get_right project1) + (get_right project2) ) / 2
		)

	let fail (proj : project) = (get_left proj, "failed", 0)
	let success (proj : project) = (get_left proj, "succeed", 80)

  end

let print_proj proj =
	print_endline ("Project "^(get_left proj)^" :");
	print_endline ("Status : "^(get_mid proj));
	print_endline ("Grade : "^(string_of_int(get_right proj)))

let () =
	print_proj (App.combine ("ft_", "foo", 100) ("ls", "bar", 50) );
	print_char '\n';

	print_proj (App.fail ("ls", "", 50) );
	print_char '\n';

	print_proj (App.success ("ls", "", 50) );
	print_char '\n'
