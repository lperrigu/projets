let get_left ( left, _ ) = left
let get_right ( _, right ) = right

let encode liste = match liste with
	| [] -> []
	| head::tail ->
		let rec encode_aux liste liste_paire current_paire = match liste with
			| [] -> liste_paire@[current_paire]
			| head::tail ->
			if get_right current_paire = head then
				encode_aux tail liste_paire
				( get_left current_paire + 1, get_right current_paire)
			else
				encode_aux tail (liste_paire@[current_paire]) (1, head)
	in encode_aux tail [] (1, head)


let rec print_list liste = match liste with
	| [] -> print_char '\n'
	| head::tail ->
		print_int (get_left head);
		print_int (get_right head);
		print_list tail

let sequence n =
	if n < 1 then
		print_char '\n'
	else if n = 1 then
		print_endline "1"
	else
		let rec sequence_rec liste n = match n with
			| 1 -> print_list (encode liste)
			| _ ->
				let rec get_new_liste liste new_liste n = match liste with
					| [] -> new_liste
					| head::tail ->
						if n = 0 then
							get_new_liste liste (new_liste@[(get_left head)]) 1
						else
							get_new_liste tail (new_liste@[(get_right head)]) 0
				in
				sequence_rec ( get_new_liste ( encode liste ) [] 0 ) ( n - 1 )
		in sequence_rec [1] ( n - 1 )

let() =
	sequence 1;

	sequence 2;

	sequence 3;

	sequence 4;

	sequence 5;

	sequence 6;

	sequence 7;

	sequence 8;

	sequence 9;

	sequence 10;

	sequence 11;

	sequence 12;

	sequence 13;

	sequence 14
