type phosphate = string
type deoxyribose = string
type nucleobase = A | T | C | G | None
type nucleotide = { phosphate : phosphate; deoxyribose : deoxyribose; nucleobase: char }

let generate_nucleotide c =
	{
	phosphate = "phosphate";
	deoxyribose = "deoxyribose";
	nucleobase = c }

let print_nucleotide n =
	print_endline n.phosphate;
	print_endline n.deoxyribose;
	print_char n.nucleobase;
	print_char '\n'

type helix = nucleotide list

let generate_random_nucleotide n = match n with
	| 0 -> generate_nucleotide 'A'
	| 1 -> generate_nucleotide 'T'
	| 2 -> generate_nucleotide 'C'
	| _ -> generate_nucleotide 'G'

let generate_helix n =
	let rec loop helix n = match n with
		| 0 -> helix
		| _ ->
		loop ( (generate_random_nucleotide(Random.int 4) )::helix ) ( n - 1 )
	in loop [] n

let reverse l =
        let rec reverse_aux l acc = match l with
                | [] -> acc
                | head::tail -> reverse_aux tail (head::acc)
        in reverse_aux l []

let helix_to_string helix =
	let rec loop helix str = match helix with
		| [] -> str
		| head::tail ->
			if head.nucleobase = 'A' then
				loop tail str^"A"
			else if head.nucleobase = 'T' then
				loop tail str^"T"
			else if head.nucleobase = 'C' then
				loop tail str^"C"
			else
				loop tail str^"G"
	in loop (reverse helix) ""


let rec print_helix helix = match helix with
	| [] -> print_char '\n'
	| head::tail ->
		print_nucleotide head;
		print_helix tail

let complementary_helix helix =
	let rec loop helix new_helix = match helix with
		| [] -> new_helix
		| head::tail ->
			if head.nucleobase = 'A' then
(*				loop tail new_helix@[(generate_nucleotide 'T')]*)
				loop tail [(generate_nucleotide 'T')]@new_helix
			else if head.nucleobase = 'T' then
(*				loop tail new_helix@[(generate_nucleotide 'A')]*)
				loop tail [(generate_nucleotide 'A')]@new_helix
			else if head.nucleobase = 'C' then
(*				loop tail new_helix@[(generate_nucleotide 'G')]*)
				loop tail [(generate_nucleotide 'G')]@new_helix
			else
(*				loop tail new_helix@[(generate_nucleotide 'C')]*)
				loop tail [(generate_nucleotide 'C')]@new_helix
	in loop (reverse helix) []

let test_helix helix =
	print_helix ( helix );
	print_endline ( helix_to_string ( helix ));
	print_helix ( complementary_helix helix )
(*	print_endline ( helix_to_string ( complementary_helix helix ))*)

let () =
	Random.self_init();
	test_helix( generate_helix 6 )
