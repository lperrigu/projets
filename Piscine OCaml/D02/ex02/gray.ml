let rec concat l1 l2 = match l1 with
	| [] -> l2
	| head::tail -> head::( concat tail l2 )

let reverse l =
	let rec reverse_aux l acc = match l with
		| [] -> acc
		| head::tail -> reverse_aux tail (head::acc)
	in reverse_aux l []

let rec print_listString l = match l with
	| [] -> print_char '\n'
	| head::tail ->
	print_string head;
	print_char ' ';
	print_listString tail

let rec compute_liste1 liste new_liste = match liste with
	| [] -> new_liste
	| head::tail ->
	compute_liste1 tail ( concat ["0"^head] new_liste )

let rec compute_liste2 liste new_liste = match liste with
	| [] -> new_liste
	| head::tail ->
	if String.get head 0 = '1' then
		compute_liste2 tail ( concat
		["10"^(String.sub head 1 (String.length head - 1))] new_liste )
	else
		compute_liste2 tail ( concat
		["11"^(String.sub head 1 (String.length head - 1))] new_liste )


let gray n =
	if n <= 0 then
		print_char '\n'
	else
		let rec gray_aux n liste = match n with
			| 0 -> print_listString liste
			| _ ->
			gray_aux ( n - 1 )
			( concat (reverse(compute_liste1 liste [])) (reverse(compute_liste2 liste [])) )
		in
		gray_aux (n - 1) ["0"; "1"]

let() =
	gray 1;
	print_char '\n';

	gray 2;
	print_char '\n';

	gray 3;
	print_char '\n';

	gray 4;
	print_char '\n';

	gray 5
