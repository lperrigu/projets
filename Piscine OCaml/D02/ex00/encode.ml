let get_left ( left, _ ) = left
let get_right ( _, right ) = right

let encode liste = match liste with
	| [] -> []
	| head::tail ->
		let rec encode_aux liste liste_paire current_paire = match liste with
			| [] -> liste_paire@[current_paire]
			| head::tail ->
			if get_right current_paire = head then
				encode_aux tail liste_paire
				( get_left current_paire + 1, get_right current_paire)
			else
				encode_aux tail (liste_paire@[current_paire]) (1, head)
	in encode_aux tail [] (1, head)


let rec print_list liste print = match liste with
	| [] -> print_char '\n'
	| head::tail ->
		print_int (get_left head);
		print_char ' ';
		print (get_right head);
		print_char ';';
		print_list tail print

let() =
	let l = ['a'; 'a'; 'b'; 'b'; 'b'; 'a'; 'b'] in
	print_list (encode l) print_char;

	let n = [8; 8; 4; 2; 1; 1; 1; 1; 1; 1; 46; 46] in
	print_list (encode n) print_int;

	let m = ["qw"; "qw"; "ert"; "yuiop"; "yuiop"; "yuiop"; "yuiop"] in
	print_list (encode m) print_string
