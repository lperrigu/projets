let crossover liste1 liste2 =
	let  rec crossover_aux l1 l2 res = match l1 with
		| [] -> res
		| head::tail ->
			let rec crossover_aux2 l2 elem = match l2 with
				| [] -> false
				| head::tail ->
					if head = elem then
						true
					else
						crossover_aux2 tail elem
			in
			if crossover_aux2 l2 head then
				crossover_aux tail l2 (res @ [head])
			else
				crossover_aux tail l2 res
	in crossover_aux liste1 liste2 []

let  rec print_mylist print liste = match liste with
	| [] -> print_char '\n'
	| head::tail ->
		print head;
		print_string " ";
		print_mylist print tail


let () =
	let l1 = [1;2;3;4;5;6;7;8;9] in
	let l2 = [11;22;34;2;44;25;66;6;74;8;9] in
	print_mylist print_int (crossover l1 l2);

	let l1 = ['1';'2';'3';'4';'5';'6';'7';'8';'9'] in
	let l2 = ['f';'s';'w';'2';'4';'5';'6';'l'] in
	print_mylist print_char (crossover l1 l2)
