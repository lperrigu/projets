type phosphate = string
type deoxyribose = string
type nucleobase = A | T | C | G | None
type nucleotide = { phosphate : phosphate; deoxyribose : deoxyribose; nucleobase: char }

let generate_nucleotide c =
	{
	phosphate = "phosphate";
	deoxyribose = "deoxyribose";
	nucleobase = c }

let print_nucleotide n =
	print_endline n.phosphate;
	print_endline n.deoxyribose;
	print_char n.nucleobase;
	print_char '\n'

let () =
	print_nucleotide ( generate_nucleotide 'T' )
