class sodium =
  object (self)
	inherit Atom.atom as super
	method name = "Sodium"
	method symbol = "Na"
	method atomic_number = 11
  end
