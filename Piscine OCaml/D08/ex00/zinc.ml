class zinc =
  object (self)
	inherit Atom.atom as super
	method name = "Zinc"
	method symbol = "zn"
	method atomic_number = 30
  end
