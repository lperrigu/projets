class virtual atom =
  object (self)
	method virtual name: string
	method virtual symbol: string
	method virtual atomic_number: int
	method to_string = (self#name)^": Symbol "^(self#symbol)^", atomic number "^
		(string_of_int(self#atomic_number))
  end
