class oxygen =
  object (self)
	inherit Atom.atom as super
	method name = "Oxygen"
	method symbol = "O"
	method atomic_number = 8
  end
