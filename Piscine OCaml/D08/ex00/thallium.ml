class thallium =
  object (self)
	inherit Atom.atom as super
	method name = "Thallium"
	method symbol = "Tl"
	method atomic_number = 81
  end
