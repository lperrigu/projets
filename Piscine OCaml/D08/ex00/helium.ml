class helium =
  object (self)
	inherit Atom.atom as super
	method name = "Helium"
	method symbol = "He"
	method atomic_number = 2
  end
