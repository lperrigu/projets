let () =
	let h = new Hydrogen.hydrogen in
	print_endline h#to_string;

	let he = new Helium.helium in
	print_endline he#to_string;

	let fe = new Iron.iron in
	print_endline fe#to_string;

	let o = new Oxygen.oxygen in
	print_endline o#to_string;

	let c = new Carbon.carbon in
	print_endline c#to_string;

	let n = new Nitrogen.nitrogen in
	print_endline n#to_string;

	let b = new Boron.boron in
	print_endline b#to_string;

	let z = new Zinc.zinc in
	print_endline z#to_string;

	let r = new Radon.radon in
	print_endline r#to_string;

	let t = new Thallium.thallium in
	print_endline t#to_string;

	let na = new Sodium.sodium in
	print_endline na#to_string
