class carbon =
  object (self)
	inherit Atom.atom as super
	method name = "Carbon"
	method symbol = "C"
	method atomic_number = 6
  end
