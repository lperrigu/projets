class hydrogen =
  object (self)
	inherit Atom.atom as super
	method name = "Hydrogen"
	method symbol = "H"
	method atomic_number = 1
  end
