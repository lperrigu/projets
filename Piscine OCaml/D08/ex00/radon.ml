class radon =
  object (self)
	inherit Atom.atom as super
	method name = "Radon"
	method symbol = "Rn"
	method atomic_number = 86
  end
