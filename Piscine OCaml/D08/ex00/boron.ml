class boron =
  object (self)
	inherit Atom.atom as super
	method name = "Boron"
	method symbol = "B"
	method atomic_number = 5
  end
