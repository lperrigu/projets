let () =
	let h = new Hydrogen.hydrogen in
	let o = new Oxygen.oxygen in
	let w = new Water.water "Water" [h;h;o] in
	print_endline w#to_string;

	let c = new Carbon.carbon in
	let o = new Oxygen.oxygen in
	let cd = new Carbondioxyde.carbondioxyde "Carbon dioxyde" [o;c;o] in
	print_endline cd#to_string;

	let o = new Oxygen.oxygen in
	let c = new Carbon.carbon in
	let h = new Hydrogen.hydrogen in
	let n = new Nitrogen.nitrogen in
	let e = new Ecstasy.ecstasy "Ecstasy" [n;o;c;c;c;c;c;c;c;c;c;c;c;h;h;h;h;h;h;h;h;h;h;h;h;h;h;h;h;o] in
	print_endline e#to_string;

	let h = new Hydrogen.hydrogen in
	let c = new Carbon.carbon in
	let o = new Oxygen.oxygen in
	let e = new Ethanol.ethanol "Ethanol" [c;c;h;h;h;h;h;h;o] in
	print_endline e#to_string;

	let c = new Carbon.carbon in
	let h = new Hydrogen.hydrogen in
	let e = new Ethylene.ethylene "Ethylene" [h;c;h;c;h;h] in
	print_endline e#to_string
