class iron =
  object (self)
	inherit Atom.atom as super
	method name = "Iron"
	method symbol = "Fe"
	method atomic_number = 26
  end
