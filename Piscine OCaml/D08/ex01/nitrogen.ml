class nitrogen =
  object (self)
	inherit Atom.atom as super
	method name = "Nitrogen"
	method symbol = "N"
	method atomic_number = 7
  end
