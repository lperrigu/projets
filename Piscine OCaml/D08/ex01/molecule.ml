let get_left ( left, _ ) = left
let get_right ( _, right ) = right

let compare_hill a1 a2 =
	if a1#symbol = a2#symbol then
		0
	else if a1#symbol = "C" then
		(-1)
	else if a2#symbol = "C" then
		1
	else if a1#symbol = "H" then
		(-1)
	else if a2#symbol = "H" then
		1
	else if a1#symbol < a2#symbol then
		(-1)
	else
		1

let encode liste = match liste with
	| [] -> []
	| head::tail ->
		let rec encode_aux liste liste_paire current_paire = match liste with
		| [] -> liste_paire@[current_paire]
		| head::tail ->
			if get_right current_paire = head#symbol then
				encode_aux tail liste_paire
				( get_left current_paire + 1, get_right current_paire)
			else
				encode_aux tail (liste_paire@[current_paire]) (1, head#symbol)
		in encode_aux tail [] (1, head#symbol)

let get_string liste =
	let rec loop liste str = match liste with
		| [] -> str
		| head::tail ->
			if get_left head <> 1 then
				loop tail str^(get_right head)^(string_of_int (get_left head))
			else
				loop tail str^(get_right head)
	in loop liste ""

class virtual molecule (n : string) (m : Atom.atom list) =
  object (self)
	method virtual name: string
	method virtual equals: molecule -> bool
	method formula = get_string (encode (List.rev(List.sort compare_hill m)))
	method to_string = (self#name)^" : Formula "^(self#formula)
  end
