class ethylene n m =
  object (self)
	inherit Molecule.molecule n m as super
	method name = n
	method equals mol =
		if self#name = mol#name && self#formula = mol#formula then
			true
		else
			false
  end
