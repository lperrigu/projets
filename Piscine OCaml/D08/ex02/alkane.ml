class alkane n =
  object (self)
	inherit Molecule.molecule "" [] as super
	method name = match n with
		| 1 -> "Methane"
		| 2 -> "Ethane"
		| 3 -> "Propane"
		| 4 -> "Butane"
		| 5 -> "Pentane"
		| 6 -> "Hexane"
		| 7 -> "Heptane"
		| 8 -> "Octane"
		| 9 -> "Nonane"
		| 10 -> "Decane"
		| 11 -> "Undecane"
		| 12 -> "Dodecane"
		| _ -> "Error"
	method formula =
		if n = 1 then
			"CH4"
		else
			"C"^(string_of_int n)^"H"^(string_of_int (2 * n + 2))
	method equals mol =
		if self#name = mol#name && self#formula = mol#formula then
			true
		else
			false
  end
