module type FIXED = sig
  type t
  val of_float : float -> t
  val of_int : int -> t
  val to_float : t -> float
  val to_int : t -> int
  val to_string : t -> string
  val zero : t
  val one : t
  val succ : t -> t
  val pred : t -> t
  val min : t -> t -> t
  val max : t -> t -> t
  val gth : t -> t -> bool
  val lth : t -> t -> bool
  val gte : t -> t -> bool
  val lte : t -> t -> bool
  val eqp : t -> t -> bool (** physical equality *)
  val eqs : t -> t -> bool (** structural equality *)
  val add : t -> t -> t
  val sub : t -> t -> t
  val mul : t -> t -> t
  val div : t -> t -> t
  val foreach : t -> t -> (t -> unit) -> unit
end


module type MAKETYPE = sig
	val bits : int
end

module type MAKE =
	functor (MakeType : MAKETYPE) -> FIXED

module Make = functor (MakeType : MAKETYPE) ->
  struct
        type t = int

		let of_float f =
			if mod_float (f /. (1. /. float_of_int(1 lsl MakeType.bits))) 1. < 0.5 then
				int_of_float(f /. (1. /. float_of_int(1 lsl MakeType.bits)))
			else
				int_of_float(f /. (1. /. float_of_int(1 lsl MakeType.bits))) + 1

		let of_int i =
			if mod_float ((float_of_int i) /. (1. /. float_of_int(1 lsl MakeType.bits))) 1. < 0.5 then
				int_of_float((float_of_int i) /. (1. /. float_of_int(1 lsl MakeType.bits)))
			else
				int_of_float((float_of_int i) /. (1. /. float_of_int(1 lsl MakeType.bits))) + 1

		let to_float f =
			(float_of_int f) *. (1. /. float_of_int(1 lsl MakeType.bits))
		let to_string f =
			string_of_float((float_of_int f) *. (1. /. float_of_int(1 lsl MakeType.bits)))
		let to_int f =
			int_of_float((float_of_int f) *. (1. /. float_of_int(1 lsl MakeType.bits)))

		let zero = 0
		let one = int_of_float(1. /. (1. /. float_of_int(1 lsl MakeType.bits)))
		let succ f = f + 1
		let pred f = f - 1

		let min a b =
			if a <= b then
				a
			else
				b

		let max a b =
			if a < b then
				b
			else
				a

		let gth a b = a > b
		let lth a b = a < b
		let gte a b = a >= b
		let lte a b = a <= b
		let eqp a b = a == b
		let eqs a b = a = b
		let add a b = a + b
		let sub a b = a - b

		let mul a b =
			of_float(to_float(a) *. to_float(b))

		let div a b =
			of_float(to_float(a) /. to_float(b))

		let rec foreach a b f =
			f a;
			if a < b then
				foreach (succ a) b f
  end

module Fixed4 : FIXED = Make (struct let bits = 4 end)
module Fixed8 : FIXED = Make (struct let bits = 8 end)

let print_bool b =
	if b then
		print_endline "TRUE"
	else
		print_endline "FALSE"

let () =
	let x8 = Fixed8.of_float 21.10 in
	let y8 = Fixed8.of_float 21.32 in
	let r8 = Fixed8.add x8 y8 in
	print_endline (Fixed8.to_string r8);
	Fixed4.foreach (Fixed4.zero) (Fixed4.one) (fun f -> print_endline (Fixed4.to_string f));

	let x8 = Fixed8.of_float 21.10 in
	let y8 = Fixed8.of_float 21. in
	print_endline(Fixed8.to_string (Fixed8.max x8 y8));

	let x8 = Fixed8.of_float 21.10 in
	let y8 = Fixed8.of_float 21. in
	print_endline(Fixed8.to_string (Fixed8.min x8 y8));

	let x8 = Fixed8.of_float 21.10 in
	print_int (Fixed8.to_int x8);
	print_char '\n';

	let x8 = Fixed8.of_float 23.10 in
	let y8 = Fixed8.of_float 21. in
	print_bool(Fixed8.gth x8 y8);

	let x8 = Fixed8.of_float 21. in
	let y8 = Fixed8.of_float 21. in
	print_bool(Fixed8.lth x8 y8);

	let x8 = Fixed8.of_float 21.10 in
	let y8 = Fixed8.of_float 21. in
	print_bool(Fixed8.gte x8 y8);

	let x8 = Fixed8.of_float 21.10 in
	let y8 = Fixed8.of_float 21. in
	print_bool(Fixed8.lte x8 y8);

	let y8 = Fixed8.of_float 21. in
	print_bool(Fixed8.eqp x8 y8);

	let y8 = Fixed8.of_float 21. in
	print_bool(Fixed8.eqs x8 y8);

	let x8 = Fixed8.of_float 21.21 in
	let y8 = Fixed8.of_float 2. in
	print_endline(Fixed8.to_string (Fixed8.add x8 y8));

	let x8 = Fixed8.of_float 21.21 in
	let y8 = Fixed8.of_float 2. in
	print_endline(Fixed8.to_string (Fixed8.sub x8 y8));

	let x8 = Fixed8.of_float 21.21 in
	let y8 = Fixed8.of_float 2. in
	print_endline(Fixed8.to_string (Fixed8.mul x8 y8));

	let x8 = Fixed8.of_float 21.21 in
	let y8 = Fixed8.of_float 2. in
	print_endline(Fixed8.to_string (Fixed8.div x8 y8))
