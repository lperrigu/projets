type radar = float array * string

let get_left (left, _) = left
let get_right (_, right) = right

let radar = ([|0.;0.|], "radar")

(*
let radar = ([|
1.; 1.; 1.; 1.; 1.; 1.; 1.; 1.; 1.; 1.;
1.; 1.; 1.; 1.; 1.; 1.; 1.; 1.; 1.; 1.;
1.; 1.; 1.; 1.; 1.; 1.; 1.; 1.; 1.; 1.;
1.; 1.; 1.; 1.;
|], "radar")
*)

let eu_dist point1 point2 =
	let rec loop point1 point2 sum i n =
		if i < n then
			loop point1 point2
			(sum +. ((point2.(i) -. point1.(i)) ** 2.)) (i + 1) n
		else
			sqrt sum
	in loop point1 point2 0. 0 (Array.length point1)

let rec print_mytab tab i n =
	if i <> n then
begin
		print_float tab.(i);
		print_string "; ";
		print_mytab tab (i + 1) n
end

let rec print_mylist liste n = match liste with
	| [] -> print_char '\n'
	| head::tail ->
		print_string "Affichage du radar ";
		print_int n;
		print_char '\n';
		print_mytab (get_left head) 0 (Array.length (get_left head));
		print_endline (get_right head);
		print_char '\n';
		print_mylist tail ( n + 1 )

let one_nn (liste_radar : radar list) (radar : radar) =
	let rec loop liste_radar radar min_length type_radar = match liste_radar with
		| head::tail ->
			if eu_dist (get_left head) radar < min_length then
				loop tail radar (eu_dist (get_left head) radar) (get_right head)
			else
				loop tail radar min_length type_radar
		| [] -> type_radar
	in loop liste_radar (get_left radar) max_float ""


let get_list_radar file =
	let rec loop file liste_radar tab s i end_of_string c = 
		if i = end_of_string then
			liste_radar
		else if c = ',' then
			loop file liste_radar
			(Array.append tab [|(float_of_string s)|]) 
			"" (i + 1) end_of_string (String.get file (i + 1))
		else if c = '\n' then
			loop file
			(List.append liste_radar [(tab, s)]) [||]
			"" (i + 1) end_of_string (String.get file (i + 1))
		else
			loop file liste_radar tab (s^(Char.escaped c))
			(i + 1) end_of_string (String.get file (i + 1))
	in loop file [] [||] "" 0 ((String.length file) - 1) (String.get file 0)

let rec main ic file =
	try
		main ic (file^(input_line ic)^"\n")
	with
		| End_of_file -> print_endline (one_nn (get_list_radar file) radar )
		| _ -> print_endline "Probleme avec le fichier recu en entree ou avec le radar passe en parametre"

let () =
	let argv = Sys.argv in
	if (Array.length argv) = 2 then
		main (open_in argv.(1)) ""
