type 'a ft_ref = {mutable content: 'a}

let return c =
	{content = c}

let get r =
	r.content

let set r c =
	r.content <- c

let bind r (f:'a -> 'b ft_ref) =
	f r.content

let func v =
	if v = 21 then
		{content = "foo"}
	else
		{content = "bar"}

let r = return 42

let () =
	print_int (get r);
	print_char '\n';

	set r 21;
	print_int (get r);
	print_char '\n';

	print_endline (get(bind r func))
