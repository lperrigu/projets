
let sum float1 float2 =
	float1 +. float2

let () =
	print_float (sum 0.1111111 0.5555555);
	print_char '\n';

	print_float (sum 123.11145 0.5);
	print_char '\n';

	print_float (sum 0.0 nan);
	print_char '\n'
