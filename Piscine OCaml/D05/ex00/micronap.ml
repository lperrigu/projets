let my_sleep () = Unix.sleep 1

let main argc argv =
	if argc = 2 then
		try
			for i = 1 to int_of_string (argv.(1)) do
				my_sleep ()
			done;
		with
			| _ -> ()

let () =
	let argv = Sys.argv in
	main (Array.length argv) argv
