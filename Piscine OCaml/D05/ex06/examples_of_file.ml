let get_left (left, _) = left
let get_right (_, right) = right

let rec print_mytab tab i n =
	if i <> n then
begin
		print_float tab.(i);
		print_string "; ";
		print_mytab tab (i + 1) n
end

let rec print_mylist liste n = match liste with
	| [] -> print_char '\n'
	| head::tail ->
		print_string "Affichage du radar ";
		print_int n;
		print_char '\n';
		print_mytab (get_left head) 0 (Array.length (get_left head));
		print_endline (get_right head);
		print_char '\n';
		print_mylist tail ( n + 1 )

let get_list_radar file =
	let rec loop file liste_radar tab s i end_of_string c = 
		if i = end_of_string then
			liste_radar
		else if c = ',' then
			loop file liste_radar
			(Array.append tab [|(float_of_string s)|]) 
			"" (i + 1) end_of_string (String.get file (i + 1))
		else if c = '\n' then
			loop file
			(List.append liste_radar [(tab, s)]) [||]
			"" (i + 1) end_of_string (String.get file (i + 1))
		else
			loop file liste_radar tab (s^(Char.escaped c))
			(i + 1) end_of_string (String.get file (i + 1))
	in loop file [] [||] "" 0 ((String.length file) - 1) (String.get file 0)

let rec main ic file =
	try
		main ic (file^(input_line ic)^"\n")
	with
		| End_of_file -> print_mylist (get_list_radar file ) 0
		| _ -> print_endline "Probleme avec le fichier recu en parametre"

let () =
	let argv = Sys.argv in
	if (Array.length argv) = 2 then
		main (open_in argv.(1)) ""
