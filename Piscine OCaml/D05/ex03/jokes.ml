
let jokes = Array.make 5 ""
let i = ref 0


let () =
	Random.self_init();
	let ic = open_in "jokes_file" in
	try
		while true do
			Array.set jokes !i (input_line ic);
			incr i
		done
	with
		| End_of_file -> print_endline jokes.(Random.int !i)
		| Invalid_argument err -> print_endline jokes.(Random.int !i)
