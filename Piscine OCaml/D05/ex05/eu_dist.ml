
let eu_dist point1 point2 =
	let rec loop point1 point2 sum i n =
		if i < n then
			loop point1 point2
			(sum +. ((point2.(i) -. point1.(i)) ** 2.)) (i + 1) n
		else
			sqrt sum
	in loop point1 point2 0. 0 (Array.length point1)

let point1 = Array.make 2 0.
let point2 = Array.make 2 1.
let point3 = Array.make 2 2.
let point4 = Array.make 2 12.

let () =
	print_float ( eu_dist point1 point2 );
	print_char '\n';
	print_float ( eu_dist point1 point3 );
	print_char '\n';
	print_float ( eu_dist point1 point4 );
	print_char '\n';
	print_float ( eu_dist point4 point2 );
	print_char '\n';
	print_float ( eu_dist point2 point4 );
	print_char '\n'
