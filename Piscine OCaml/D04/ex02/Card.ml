
module Color =
struct

type t = Spade | Heart | Diamond | Club
let all = [Spade; Heart; Diamond; Club]

let toString t = match t with
    | Spade -> "S"
    | Heart -> "H"
    | Diamond -> "D"
    | Club -> "C"

let toStringVerbose t = match t with
    | Spade -> "Spade"
    | Heart -> "Heart"
    | Diamond -> "Diamond"
    | Club -> "Club"

end


module Value =
struct

type t = T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | Jack | Queen | King | As
let all = [T2; T3; T4; T5; T6; T7; T8; T9; T10; Jack; Queen; King; As]

let toInt value = match value with
    | T2 -> 1
    | T3 -> 2
    | T4 -> 3
    | T5 -> 4
    | T6 -> 5
    | T7 -> 6
    | T8 -> 7
    | T9 -> 8
    | T10 -> 9
    | Jack -> 10
    | Queen -> 11
    | King -> 12
    | As -> 13

let toString value = match value with
    | T2 -> "2"
    | T3 -> "3"
    | T4 -> "4"
    | T5 -> "5"
    | T6 -> "6"
    | T7 -> "7"
    | T8 -> "8"
    | T9 -> "9"
    | T10 -> "10"
    | Jack -> "J"
    | Queen -> "Q"
    | King -> "K"
    | As -> "A"

let toStringVerbose value = match value with
    | T2 -> "2"
    | T3 -> "3"
    | T4 -> "4"
    | T5 -> "5"
    | T6 -> "6"
    | T7 -> "7"
    | T8 -> "8"
    | T9 -> "9"
    | T10 -> "10"
    | Jack -> "Jack"
    | Queen -> "Queen"
    | King -> "King"
    | As -> "As"

let next value = match value with
    | T2 -> T3
    | T3 -> T4
    | T4 -> T5
    | T5 -> T6
    | T6 -> T7
    | T7 -> T8
    | T8 -> T9
    | T9 -> T10
    | T10 -> Jack
    | Jack -> Queen
    | Queen -> King
    | King -> As
    | As -> invalid_arg "As is already the highest value"

let previous value = match value with
    | T2 -> invalid_arg "T2 is already the shortest value"
    | T3 -> T2
    | T4 -> T3
    | T5 -> T4
    | T6 -> T5
    | T7 -> T6
    | T8 -> T7
    | T9 -> T8
    | T10 -> T9
    | Jack -> T10
    | Queen -> Jack
    | King -> Queen
    | As -> King

end


type t = ( Value.t * Color.t )

let newCard value color = (value, color)

let allSpades =
	[
	(Value.T2, Color.Spade);
	(Value.T3, Color.Spade);
	(Value.T4, Color.Spade);
	(Value.T5, Color.Spade);
	(Value.T6, Color.Spade);
	(Value.T7, Color.Spade);
	(Value.T8, Color.Spade);
	(Value.T9, Color.Spade);
	(Value.T10, Color.Spade);
	(Value.Jack, Color.Spade);
	(Value.Queen, Color.Spade);
	(Value.King, Color.Spade);
	(Value.As, Color.Spade)
	]

let allHearts =
	[
	(Value.T2, Color.Heart);
	(Value.T3, Color.Heart);
	(Value.T4, Color.Heart);
	(Value.T5, Color.Heart);
	(Value.T6, Color.Heart);
	(Value.T7, Color.Heart);
	(Value.T8, Color.Heart);
	(Value.T9, Color.Heart);
	(Value.T10, Color.Heart);
	(Value.Jack, Color.Heart);
	(Value.Queen, Color.Heart);
	(Value.King, Color.Heart);
	(Value.As, Color.Heart);
	]

let allDiamonds =
	[
	(Value.T2, Color.Diamond);
	(Value.T3, Color.Diamond);
	(Value.T4, Color.Diamond);
	(Value.T5, Color.Diamond);
	(Value.T6, Color.Diamond);
	(Value.T7, Color.Diamond);
	(Value.T8, Color.Diamond);
	(Value.T9, Color.Diamond);
	(Value.T10, Color.Diamond);
	(Value.Jack, Color.Diamond);
	(Value.Queen, Color.Diamond);
	(Value.King, Color.Diamond);
	(Value.As, Color.Diamond)
	]

let allClubs =
	[
	(Value.T2, Color.Club);
	(Value.T3, Color.Club);
	(Value.T4, Color.Club);
	(Value.T5, Color.Club);
	(Value.T6, Color.Club);
	(Value.T7, Color.Club);
	(Value.T8, Color.Club);
	(Value.T9, Color.Club);
	(Value.T10, Color.Club);
	(Value.Jack, Color.Club);
	(Value.Queen, Color.Club);
	(Value.King, Color.Club);
	(Value.As, Color.Club)
	]


let all =
	[
	(Value.T2, Color.Spade);
	(Value.T3, Color.Spade);
	(Value.T4, Color.Spade);
	(Value.T5, Color.Spade);
	(Value.T6, Color.Spade);
	(Value.T7, Color.Spade);
	(Value.T8, Color.Spade);
	(Value.T9, Color.Spade);
	(Value.T10, Color.Spade);
	(Value.Jack, Color.Spade);
	(Value.Queen, Color.Spade);
	(Value.King, Color.Spade);
	(Value.As, Color.Spade);
	(Value.T2, Color.Heart);
	(Value.T3, Color.Heart);
	(Value.T4, Color.Heart);
	(Value.T5, Color.Heart);
	(Value.T6, Color.Heart);
	(Value.T7, Color.Heart);
	(Value.T8, Color.Heart);
	(Value.T9, Color.Heart);
	(Value.T10, Color.Heart);
	(Value.Jack, Color.Heart);
	(Value.Queen, Color.Heart);
	(Value.King, Color.Heart);
	(Value.As, Color.Heart);
	(Value.T2, Color.Diamond);
	(Value.T3, Color.Diamond);
	(Value.T4, Color.Diamond);
	(Value.T5, Color.Diamond);
	(Value.T6, Color.Diamond);
	(Value.T7, Color.Diamond);
	(Value.T8, Color.Diamond);
	(Value.T9, Color.Diamond);
	(Value.T10, Color.Diamond);
	(Value.Jack, Color.Diamond);
	(Value.Queen, Color.Diamond);
	(Value.King, Color.Diamond);
	(Value.As, Color.Diamond);
	(Value.T2, Color.Club);
	(Value.T3, Color.Club);
	(Value.T4, Color.Club);
	(Value.T5, Color.Club);
	(Value.T6, Color.Club);
	(Value.T7, Color.Club);
	(Value.T8, Color.Club);
	(Value.T9, Color.Club);
	(Value.T10, Color.Club);
	(Value.Jack, Color.Club);
	(Value.Queen, Color.Club);
	(Value.King, Color.Club);
	(Value.As, Color.Club)
	]

let getValue (value, _) = value
let getColor (_, color) = color

let toString card =
	Value.toString (getValue card)
	^" "^
	Color.toString (getColor card)

let toStringVerbose card =
	Value.toStringVerbose (getValue card)
	^" of "^
	Color.toStringVerbose (getColor card)

let compare card1 card2 =
	Value.toInt(getValue card1) - Value.toInt(getValue card2)

let max card1 card2 =
	if (compare card1 card2) >= 0 then
		card1
	else
		card2

let min card1 card2 =
	if (compare card1 card2) <= 0 then
		card2
	else
		card1

let best cards = match cards with
	| [] -> invalid_arg "No best in no cards"
	| head::tail ->
		let rec loop cards best_card = match cards with
			| [] -> best_card
			| head::tail -> loop tail ( max head best_card )
		in loop tail head

let isOf card color = ( ( getColor card ) = color )

	let isSpade card = (isOf card Color.Spade)

	let isHeart card = (isOf card Color.Heart)

	let isDiamond card = (isOf card Color.Diamond)

	let isClub card = (isOf card Color.Club)
