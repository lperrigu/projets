
let rec test_color all = match all with
	| [] -> print_char '\n'
	| head::tail ->
		print_endline ( Card.Color.toString head );
		print_endline ( Card.Color.toStringVerbose head );
		test_color tail

let rec test_value all = match all with
	| [] -> print_char '\n'
	| head::tail ->
		print_int (Card.Value.toInt head);
		print_char '\n';
		print_endline (Card.Value.toString head);
		print_endline (Card.Value.toStringVerbose head);
		if Card.Value.toInt head <> 13 then
			print_endline (Card.Value.toStringVerbose (Card.Value.next head));
		if Card.Value.toInt head <> 1 then
			print_endline (Card.Value.toStringVerbose (Card.Value.previous head));
		print_char '\n';
		test_value tail

let rec test_card all = match all with
	| [] -> print_char '\n'
	| head::tail ->
		print_string ((Card.toString head)^"\\"^(Card.toStringVerbose head));

		if (Card.isOf head Card.Color.Club) then
			print_endline " oui c'est un club"
		else if Card.isSpade head then
			print_endline " oui c'est un spade"
		else if Card.isHeart head then
			print_endline " oui c'est un heart"
		else
			print_endline " oui c'est un diamond";
		test_card tail

let () =
	print_endline "**************************COLOR********************************";
	test_color ( Card.Color.all );
	print_endline "\n\n\n";
	print_endline "**************************VALUE********************************";
	test_value ( Card.Value.all );
	print_endline "\n\n\n";
	print_endline "**************************CARD********************************";
	test_card ( Card.all )
