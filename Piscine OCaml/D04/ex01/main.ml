
let rec test_value all = match all with
	| [] -> print_char '\n'
	| head::tail ->
		print_int (Value.toInt head);
		print_char '\n';
		print_endline (Value.toString head);
		print_endline (Value.toStringVerbose head);
		if Value.toInt head <> 13 then
			print_endline (Value.toStringVerbose (Value.next head));
		if Value.toInt head <> 1 then
			print_endline (Value.toStringVerbose (Value.previous head));
		print_char '\n';
		test_value tail

let () =
	test_value ( Value.all )
