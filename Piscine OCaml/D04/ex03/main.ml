
let rec test_color all = match all with
	| [] -> print_char '\n'
	| head::tail ->
		print_endline ( Deck.Card.Color.toString head );
		print_endline ( Deck.Card.Color.toStringVerbose head );
		test_color tail

let rec test_value all = match all with
	| [] -> print_char '\n'
	| head::tail ->
		print_int (Deck.Card.Value.toInt head);
		print_char '\n';
		print_endline (Deck.Card.Value.toString head);
		print_endline (Deck.Card.Value.toStringVerbose head);
		if Deck.Card.Value.toInt head <> 13 then
			print_endline (Deck.Card.Value.toStringVerbose (Deck.Card.Value.next head));
		if Deck.Card.Value.toInt head <> 1 then
			print_endline (Deck.Card.Value.toStringVerbose (Deck.Card.Value.previous head));
		print_char '\n';
		test_value tail

let rec test_card all = match all with
	| [] -> print_char '\n'
	| head::tail ->
		print_string ((Deck.Card.toString head)^"\\"^(Deck.Card.toStringVerbose head));

		if (Deck.Card.isOf head Deck.Card.Color.Club) then
			print_endline " oui c'est un club"
		else if Deck.Card.isSpade head then
			print_endline " oui c'est un spade"
		else if Deck.Card.isHeart head then
			print_endline " oui c'est un heart"
		else
			print_endline " oui c'est un diamond";
		test_card tail

let get_left ( left, _ ) = left

let test_deck deck =
	print_string "On pioche la premiere carte : ";
	print_endline ( Deck.Card.toStringVerbose ( get_left( Deck.drawCard deck ) ) );
	let rec loop liste = match liste with
		| [] -> print_char '\n'
		| head::tail -> print_endline head;
		loop tail
	in loop (Deck.toStringListVerbose deck)

let () =
	Random.self_init();
	print_endline "**************************COLOR********************************";
	test_color ( Deck.Card.Color.all );
	print_endline "\n\n\n";
	print_endline "**************************VALUE********************************";
	test_value ( Deck.Card.Value.all );
	print_endline "\n\n\n";
	print_endline "**************************CARD********************************";
	test_card ( Deck.Card.all );
	print_endline "\n\n\n";
	print_endline "**************************DECK********************************";
	test_deck ( Deck.newDeck() )
