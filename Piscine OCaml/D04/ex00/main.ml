
let rec test_color all = match all with
	| [] -> print_char '\n'
	| head::tail ->
	   	print_endline ( Color.toString head );
	   	print_endline ( Color.toStringVerbose head );
		test_color tail

let () =
	test_color( Color.all )