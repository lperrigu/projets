(*type 'a tree = Nil | Node of 'a * 'a tree * 'a tree*)
(*type tree = Nil | ( 'a, 'a tree, 'a tree)*)
type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let get_left ( left, _, _ ) = left
let get_mid ( _, mid, _ ) = mid
let get_right ( _, _, right ) = right

let draw_square x y size =	
	Graphics.moveto ( x - size / 2 ) ( y - size / 2 );
	Graphics.lineto ( x + size / 2 ) ( y - size / 2 );
	Graphics.lineto ( x + size / 2 ) ( y + size / 2 );
	Graphics.lineto ( x - size / 2 ) ( y + size / 2 );
	Graphics.lineto ( x - size / 2 ) ( y - size / 2 )
(*
let draw_tree_node node =
	let rec draw_tree_node_rec node x y n = match node with
		| Nil -> draw_square x y 100;
		Graphics.moveto x y;
		Graphics.draw_string "Nil"
		| _ -> Graphics.draw_string ( get_left node );
		draw_square x y 100;
		Graphics.moveto x y;
		Graphics.draw_string
	 	draw_tree_node_rec ( get_mid node ) ( x + 150 )
		( 300 + int_of_float( 600.0 /. (2.0 ** float_of_int n) ) ) (n + 1);
	 	draw_tree_node_rec ( get_right node ) ( x + 150 )
		( 300 - int_of_float( 600.0 /. (2.0 ** float_of_int n) ) ) (n + 1)
	in draw_tree_node_rec node 60 300 1
*)
let draw_tree_node node = match node with
	| Nil ->
	draw_square 60 300 100;
	Graphics.moveto 60 300;
	Graphics.draw_string "Nil"
	| _ ->
	draw_square 60 300 100;
	Graphics.moveto 60 300;
	Graphics.draw_string "value";

	draw_square 260 450 100;
	Graphics.moveto 260 450;
	Graphics.draw_string "Nil";

	draw_square 260 150 100;
	Graphics.moveto 260 150;
	Graphics.draw_string "Nil";

	Graphics.moveto 110 300;
	Graphics.lineto 210 450;

	Graphics.moveto 110 300;
	Graphics.lineto 210 150

let main () =
	Graphics.open_graph " 800x600";
	draw_tree_node (Node("value", Nil, Nil));
	let rec inf_loop n =
		inf_loop n
	in inf_loop 3

let _ = main ();;