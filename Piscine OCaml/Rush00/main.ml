(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: oceyral <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/11/08 19:54:11 by oceyral           #+#    #+#             *)
(*   Updated: 2015/11/08 22:17:14 by oceyral          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)


let () =
  let rec playone () =
	let game = Game.newGame () in
	Game.playGame game;
	print_endline "Restart ? Y/N";
	let ans = read_line ()
	in
	if ans = "Y" || ans = "y" then playone ()
	else print_endline "Good bye :("
  in playone()
