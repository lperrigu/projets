(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Game.mli                                           :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: oceyral <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/11/08 16:08:21 by oceyral           #+#    #+#             *)
(*   Updated: 2015/11/08 20:17:18 by oceyral          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)


module Subgame :
  sig
	type t

	val newSub : unit -> t
	val printLine : t -> int -> unit
	val isDone : t -> bool
	val wonBy : t -> int
	val play : t -> int -> int -> t

  end



type t

val newGame : unit -> t
val printGame : t -> unit
val playGame : t -> unit

