(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Game.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: oceyral <marvin@42.fr>                     +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2015/11/08 16:15:16 by oceyral           #+#    #+#             *)
(*   Updated: 2015/11/08 22:15:10 by oceyral          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)


module Subgame =
  struct
	type t = ( char * char * char ) list

	let newSub() =
		[ ( '-', '-', '-' ); ( '-', '-', '-' ); ( '-', '-', '-' ) ]

	let get_left ( left, _, _ ) = left
	let get_mid ( _, mid, _ ) = mid
	let get_right ( _, _, right ) = right

	let checkLine grid n =
		let rec loop grid i n = match grid with
			| head::tail ->
				if i = n then
					if (get_left head) = (get_mid head) &&
					(get_mid head) = (get_right head) &&
					(get_left head) = 'O' then
						0
					else if (get_left head) = (get_mid head) &&
					(get_mid head) = (get_right head) &&
					(get_left head) = 'X' then
						1
					else
						(-2)
				else
					loop tail (i + 1) n
			| [] -> (-1)
		in loop grid 1 n


	let checkCol grid n = match grid with
		| head::tail ->
			let rec loop grid n l m r = match grid with
				| head::tail ->
			   		if n = 1 && l = '-' then
						(-2)
			   		else if n = 2 && m = '-' then
						(-2)
			   		else if n = 3 && r = '-' then
						(-2)
					else if n = 1 && l <> (get_left head) then
						(-2)
					else if n = 2 && m <> (get_mid head) then
						(-2)
					else if n = 3 && r <> (get_right head) then
						(-2)
					else
						loop tail n l m r
				| [] ->
					if n = 1 && l = 'O' then
						0
					else if n = 1 && l = 'X' then
						1
					else if n = 2 && m = 'O' then
						0
					else if n = 2 && m = 'X' then
						1
					else if n = 3 && r = 'O' then
						0
					else if n = 3 && r = 'X' then
						1
					else
						(-1)
			in loop tail n
			(get_left (List.hd grid)) (get_mid (List.hd grid)) (get_right (List.hd grid))
		| [] -> (-1)

	let checkDial grid n = match grid with
		| head::tail ->
			let rec loop grid i n l r = match grid with
				| head::tail ->
					if i = 2 && n = 1 && l = '-' then
						(-2)
					else if i = 2 && n = 2 && r = '-' then
						(-2)
					else if i = 2 && n = 1 && (get_mid head) <> l then
						(-2)
					else if i = 2 && n = 2 && (get_mid head) <> r then
						(-2)
					else if i = 3 && n = 1 && (get_right head) <> l then
						(-2)
					else if i = 3 && n = 2 && (get_left head) <> r then
						(-2)
					else
						loop tail (i + 1) n l r
				| [] ->
					if n = 1 && l = 'O' then
						0
					else if n = 1 && l = 'X' then
						1
					else if n = 2 && r = 'O' then
						0
					else if n = 2 && r = 'X' then
						1
					else
						(-1)
			in loop tail 2 n
			(get_left (List.hd grid)) (get_right (List.hd grid))
		| [] -> (-1)

	let isDone grid =
		((checkLine grid 1) >= 0) || ((checkLine grid 2) >= 0) ||
		((checkLine grid 3) >= 0) || ((checkCol grid 1) >= 0) ||
		((checkCol grid 2) >= 0) || ((checkCol grid 3) >= 0) ||
		((checkDial grid 1) >= 0) || ((checkDial grid 2) >= 0)

	let wonBy grid =
		if ((checkLine grid 1) >= 0) then
			checkLine grid 1
		else if ((checkLine grid 2) >= 0) then
			checkLine grid 2
		else if ((checkLine grid 3) >= 0) then
			checkLine grid 3
		else if ((checkCol grid 1) >= 0) then
			checkCol grid 1
		else if ((checkCol grid 2) >= 0) then
			checkCol grid 2
		else if ((checkCol grid 3) >= 0) then
			checkCol grid 3
		else if ((checkDial grid 1) >= 0) then
			checkDial grid 1
		else if ((checkDial grid 2) >= 0) then
			checkDial grid 2
		else
			(-1)

	let change_cell grid case c =
		if case = 1 then
			[c, get_mid (List.hd grid), get_right(List.hd grid)]@ List.tl grid
		else if case = 2 then
			[get_left (List.hd grid), c, get_right(List.hd grid)]@ List.tl grid
		else if case = 3 then
			[get_left (List.hd grid), get_mid(List.hd grid), c]@ List.tl grid
		else if case = 4 then
			[List.hd grid]@[c, get_mid (List.nth grid 1), get_right(List.nth grid 1)]
			@[List.nth grid 2]
		else if case = 5 then
			[List.hd grid]@[get_left (List.nth grid 1), c, get_right(List.nth grid 1)]
			@[List.nth grid 2]
		else if case = 6 then
			[List.hd grid]@[get_left (List.nth grid 1), get_mid(List.nth grid 1), c]
			@[List.nth grid 2]
		else if case = 7 then
			[List.hd grid]@[List.nth grid 1]@[c, get_mid (List.nth grid 2), get_right(List.nth grid 2)]
		else if case = 8 then
			[List.hd grid]@[List.nth grid 1]@[get_left (List.nth grid 2), c, get_right(List.nth grid 2)]
		else
			[List.hd grid]@[List.nth grid 1]@[get_left (List.nth grid 2), get_mid(List.nth grid 2), c]

	let is_empty grid case =
		if case = 1 then
			((get_left (List.hd grid)) = '-')
		else if case = 2 then
			((get_mid (List.hd grid)) = '-')
		else if case = 3 then
			((get_right (List.hd grid)) = '-')
		else if case = 4 then
			((get_left (List.nth grid 1)) = '-')
		else if case = 5 then
			((get_mid (List.nth grid 1)) = '-')
		else if case = 6 then
			((get_right (List.nth grid 1)) = '-')
		else if case = 7 then
			((get_left (List.nth grid 2)) = '-')
		else if case = 8 then
			((get_mid (List.nth grid 2)) = '-')
		else
			((get_right (List.nth grid 2)) = '-')

	let count grid =
		let rec loop grid n c = match grid with
			| head::tail ->
				if (get_left head) = '-' && (get_mid head) = '-' && (get_right head) = '-' then
					loop tail n (c + 3)
				else if (get_left head) = '-' && (get_mid head) = '-' then
					loop tail n (c + 2)
				else if (get_left head) = '-' && (get_right head) = '-' then
					loop tail n (c + 2)
				else if (get_mid head) = '-' && (get_right head) = '-' then
					loop tail n (c + 2)
				else if (get_left head) = '-' then
					loop tail n (c + 1)
				else if (get_mid head) = '-' then
					loop tail n (c + 1)
				else if (get_right head) = '-' then
					loop tail n (c + 1)
				else
					loop tail n c
			| [] -> c
		in loop grid 1 0

	let play grid case player =
		if count grid = 1 && (is_empty grid case) && player = 0 then
			[('O', 'O', 'O'); ('O', 'O', 'O'); ('O', 'O', 'O')]
		else if count grid = 1 && (is_empty grid case) && player = 1 then
			[('X', 'X', 'X'); ('X', 'X', 'X'); ('X', 'X', 'X')]
		else if (is_empty grid case) = false then
		  begin
			print_endline "Illegal move.";
			grid
		  end
		else if player = 0 then
			change_cell grid case 'O'
		else if player = 1 then
			change_cell grid case 'X'
		else
			grid (*Ce cas me devrait jamais arriver*)


	let printLine grid row =
		let rec printLine grid row i gridC = match grid with
			| head::tail ->
				if i = row && (wonBy gridC) = 0 && row = 1 then
				  begin
					print_char '/';
					print_char ' ';
					print_char '-';
					print_char ' ';
					print_char '\\';
				  end
				else if i = row && (wonBy gridC) = 0 && row = 2 then
				  begin
					print_char '|';
					print_char ' ';
					print_char ' ';
					print_char ' ';
					print_char '|';
				  end
				else if i = row && (wonBy gridC) = 0 && row = 3 then
				  begin
					print_char '\\';
					print_char ' ';
					print_char '-';
					print_char ' ';
					print_char '/';
				  end
				else if i = row && (wonBy gridC) = 1 && row = 1 then
				  begin
					print_char '\\';
					print_char ' ';
					print_char ' ';
					print_char ' ';
					print_char '/';
				  end
				else if i = row && (wonBy gridC) = 1 && row = 2 then
				  begin
					print_char ' ';
					print_char ' ';
					print_char 'X';
					print_char ' ';
					print_char ' ';
				  end
				else if i = row && (wonBy gridC) = 1 && row = 3 then
				  begin
					print_char '/';
					print_char ' ';
					print_char ' ';
					print_char ' ';
					print_char '\\';
				  end
				else if i = row then
				  begin
					print_char (get_left head);
					print_char ' ';
					print_char (get_mid head);
					print_char ' ';
					print_char (get_right head)
				  end
				else
					printLine tail row ( i + 1 ) gridC
			| [] -> ()
		in printLine grid row 1 grid

  end


	
type t = {
	s1 : Subgame.t;
	s2 : Subgame.t;
	s3 : Subgame.t;
	s4 : Subgame.t;
	s5 : Subgame.t;
	s6 : Subgame.t;
	s7 : Subgame.t;
	s8 : Subgame.t;
	s9 : Subgame.t;
	playerTurn : int;
  }



let newGame () =
  {
	s1 = Subgame.newSub (); s2 = Subgame.newSub (); s3 = Subgame.newSub ();
	s4 = Subgame.newSub (); s5 = Subgame.newSub (); s6 = Subgame.newSub ();
	s7 = Subgame.newSub (); s8 = Subgame.newSub (); s9 = Subgame.newSub ();
	playerTurn = 0;
  }
	
						

let printGame game =
  let rec print_row n s1 s2 s3 =
	Subgame.printLine s1 n ;
	print_string " | ";
	Subgame.printLine s2 n ;
	print_string " | ";
	Subgame.printLine s3 n ;
	print_endline "";
	if n < 3 then print_row (n + 1) s1 s2 s3
  in
  print_row 1 game.s1 game.s2 game.s3;
  print_endline "----------------------";
  print_row 1 game.s4 game.s5 game.s6;
  print_endline "----------------------";
  print_row 1 game.s7 game.s8 game.s9


let updateSub game sub n = match n with
  | 1 -> {game with s1 = sub}
  | 2 -> {game with s2 = sub}
  | 3 -> {game with s3 = sub}
  | 4 -> {game with s4 = sub}
  | 5 -> {game with s5 = sub}
  | 6 -> {game with s6 = sub}
  | 7 -> {game with s7 = sub}
  | 8 -> {game with s8 = sub}
  | 9 -> {game with s9 = sub}
  |_ -> game
  
					  
let playSub game n =
  let play_sub sub =
	if Subgame.isDone sub then
	  begin
		print_string "Tile has been won by player " ;
		print_int (Subgame.wonBy sub);
		print_endline "";
		sub
	  end
	else
	  begin
		let is_digit c = c >= '0' && c <= '9' in
		let rec play_move sub  =
		  print_string "Play square (1-9) :" ;
		  let selected = read_line () in
		  if (String.length selected) = 1
			 && is_digit (String.get selected 0)
		  then
			if (Subgame.is_empty sub (int_of_string selected))
			then
			  let toto = Subgame.play sub (int_of_string selected) game.playerTurn in
			  if Subgame.isDone toto then
				begin
				  print_string "Tile has been won by player " ;
				  print_int (Subgame.wonBy toto);
				  print_endline "";
				  toto
				end
			  else
				toto
			else
			  begin
			  print_endline "Error" ;
			  play_move sub
			  end
		  else
			begin
			  print_endline "Error" ;
			  play_move sub
			end
		in play_move sub
	  end
  in match n with
  | 1 -> {game with s1 = play_sub game.s1}
  | 2 -> {game with s2 = play_sub game.s2}
  | 3 -> {game with s3 = play_sub game.s3}
  | 4 -> {game with s4 = play_sub game.s4}
  | 5 -> {game with s5 = play_sub game.s5}
  | 6 -> {game with s6 = play_sub game.s6}
  | 7 -> {game with s7 = play_sub game.s7}
  | 8 -> {game with s8 = play_sub game.s8}
  | 9 -> {game with s9 = play_sub game.s9}
  |_ -> print_endline "Error" ; game


let rec selectGame (game:t) =
  let is_digit c = c >= '0' && c <= '9' in
  let play_selected game selected =
	if (String.length selected) = 1
	   && is_digit (String.get selected 0)
	then playSub game (int_of_string selected)
	else begin print_endline "Error" ; selectGame game end
  in
  print_string "Select SubGame (1-9) :" ;
  play_selected game (read_line ())


let check_diag game =
  (Subgame.isDone game.s1 && Subgame.isDone game.s5 && Subgame.isDone game.s9
   && Subgame.wonBy game.s1 = Subgame.wonBy game.s5 && Subgame.wonBy game.s1 = Subgame.wonBy game.s9)
  || (Subgame.isDone game.s3 && Subgame.isDone game.s5 && Subgame.isDone game.s7
   && Subgame.wonBy game.s7 = Subgame.wonBy game.s5 && Subgame.wonBy game.s7 = Subgame.wonBy game.s7)

let check_cols game =
  (Subgame.isDone game.s1 && Subgame.isDone game.s4 && Subgame.isDone game.s7
   && Subgame.wonBy game.s1 = Subgame.wonBy game.s4 && Subgame.wonBy game.s1 = Subgame.wonBy game.s7)
  || (Subgame.isDone game.s2 && Subgame.isDone game.s5 && Subgame.isDone game.s8
   && Subgame.wonBy game.s2 = Subgame.wonBy game.s5 && Subgame.wonBy game.s2 = Subgame.wonBy game.s8)
  || (Subgame.isDone game.s3 && Subgame.isDone game.s6 && Subgame.isDone game.s9
   && Subgame.wonBy game.s3 = Subgame.wonBy game.s6 && Subgame.wonBy game.s3 = Subgame.wonBy game.s9)

let check_rows game =
  (Subgame.isDone game.s1 && Subgame.isDone game.s2 && Subgame.isDone game.s3
   && Subgame.wonBy game.s1 = Subgame.wonBy game.s2 && Subgame.wonBy game.s1 = Subgame.wonBy game.s3)
  || (Subgame.isDone game.s4 && Subgame.isDone game.s5 && Subgame.isDone game.s6
   && Subgame.wonBy game.s4 = Subgame.wonBy game.s5 && Subgame.wonBy game.s4 = Subgame.wonBy game.s6)
  || (Subgame.isDone game.s7 && Subgame.isDone game.s8 && Subgame.isDone game.s9
   && Subgame.wonBy game.s7 = Subgame.wonBy game.s8 && Subgame.wonBy game.s7 = Subgame.wonBy game.s9)

																		
let rec playGame game =
  let check_won game =
	(check_diag game)|| (check_cols game) || (check_rows game)
  in
  if (check_won game)
  then
	begin
	  printGame game;
	  print_string "Game won by player " ;
	  print_int game.playerTurn ;
	  print_endline ""
	end
  else
	begin
	  printGame game;
	  playGame {(selectGame game) with playerTurn = (game.playerTurn + 1) mod 2}
	end


