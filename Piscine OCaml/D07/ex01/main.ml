let () =
	let boombot = new People.people "boombot" in
	let boom = new Doctor.doctor"Boom" 3 boombot in
	print_endline boom#to_string;
	boom#talk;
	boom#travel_in_time 2015 2017;
	print_endline boom#to_string;
	boom#use_sonic_screwdriver;
	boom#set_hp 30;
	print_endline boom#to_string;
	boom#regenerate;
	print_endline boom#to_string;
	boom#set_hp 130;
	print_endline boom#to_string
