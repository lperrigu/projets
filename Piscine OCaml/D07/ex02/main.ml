let () =
	Random.self_init();
	let boombot = new People.people "boombot" in
	let boom = new Doctor.doctor "Boom" 3 boombot in
	let dal = new Dalek.dalek in
	print_endline dal#to_string;
	dal#talk;
	dal#exterminate boombot;
	print_endline boombot#to_string;
	print_endline dal#to_string;
	boom#use_sonic_screwdriver;
	dal#die
