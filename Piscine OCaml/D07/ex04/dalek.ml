
class dalek =
	object (self)
		val _name = "Dalek"^(String.make 1 (char_of_int (97 + Random.int 26)))
			^(String.make 1 (char_of_int (97 + Random.int 26)))
			^(String.make 1 (char_of_int (97 + Random.int 26)))
		val mutable _hp = 100
		val mutable _shield = true

		method get_name = _name
		method get_hp = _hp
		method get_shield = _shield
		method change_shield = _shield <- (not _shield)
		method set_hp x = _hp <- x
		method to_string =
			if self#get_shield then
			(self#get_name)^" is a dalek with "^(string_of_int self#get_hp)^" hp"
				^" and have a shield"
			else
			(self#get_name)^" is a dalek with "^(string_of_int self#get_hp)^" hp"
				^" and don't have a shield"

		method talk =
			let i = Random.int 4 in
			if i = 0 then
				print_endline "Explain! Explain!"
			else if i = 1 then
				print_endline "Exterminate! Exterminate!"
			else if i = 2 then
				print_endline "I obey!"
			else
				print_endline "You are the Doctor! You are the enemy of the Daleks!"
		method exterminate (peop : People.people) =
			print_endline ((self#get_name)^" attack "^(peop#get_name));
			peop#set_hp 0;
			peop#die;
			self#change_shield

		method die = print_endline "Emergency Temporal Shift!"
		initializer print_endline ((self#get_name)^" the dalek is born")
	end
 
