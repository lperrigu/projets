Random.self_init()

let p1 = new People.people "people1"
let p2 = new People.people "people2"
let p3 = new People.people "people3"
let p4 = new People.people "people4"
let p5 = new People.people "people5"

let l1 = [p1;p2;p3;p4;p5]

let s1 = new People.people "sidekick1"
let d1 = new Doctor.doctor "doctor1" 3 s1
let s2 = new People.people "sidekick2"
let d2 = new Doctor.doctor "doctor2" 3 s2
let s3 = new People.people "sidekick"
let d3 = new Doctor.doctor "doctor3" 3 s3
let s4 = new People.people "sidekick4"
let d4 = new Doctor.doctor "doctor4" 3 s4
let s5 = new People.people "sidekick5"
let d5 = new Doctor.doctor "doctor5" 3 s5

let l2 = [d1;d2;d3;d4;d5]

let da1 = new Dalek.dalek
let da2 = new Dalek.dalek
let da3 = new Dalek.dalek
let da4 = new Dalek.dalek
let da5 = new Dalek.dalek

let l3 = [da1;da2;da3;da4;da5]


let () =
	let gali = new Galifrey.galifrey l1 l2 l3
	in gali#do_time_war
