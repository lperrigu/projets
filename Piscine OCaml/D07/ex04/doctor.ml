
class doctor (name : string) (age : int) (sidekick : People.people) =
	object (self)
		val _name = name
		val mutable _age = age
		val _sidekick = sidekick
		val mutable _hp = 100
		method get_name = _name
		method get_age = _age
		method get_sidekick = _sidekick
		method get_hp = _hp
		method set_hp x =
		if x < 0 then
			self#set_hp 0
		else if x > 100 then
			self#set_hp 100
		else
			_hp <- x
		method to_string =
			(self#get_name)^" is a doctor with "^(string_of_int self#get_hp)^" hp, who is "
			^(string_of_int self#get_age)^" year's old, and has "^(self#get_sidekick#get_name)
			^" as sidekick."
			
		method talk = print_endline "Hi! I'm the Doctor!"
		initializer print_endline (name^" the doctor is born")
		method travel_in_time start arrival =
			_age <- self#get_age + ((max start arrival) - (min start arrival));
			print_endline "          _";
			print_endline "         /-\\";
			print_endline "    _____|#|_____";
			print_endline "   |_____________|";
			print_endline "  |_______________|";
			print_endline "|||_POLICE_##_BOX_|||";
			print_endline " | |¯|¯|¯|||¯|¯|¯| |";
			print_endline " | |-|-|-|||-|-|-| |";
			print_endline " | |_|_|_|||_|_|_| |";
			print_endline " | ||~~~| | |¯¯¯|| |";
			print_endline " | ||~~~|!|!| O || |";
			print_endline " | ||~~~| |.|___|| |";
			print_endline " | ||¯¯¯| | |¯¯¯|| |";
			print_endline " | ||   | | |   || |";
			print_endline " | ||___| | |___|| |";
			print_endline " | ||¯¯¯| | |¯¯¯|| |";
			print_endline " | ||   | | |   || |";
			print_endline " | ||___| | |___|| |";
			print_endline "|¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯|";
			print_endline " ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯"

		method use_sonic_screwdriver =
			print_endline "Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii"
		method regenerate = self#set_hp 100
		method attack (dal : Dalek.dalek) =
			print_endline ((self#get_name)^" attack "^(dal#get_name));
			dal#set_hp 0;
			dal#die
	end
