
class galifrey (peoples : People.people list) (doctors : Doctor.doctor list)
(daleks : Dalek.dalek list) =
	object (self)
		val _peoples = peoples
		val _doctors = doctors
		val _daleks = daleks

		method get_peoples = _peoples
		method get_doctors = _doctors
		method get_daleks = _daleks

		method get_random_daleks =
			let rec loop liste i n =
				if i = n then
					(List.hd liste)
				else
					loop (List.tl liste) (i + 1) n
			in loop (self#get_daleks) 0 (Random.int (List.length (self#get_daleks) - 1))

		method get_random_doctors =
			let rec loop liste i n =
				if i = n then
					(List.hd liste)
				else
					loop (List.tl liste) (i + 1) n
			in loop (self#get_doctors) 0 (Random.int (List.length (self#get_doctors) - 1))

		method do_time_war =
			let rec loop_dalek dalek_ peoples_ = match peoples_ with
				| head::tail ->
					dalek_#exterminate head;
					loop_dalek (self#get_random_daleks) tail
				| [] -> ()
			in loop_dalek (self#get_random_daleks) (self#get_peoples);
			
			let rec loop_doctor doctor_ daleks_ = match daleks_ with
				| head::tail ->
					doctor_#attack head;
					loop_doctor (self#get_random_doctors) tail
				| [] -> ()
			in loop_doctor (self#get_random_doctors) (self#get_daleks)
	end
