
class ['a] army (soldier: 'a) =
	object (self)
		val mutable _army = [soldier]

		method get_army = _army
		method set_army new_army = _army <- new_army
		method add (soldier : 'a) =
			print_endline ("Add a soldier "^soldier#get_name^" to the army");
(*			_army <- (List.append self#get_army [soldier]) *)
			self#set_army (List.append self#get_army [soldier])
		method delete = match self#get_army with
			| head::tail ->
				print_endline "Soldier delete from the army";
				self#set_army tail
			| [] -> print_endline "Army already empty"
	end
