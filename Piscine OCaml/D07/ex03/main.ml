
let people_army str =
	print_endline str;
	let p1 = new People.people "people1" in
	let p2 = new People.people "people2" in
	let p3 = new People.people "people3" in
	let p4 = new People.people "people4" in
	let p5 = new People.people "people5" in
	let a = new Army.army p1 in
	a#add p2;
	a#add p3;
	a#add p4;
	a#add p5;

	a#delete;
	a#delete;
	a#delete;
	a#delete;
	a#delete;
	a#delete

let doctor_army str =
	print_endline str;
	let p1 = new People.people "people1" in
	let d1 = new Doctor.doctor "doctor1" 3 p1 in
	let p2 = new People.people "people2" in
	let d2 = new Doctor.doctor "doctor2" 3 p2 in
	let p3 = new People.people "people3" in
	let d3 = new Doctor.doctor "doctor3" 3 p3 in
	let p4 = new People.people "people4" in
	let d4 = new Doctor.doctor "doctor4" 3 p4 in
	let p5 = new People.people "people5" in
	let d5 = new Doctor.doctor "doctor5" 3 p5 in
	let a = new Army.army d1 in
	a#add d2;
	a#add d3;
	a#add d4;
	a#add d5;

	a#delete;
	a#delete;
	a#delete;
	a#delete;
	a#delete;
	a#delete

let dalek_army str =
	print_endline str;
	let d1 = new Dalek.dalek in
	let d2 = new Dalek.dalek in
	let d3 = new Dalek.dalek in
	let d4 = new Dalek.dalek in
	let d5 = new Dalek.dalek in
	let a = new Army.army d1 in
	a#add d2;
	a#add d3;
	a#add d4;
	a#add d5;

	a#delete;
	a#delete;
	a#delete;
	a#delete;
	a#delete;
	a#delete

let () =
	Random.self_init();
	people_army "*******************PEOPLE************************";
	print_endline "\n";
	doctor_army "*******************DOCTOR************************";
	print_endline "\n";
	dalek_army "*******************DALEK************************";
