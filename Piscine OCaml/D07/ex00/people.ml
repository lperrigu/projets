
class people (name : string) =
	object (self)
		val _name = name
		val _hp = 100

		method get_name = _name
		method get_hp = _hp
		method to_string = (self#get_name)^" is a people with "^(string_of_int self#get_hp)
		method talk = print_endline ("I'm "^(self#get_name)^"! Do you know the Doctor?")
		method die = print_endline "Aaaarghh!"
		initializer print_endline (name^" is born")
	end
