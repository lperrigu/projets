
let rec converges func x n =
	if n < 0 then
		false
	else
		let rec loop func x n =
			if n = 0 then
				func x = x
			else
				loop func  ( func x ) ( n - 1 )
		in loop func ( func x ) ( n - 1 )

let () =

	print_endline "Test 1 de converges avec func = ( ( * ) 2 ), x = 2 et n = 5 :";
	if converges ( ( * ) 2 ) 2 5 then
		print_endline "Oui"
	else
		print_endline "Non";

	print_endline "Test 2 de converges avec func = x -> x / 2, x = 2 et n = 3 :";
	if converges ( fun x -> x / 2 ) 2 3 then
		print_endline "Oui"
	else
		print_endline "Non";

	print_endline "Test 3 de converges avec func = x -> x / 2, x = 2 et n = 2 :";
	if converges ( fun x -> x / 2 ) 2 2 then
		print_endline "Oui"
	else
		print_endline "Non";
