let rec tak x y z =
	if y < x then
		tak ( tak ( x - 1 ) y z ) ( tak (y - 1) z x ) ( tak ( z - 1 ) x y )
	else
		z

let () =
	print_endline "Test 1 de tak avec x = 1, y = 2 et z = 3 :";
	print_int ( tak 1 2 3 );
	print_char '\n';

	print_endline "Test 2 de tak avec x = 1, y = 2 et z = 3 :";
	print_int ( tak 5 23 7 );
	print_char '\n';

	print_endline "Test 3 de tak avec x = 1, y = 2 et z = 3 :";
	print_int ( tak 9 1 0 );
	print_char '\n';

	print_endline "Test 4 de tak avec x = 1, y = 2 et z = 3 :";
	print_int ( tak 1 1 1 );
	print_char '\n';

	print_endline "Test 5 de tak avec x = 1, y = 2 et z = 3 :";
	print_int ( tak 0 42 0 );
	print_char '\n';

	print_endline "Test 6 de tak avec x = 1, y = 2 et z = 3 :";
	print_int ( tak 23498 98734 98776 );
	print_char '\n'

