
let ft_sum func i n =
	if i > n then
		nan
	else
		let rec sum_aux i n acc func =
			if i = n then
				acc +. ( func i )
			else
				sum_aux ( i + 1 ) n ( acc +. ( func i ) ) func
		in sum_aux i n 0.0 func

let () =
	print_endline "Test 1 ,calcul de la somme des i de i = 1 jusqu'a 3 de i^2";
	print_float ( ft_sum ( fun i -> float_of_int ( i * i ) ) 1 3 );
	print_char '\n';

	print_endline "Test 3 ,calcul de la somme des i de i = 1 jusqu'a 10 de i^2";
	print_float ( ft_sum ( fun i -> float_of_int ( i * i ) ) 1 10 );
	print_char '\n';

	print_endline "Test 4 ,calcul de la somme des i de i = 1 jusqu'a 10 de i";
	print_float ( ft_sum ( fun i -> float_of_int ( i ) ) 1 10 );
	print_char '\n';

	print_endline "Test 4 ,calcul de la somme des i de i = 1 jusqu'a 100 de i";
	print_float ( ft_sum ( fun i -> float_of_int ( i ) ) 1 100 );
	print_char '\n'

