let repeat_string ?str:(s="x") n =
	if n < 0 then
		"Error"
	else
	begin
		let rec loop n s =
			if n = 0 then
				""
			else if n = 1 then
				s
			else
				s ^ ( loop  ( n - 1 ) s  )
		in
		loop n s
	end

let () =
	print_endline "Test 1 de repeat avec str = what n = -1 :";
	print_endline ( repeat_string ~str:"what" (-1) );

	print_endline "Test 2 de repeat avec str = what n = 3 :";
	print_endline ( repeat_string ~str:"what" 3 );

	print_endline "Test 3 de repeat avec str = what n = 0 :";
	print_endline ( repeat_string ~str:"what" 0 );

	print_endline "Test 4 de repeat avec n = -1 :";
	print_endline ( repeat_string  (-1) );

	print_endline "Test 5 de repeat avec n = 0 :";
	print_endline ( repeat_string  0 );

	print_endline "Test 6 de repeat avec n = 1 :";
	print_endline ( repeat_string  1 );

	print_endline "Test 7 de repeat avec n = 5 :";
	print_endline ( repeat_string  5 );

	print_endline "Test 8 de repeat avec str = allo n = 1 :";
	print_endline ( repeat_string ~str:"allo" 1 )
