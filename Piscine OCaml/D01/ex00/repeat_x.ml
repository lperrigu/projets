let repeat_x n =
	if n < 0 then
		"Error"
	else
	begin
		let rec loop n =
			if n = 0 then
				""
			else if n = 1 then
				"x"
			else
				"x" ^ loop ( n - 1 )
		in
		loop n
	end

let () =
	print_endline "Test 1 de repeat avec n = -1 :";
	print_endline ( repeat_x (-1) );

	print_endline "Test 1 de repeat avec n = 0 :";
	print_endline ( repeat_x 0 );

	print_endline "Test 1 de repeat avec n = 1 :";	
	print_endline ( repeat_x 1 );

	print_endline "Test 1 de repeat avec n = 4 :";
	print_endline ( repeat_x 4 );

	print_endline "Test 1 de repeat avec n = 12 :";
	print_endline ( repeat_x 12 )


