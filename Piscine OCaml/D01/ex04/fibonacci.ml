
let fibonacci n =
	let rec fibonacci_aux acc1 acc2 n =
		if n < 0 then
			(-1)
		else if n = 0 then
			acc1
		else
			fibonacci_aux acc2 (acc1 + acc2) (n - 1)
	in fibonacci_aux 0 1 n

let () =
	print_endline "Test 1 de Fibonacci avec n = -12 :";
	print_int ( fibonacci (-12) );
	print_char '\n';

	print_endline "Test 2 de Fibonacci avec n = 1 :";
	print_int ( fibonacci 1 );
	print_char '\n';

	print_endline "Test 3 de Fibonacci avec n = 3 :";
	print_int ( fibonacci 3 );
	print_char '\n';

	print_endline "Test 4 de Fibonacci avec n = 6 :";
	print_int ( fibonacci 6 );
	print_char '\n';

	print_endline "Test 5 de Fibonacci avec n = 1000 :";
	print_int ( fibonacci 1000 );
	print_char '\n'
