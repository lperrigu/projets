
let rec iter func x n =
	if n < 0 then
		-1
	else if n = 1 then
		func x
	else
		iter func ( func x ) ( n - 1 )

let () =

	print_endline "Test 1 de iter avec func = x -> x * x, x = 2, n = 4";
	print_int ( iter ( fun x -> x * x ) 2 4 );
	print_char '\n';

	print_endline "Test 2 de iter avec func = x -> x * 2, x = 2, n = 4";
	print_int ( iter ( fun x -> x * 2 ) 2 4 );
	print_char '\n';

	print_endline "Test 3 de iter avec func = x -> x * x, x = 20, n = 1";
	print_int ( iter ( fun x -> x * x ) 20 1 );
	print_char '\n';

	print_endline "Test 4 de iter avec func = x -> x * 2, x = 1, n = 10";
	print_int ( iter ( fun x -> x * 2 ) 1 10 );
	print_char '\n';

	print_endline "Test 5 de iter avec func = x -> x * x, x = 1, n = 42";
	print_int ( iter ( fun x -> x * x ) 1 10 );
	print_char '\n'
