let rec ackermann m n =
	if m < 0 || n < 0 then
		(-1)
	else if m = 0 then
		n + 1
	else if n = 0 then
		ackermann ( m - 1 ) 1
	else
		ackermann ( m - 1 ) ( ackermann  m (n - 1) )

let () =
	print_endline "Test 1 de ackermann avec m = -1 et n = 7 :";
	print_int (ackermann (-1) 7);
	print_char '\n';

	print_endline "Test 2 de ackermann avec m = 0 et n = 0 :";
	print_int (ackermann 0 0);
	print_char '\n';

	print_endline "Test 3 de ackermann avec m = 2 et n = 3 :";
	print_int (ackermann 2 3);
	print_char '\n';

	print_endline "Test 4 de ackermann avec m = 4 et n = 1 :";
	print_int (ackermann 4 1);
	print_char '\n'



