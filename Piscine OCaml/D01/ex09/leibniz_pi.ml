
let leibniz_pi delta =
	if delta < 0.0 then
		(-1)
	else
		let rec pi_aux delta acc i =
			if ( 4.0 *. acc ) -. ( 4.0 *. atan 1.0 ) <= delta
			&& ( 4.0 *. acc ) -. ( 4.0 *. atan 1.0 ) >= 0.0 then
				i
			else if ( 4.0 *. acc ) -. ( 4.0 *. atan 1.0 ) >= delta *. ( -1.0 )
			&& ( 4.0 *. acc ) -. ( 4.0 *. atan 1.0 ) <= 0.0 then
				i
			else if ( i + 1 ) mod 2 = 0 then
				pi_aux delta ( acc +. 1.0 /. float_of_int( (2 * ( i + 1 ) ) + 1 ) ) ( i + 1 )
			else
				pi_aux delta ( acc -. 1.0 /. float_of_int( (2 * ( i + 1 ) ) + 1 ) ) ( i + 1 )
		in pi_aux delta 1.0 0

let () =
	print_endline "Test 1 de leibniz_pi avec un delta de 0.1";
	print_int (leibniz_pi 0.1);
	print_char '\n';

	print_endline "Test 2 de leibniz_pi avec un delta de 0.01";
	print_int (leibniz_pi 0.01);
	print_char '\n';

	print_endline "Test 3 de leibniz_pi avec un delta de 0.001";
	print_int (leibniz_pi 0.001);
	print_char '\n';

	print_endline "Test 4 de leibniz_pi avec un delta de 0.0001";
	print_int (leibniz_pi 0.0001);
	print_char '\n';

	print_endline "Test 5 de leibniz_pi avec un delta de 0.00001";
	print_int (leibniz_pi 0.00001);
	print_char '\n';

	print_endline "Test 6 de leibniz_pi avec un delta de 0.000001";
	print_int (leibniz_pi 0.000001);
	print_char '\n';

	print_endline "Test 7 de leibniz_pi avec un delta de 0.0000001";
	print_int (leibniz_pi 0.0000001);
	print_char '\n';

	print_endline "Test 8 de leibniz_pi avec un delta de 0.00000001";
	print_int (leibniz_pi 0.00000001);
	print_char '\n'
