
let ft_print_rev s =
	let rec loop s current_index_string =
		if current_index_string >= 0 then
			begin
			print_char ( String.get s current_index_string );
			loop s ( current_index_string - 1)
			end
	in
	loop s ( ( String.length s ) - 1 );
	print_char '\n'


let main () =
	print_endline "Test 1 de ft_print_rev : Hello world !";
	ft_print_rev "Hello world !";

	print_endline "Test 2 de ft_print_rev : madam cars";
	ft_print_rev "madam cars";

	print_endline "Test 3 de ft_print_rev : a";
	ft_print_rev "a";

	print_endline "Test 4 de ft_print_rev : empty_string";
	ft_print_rev ""


let () = main ()
