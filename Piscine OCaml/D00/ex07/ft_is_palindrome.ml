
let ft_is_palindrome s =
	let rec loop s current_index_string =
		if ( String.length s ) - 1 < 0 then
			true
		else if current_index_string == ( String.length s ) - 1 then
			true
		else if String.get s current_index_string <>
		String.get s ( ( String.length s ) - 1 - current_index_string ) then
			false
		else
			loop s ( current_index_string + 1)
	in
	loop s 0


let main () =
    print_endline "Test 1 de ft_is_palindrome : kayak";
    if ft_is_palindrome "kayak" then
        print_endline "Oui"
    else
        print_endline "Nope";

	print_endline "Test 2 de ft_is_palindrome : kayyak";
    if ft_is_palindrome "kayyak" then
        print_endline "Oui"
    else
        print_endline "Nope";

    print_endline "Test 3 de ft_is_palindrome : nisioisin";
    if ft_is_palindrome "nisioisin" then
        print_endline "Oui"
    else
        print_endline "Nope";

	print_endline "Test 4 de ft_is_palindrome : maison";
	if ft_is_palindrome "maison" then
		print_endline "Oui"
	else
		print_endline "Nope";

	print_endline "Test 5 de ft_is_palindrome : a";
	if ft_is_palindrome "a" then
		print_endline "Oui"
	else
		print_endline "Nope";

	print_endline "Test 6 de ft_is_palindrome : ca";
	if ft_is_palindrome "ca" then
		print_endline "Oui"
	else
		print_endline "Nope";

	print_endline "Test 7 de ft_is_palindrome : aa";
	if ft_is_palindrome "aa" then
		print_endline "Oui"
	else
		print_endline "Nope";

	print_endline "Test 8 de ft_is_palindrome : empty_string";
	if ft_is_palindrome "" then
		print_endline "Oui"
	else
		print_endline "Nope"


let () = main ()
