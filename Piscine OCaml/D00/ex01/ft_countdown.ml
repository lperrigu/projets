
let ft_countdown n =
	if ( n <= 0 ) then
		begin
			print_int 0;
			print_char '\n'
		end
	else
		begin
			let rec loop m =
				if m = 0 then
					begin
						print_int m;
						print_char '\n'
					end
				else
					begin
						print_int m;
						print_char '\n';
						loop (m - 1)
					end
			in
			loop n
		end


let main () =
	print_endline "Test 1 de ft_countdown : 12";
	ft_countdown 12;
	print_char '\n';	

	print_endline "Test 2 de ft_countdown : 7";
	ft_countdown 7;
	print_char '\n';

	print_endline "Test 3 de ft_countdown : 0";
	ft_countdown 0;
	print_char '\n';

	print_endline "Test 4 de ft_countdown : -30";
	ft_countdown (-30);
	print_char '\n'


let () = main ()
