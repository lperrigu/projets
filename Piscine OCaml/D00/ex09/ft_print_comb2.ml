let ft_print_comb () =
	let rec loop_first_digit current_first_digit =
		if current_first_digit <= 98 then
			let rec loop_second_digit first_digit current_second_digit =
				if current_second_digit <= 99 && first_digit <> 98 then
begin
					print_int first_digit;
					print_char ' ';
					print_int current_second_digit;
					print_char ',';
					print_char ' ';
					loop_second_digit first_digit ( current_second_digit + 1 )
end
				else if current_second_digit <= 99 && first_digit = 98 then
begin
					print_int first_digit;
					print_char ' ';
					print_int current_second_digit;
					print_char '\n';
					loop_second_digit first_digit ( current_second_digit + 1 )
end
			in
				loop_second_digit current_first_digit (current_first_digit + 1);
			loop_first_digit (current_first_digit + 1)
	in
		loop_first_digit 0


let main () =
	print_endline "Test de ft_print_comb :";
	ft_print_comb ()


let () = main ()
