
let ft_print_comb () =
	let rec loop_first_digit current_first_digit =
		if current_first_digit <= 7 then
			let rec loop_second_digit first_digit current_second_digit =
				if current_second_digit <= 8 then
					let rec loop_third_digit first_digit second_digit current_third_digit =
						if (current_third_digit <= 9) && (first_digit <> 7) then
							begin
							print_int first_digit;
							print_int second_digit;
							print_int current_third_digit;
							print_string ", ";
							loop_third_digit first_digit second_digit
							( current_third_digit + 1 )
							end
						else if (current_third_digit <= 9) && (first_digit = 7) then
							begin
							print_int first_digit;
							print_int second_digit;
							print_int current_third_digit;
							print_string "\n";
							loop_third_digit first_digit second_digit
							( current_third_digit + 1 )
							end
					in
						loop_third_digit first_digit current_second_digit
						(current_second_digit + 1 );
					loop_second_digit first_digit ( current_second_digit + 1 )
			in
				loop_second_digit current_first_digit (current_first_digit + 1);
			loop_first_digit (current_first_digit + 1)
	in
		loop_first_digit 0


let main () =
	print_endline "Test de ft_print_comb :";
	ft_print_comb ()


let () = main ()
