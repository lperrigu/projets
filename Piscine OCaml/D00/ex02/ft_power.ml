
let ft_power value power =
	if ( value = 0 ) then
		0
	else if ( power = 0 ) then
		1
	else
		begin
			let rec loop value power =
					if power = 1 then
						value
					else
						value * ( loop value ( power - 1 ) )
			in
			loop value power
		end


let main () =
	print_endline "Test 1 de ft_power : 12 puissance 2";
	print_int (ft_power 12 2);
	print_char '\n';

	print_endline "Test 2 de ft_power : 2 puissance 10";
	print_int (ft_power 2 10);
	print_char '\n';

	print_endline "Test 3 de ft_power : 0 puissance 37";
	print_int (ft_power 0 37);
	print_char '\n';

	print_endline "Test 4 de ft_power : 100 puissance 0";
	print_int (ft_power 100 0);
	print_char '\n'


let () = main ()
