
let ft_string_all func s =
	let rec loop s func current_index_string =
		if current_index_string< 0 then
			true
		else if func ( String.get s current_index_string ) = false then
			false
		else if current_index_string = 0 then
			true
		else
			loop s func ( current_index_string - 1)
	in
	loop s func ( ( String.length s ) - 1 )


let is_digit c =
	c >= '0' && c <= '9'

let main () =
	print_endline "Test 1 de ft_string_all : 23456789654 avec is_digit";
	if ft_string_all is_digit "23456789654" then
		print_endline "Oui"
	else
		print_endline "Nope";

	print_endline "Test 2 de ft_string_all : D23456789654 avec is_digit";
	if ft_string_all is_digit "D23456789654" then
		print_endline "Oui"
	else
		print_endline "Nope";

	print_endline "Test 3 de ft_string_all : D2345678F9654Z avec is_digit";
	if ft_string_all is_digit "D2345678F9654Z" then
		print_endline "Oui"
	else
		print_endline "Nope";

	print_endline "Test 3 de ft_string_all : empty_string avec is_digit";
	if ft_string_all is_digit "" then
		print_endline "Oui"
	else
		print_endline "Nope"



let () = main ()
