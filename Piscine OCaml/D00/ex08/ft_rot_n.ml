let transform_char c =
	if c = 'z' then
		'a'
	else if c = 'Z' then
		'A'
	else if c <= 'z' && c >= 'a' then
		char_of_int( int_of_char c + 1 )
	else if c <= 'Z' && c >= 'A' then
		char_of_int( int_of_char c + 1 )
	else
		c

let ft_rot_n n s =
	let rec loop current_n s =
		if current_n = 0 then
			s
		else if current_n > 1 then
			loop ( current_n - 1 ) (String.map transform_char s)
		else
			String.map transform_char s
	in
	loop n s


let main () =
	print_endline "Test 1 de ft_print_rev : alphabet avec decalage de 1";
	print_endline ( ft_rot_n 1 "abcdefghijklmnopqrstuvwxyz" );

	print_endline "Test 2 de ft_print_rev : alphabet avec decalage de 13";
	print_endline ( ft_rot_n 13 "abcdefghijklmnopqrstuvwxyz" );

	print_endline "Test 3 de ft_print_rev : empty_string";
	print_endline ( ft_rot_n 1 "" );

	print_endline "Test 4 de ft_print_rev :NBzlk qnbjr ! avec decalage de 1";
	print_endline ( ft_rot_n 1 "NBzlk qnbjr !" );

	print_endline "Test 5 de ft_print_rev : On change rien sans decalage";
	print_endline ( ft_rot_n 0 "On change rien" )

let () = main ()
