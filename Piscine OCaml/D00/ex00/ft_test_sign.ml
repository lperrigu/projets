
let ft_test_sign n =
	if n < 0 then
		print_endline "negative"
	else
		print_endline "positive"


let main () =
	print_endline "Test 1 de ft_test_sign : 30";
	ft_test_sign 30;
	print_char '\n';

	print_endline "Test 2 de ft_test_sign : 0";
	ft_test_sign 0;
	print_char '\n';

	print_endline "Test 3 de ft_test_sign : -30";
	ft_test_sign (-30);
	print_char '\n'


let () = main ()
