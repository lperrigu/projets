global	_ft_tolower

_ft_tolower:
	cmp rdi, 65
	jl nochange
	cmp rdi, 90
	jle lower

nochange:
	mov rax, rdi
	jmp end

lower:
	add rdi, 32
	mov rax, rdi
	jmp end
	
end:
	ret