global	_ft_toupper

_ft_toupper:
	cmp rdi, 97
	jl nochange
	cmp rdi, 122
	jle upper

nochange:
	mov rax, rdi
	jmp end

upper:
	sub rdi, 32
	mov rax, rdi
	jmp end
	
end:
	ret