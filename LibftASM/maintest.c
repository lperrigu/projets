/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   maintest.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/17 15:59:13 by lperrigu          #+#    #+#             */
/*   Updated: 2015/05/14 16:37:09 by lperrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>

void	ft_bzero(void *s, size_t n);
size_t	ft_strlen(const char *s);
char	*ft_strcat(char *s1, const char *s2);
void	*ft_memset(void *b, int c, size_t len);
void	*ft_memcpy(void *dst, const void *src, size_t n);
int		ft_isalpha(int c);
int		ft_isdigit(int c);
int		ft_isalnum(int c);
int		ft_isascii(int c);
int		ft_isprint(int c);
int		ft_toupper(int c);
int		ft_tolower(int c);
int		ft_puts(const char *s);
void	ft_cat(int fd);
char	*ft_strdup(const char *s1);

int main(void)
{
	int i;
	int fd;
	char *s1;
	char *s2;
	char *s3;
	char *s4;

	s1 = malloc(10);
	s2 = malloc(10);
/* ************************************************************************** */
/* **************************************STRLEN****************************** */
/* ************************************************************************** */

	printf("STRLEN:\n");
	printf("|%lu|\n", ft_strlen(""));
	printf("|%lu|\n", strlen(""));
	printf("|%lu|\n", ft_strlen("1"));
	printf("|%lu|\n", strlen("1"));
	printf("|%lu|\n", ft_strlen("12345"));
	printf("|%lu|\n", strlen("12345"));
	printf("|%lu|\n", ft_strlen("1234567890123456789012345678901234567890123"));
	printf("|%lu|\n\n\n", strlen("1234567890123456789012345678901234567890123"));


/* ************************************************************************** */
/* **************************************BZERO******************************* */
/* ************************************************************************** */

	printf("BZERO:\n");
	bzero((void *)s1, 10);
	ft_bzero((void *)s2, 10);

	for(i = 0; i < 10; ++i)
		printf("s1[i] = %d s2[i] = %d i = %d\n", s1[i], s2[i], i);
	printf("\n\n");

/* ************************************************************************** */
/* **************************************STRCAT****************************** */
/* ************************************************************************** */

	free(s1);
	free(s2);
	s1 = strdup("test1");
	s2 = strdup("test2");

	s3 = strcat(s1, "xyz");
	s4 = ft_strcat(s2, "xyz");
	printf("STRCAT:\n");
	printf("s1 : |%s|\n", s1);
	printf("s2 : |%s|\n", s2);
	printf("s1 = %p return de strcat = %p\n", s1, s3);
	printf("s2 = %p return de ft_strcat = %p\n\n\n", s2, s4);
	free(s1);
	free(s2);

/* ************************************************************************** */
/* **************************************MEMSET****************************** */
/* ************************************************************************** */

	s1 = malloc(10);
	s2 = malloc(10);
	s3 = memset((void *)s1, '$', 10);
	s4 = ft_memset((void *)s2, '$', 10);
	printf("MEMSET:\n");
	for(i = 0; i < 10; ++i)
		printf("s1[i] = %c s2[i] = %c i = %d\n", s1[i], s2[i], i);
	printf("s1 = %p return de memset = %p\n", s1, s3);
	printf("s2 = %p return de ft_memset = %p\n\n\n", s2, s4);

/* ************************************************************************** */
/* **************************************IS_STUFF**************************** */
/* ************************************************************************** */

	printf("ISALPHA:\n");
	i = isalpha('a');
	printf("Return de isalpha avec a en entree: %d\n", i);
	i = ft_isalpha('a');
	printf("Return de ft_isalpha avec a en entree: %d\n", i);
	i = isalpha('A');
	printf("Return de isalpha avec A en entree: %d\n", i);
	i = ft_isalpha('A');
	printf("Return de ft_isalpha avec A en entree: %d\n", i);
	i = isalpha('z');
	printf("Return de isalpha avec z en entree: %d\n", i);
	i = ft_isalpha('z');
	printf("Return de ft_isalpha avec z en entree: %d\n", i);
	i = isalpha('Z');
	printf("Return de isalpha avec Z en entree: %d\n", i);
	i = ft_isalpha('Z');
	printf("Return de ft_isalpha avec Z en entree: %d\n", i);
	i = isalpha('f');
	printf("Return de isalpha avec f en entree: %d\n", i);
	i = ft_isalpha('f');
	printf("Return de ft_isalpha avec f en entree: %d\n", i);
	i = isalpha('F');
	printf("Return de isalpha avec F en entree: %d\n", i);
	i = ft_isalpha('F');
	printf("Return de ft_isalpha avec F en entree: %d\n", i);
	i = isalpha('5');
	printf("Return de isalpha avec 5 en entree: %d\n", i);
	i = ft_isalpha('5');
	printf("Return de ft_isalpha avec 5 en entree: %d\n\n", i);

	printf("ISDIGIT:\n");
	i = isdigit('0');
	printf("Return de isdigit avec 0 en entree: %d\n", i);
	i = ft_isdigit('0');
	printf("Return de ft_isdigit avec 0 en entree: %d\n", i);
	i = isdigit('9');
	printf("Return de isdigit avec 9 en entree: %d\n", i);
	i = ft_isdigit('9');
	printf("Return de ft_isdigit avec 9 en entree: %d\n", i);
	i = isdigit('5');
	printf("Return de isdigit avec 5 en entree: %d\n", i);
	i = ft_isdigit('5');
	printf("Return de ft_isdigit avec 5 en entree: %d\n", i);
	i = isdigit('$');
	printf("Return de isdigit avec $ en entree: %d\n", i);
	i = ft_isdigit('$');
	printf("Return de ft_isdigit avec $ en entree: %d\n\n", i);

	printf("ISALNUM:\n");
	i = isalnum('5');
	printf("Return de isalnum avec 5 en entree: %d\n", i);
	i = ft_isalnum('5');
	printf("Return de ft_isalnum avec 5 en entree: %d\n", i);
	i = isalnum('W');
	printf("Return de isalnum avec W en entree: %d\n", i);
	i = ft_isalnum('W');
	printf("Return de ft_isalnum avec W en entree: %d\n", i);
	i = isalnum('w');
	printf("Return de isalnum avec w en entree: %d\n", i);
	i = ft_isalnum('w');
	printf("Return de ft_isalnum avec w en entree: %d\n", i);
	i = isalnum('&');
	printf("Return de isalnum avec & en entree: %d\n", i);
	i = ft_isalnum('&');
	printf("Return de ft_isalnum avec & en entree: %d\n\n", i);

	printf("ISPRINT:\n");
	i = isprint(32);
	printf("Return de isprint avec 32 en entree: %d\n", i);
	i = ft_isprint(32);
	printf("Return de ft_isprint avec 32 en entree: %d\n", i);
	i = isprint(12);
	printf("Return de isprint avec 12 en entree: %d\n", i);
	i = ft_isprint(12);
	printf("Return de ft_isprint avec 12 en entree: %d\n", i);
	i = isprint(126);
	printf("Return de isprint avec 126 en entree: %d\n", i);
	i = ft_isprint(126);
	printf("Return de ft_isprint avec 126 en entree: %d\n", i);
	i = isprint(127);
	printf("Return de isprint avec 127 en entree: %d\n", i);
	i = ft_isprint(127);
	printf("Return de ft_isprint avec 127 en entree: %d\n\n", i);

	printf("ISASCII:\n");
	i = isascii(-1);
	printf("Return de isascii avec -1 en entree: %d\n", i);
	i = ft_isascii(-1);
	printf("Return de ft_isascii avec -1 en entree: %d\n", i);
	i = isascii(0);
	printf("Return de isascii avec 0 en entree: %d\n", i);
	i = ft_isascii(0);
	printf("Return de ft_isascii avec 0 en entree: %d\n", i);
	i = isascii(127);
	printf("Return de isascii avec 127 en entree: %d\n", i);
	i = ft_isascii(127);
	printf("Return de ft_isascii avec 127 en entree: %d\n", i);
	i = isascii(128);
	printf("Return de isascii avec 128 en entree: %d\n", i);
	i = ft_isascii(128);
	printf("Return de ft_isascii avec 128 en entree: %d\n\n\n", i);

/* ************************************************************************** */
/* **************************************TOUPPER***************************** */
/* ************************************************************************** */

	printf("TOUPPER:\n");
	printf("ft_toupper('a') : %c\n", ft_toupper('a'));
	printf("ft_toupper('z') : %c\n", ft_toupper('z'));
	printf("ft_toupper('d') : %c\n", ft_toupper('d'));
	printf("ft_toupper('R') : %c\n", ft_toupper('R'));
	printf("ft_toupper('!') : %c\n\n\n", ft_toupper('!'));

/* ************************************************************************** */
/* **************************************TOLOWER***************************** */
/* ************************************************************************** */

	printf("TOLOWER:\n");
	printf("ft_tolower('A') : %c\n", ft_tolower('A'));
	printf("ft_tolower('Z') : %c\n", ft_tolower('Z'));
	printf("ft_tolower('D') : %c\n", ft_tolower('D'));
	printf("ft_tolower('r') : %c\n", ft_tolower('r'));
	printf("ft_tolower('!') : %c\n\n\n", ft_tolower('!'));

/* ************************************************************************** */
/* **************************************PUTS******************************** */
/* ************************************************************************** */

	printf("PUTS:\n");
	puts("|Ceci est l'argument de puts|");
	ft_puts("|Ceci est l'argument de ft_puts|");
	puts(NULL);
	ft_puts(NULL);
	printf("\n\n");

/* ************************************************************************** */
/* **************************************MEMCPY****************************** */
/* ************************************************************************** */

	free(s1);
	free(s2);
	s1 = malloc(10);
	s1[9] = '\0';
	s2 = malloc(10);
	s2[9] = '\0';
	s3 = memcpy((void *)s1, "123456789", 9);
	s4 = ft_memcpy((void *)s2, "123456789", 9);
	printf("MEMCPY:\n");
	printf("|%s|\n", s1);
	printf("|%s|\n", s2);
	printf("s1 = %p return de memcpy = %p\n", s1, s3);
	printf("s2 = %p return de memcpy = %p\n\n\n", s2, s4);
	free(s1);
	free(s2);

/* ************************************************************************** */
/* **************************************STRDUP****************************** */
/* ************************************************************************** */

	printf("STRDUP:\n");
	s1 = strdup("123456789012345678901234567890");
	s2 = ft_strdup("123456789012345678901234567890");
	printf("|%s| a l'adresse %p\n", s1, s1);
	printf("|%s| a l'adresse %p\n\n\n", s2, s2);

/* ************************************************************************** */
/* **************************************CAT********************************* */
/* ************************************************************************** */

	printf("CAT:\n");
	fd = open("maintest.c", O_RDONLY);
	ft_cat(fd);
	printf("\nFIN DU MAIN DE TEST\n");
	return 0;
}
