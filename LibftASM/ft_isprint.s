global	_ft_isprint

_ft_isprint:
	cmp rdi, 32
	jl zero
	cmp rdi, 126
	jle one

zero:
	mov rax, 0
	jmp end

one:
	mov rax, 1
	jmp end
	
end:
	ret