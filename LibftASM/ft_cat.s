
section .data
	buffer times 256 db 0
	buffersize equ $ - buffer

section .text
	global	_ft_cat
	extern _ft_bzero

_ft_cat:
	push rdi

loop:
	lea rdi, [rel buffer]
	mov rsi, buffersize
	call _ft_bzero
	pop rdi
	lea rsi, [rel buffer]
	mov rdx, buffersize
	mov rax, 0x2000003
	syscall
	jc end
	cmp rax, 0
	je end
	push rdi
	mov rdi, 1
	lea rsi, [rel buffer]
	mov rdx, buffersize
	mov rax, 0x2000004
	syscall
	jc end
	jmp loop

end:
	mov rax, 0
	ret

