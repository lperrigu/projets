global	_ft_isdigit

_ft_isdigit:
	cmp rdi, 48
	jl zero
	cmp rdi, 57
	jle one

zero:
	mov rax, 0
	jmp end

one:
	mov rax, 1
	jmp end
	
end:
	ret