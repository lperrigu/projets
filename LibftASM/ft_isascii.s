global	_ft_isascii

_ft_isascii:
	cmp rdi, 0
	jl zero
	cmp rdi, 127
	jle one

zero:
	mov rax, 0
	jmp end

one:
	mov rax, 1
	jmp end
	
end:
	ret