global	_ft_strlen

_ft_strlen:
	mov rax, 0
	cmp rdi, 0
	je end
	mov rcx, rdi
	cld
	repne scasb
	sub rdi, rcx
	mov rax, rdi
	dec rax
	dec rax
	shr rax, 1

end:
	ret
