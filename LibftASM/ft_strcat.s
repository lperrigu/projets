global	_ft_strcat

_ft_strcat:
	cmp rdi, 0
	je end
	mov rbx, rdi
	mov rcx, rsi

loop1:
	cmp byte[rbx], 0
	je loop2
	inc rbx
	jmp loop1

loop2:
	cmp rcx, 0
	je zero_term
	cmp byte[rcx], 0
	je zero_term
	mov r8, [rcx]
	mov [rbx], r8
	inc rbx
	inc rcx
	jmp loop2

zero_term:
	mov byte[rbx], 0

end:
	mov rax, rdi
	ret