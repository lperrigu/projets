global	_ft_isalpha

_ft_isalpha:
	cmp rdi, 65
	jl zero
	cmp rdi, 90
	jle one
	cmp rdi, 97
	jl zero
	cmp rdi, 122
	jle one

zero:
	mov rax, 0
	jmp end

one:
	mov rax, 1
	jmp end
	
end:
	ret