global	_ft_memset

_ft_memset:
	mov rax, rsi
	mov rcx, rdx
	mov r8, rdi
	cld
	rep stosb
	mov rax, r8
end:
	ret
