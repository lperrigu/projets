
section .data
	newline db 0x0a
	nullmsg db "(null)"

section .text
	global	_ft_puts
	extern _ft_strlen

_ft_puts:
	cmp rdi, 0
	je null
	push rdi
	call _ft_strlen

	pop rsi
	mov rdi, 1
	mov rdx, rax
	mov rax, 0x2000004
	syscall

	mov rdx, 1
	mov rax, 0x2000004
	lea rsi, [rel newline]
	syscall
	jmp end

null:
	mov rdi, 1
	lea rsi, [rel nullmsg]
	mov rdx, 6
	mov rax, 0x2000004
	syscall

	mov rdx, 1
	mov rax, 0x2000004
	lea rsi, [rel newline]
	syscall

end:
	ret

