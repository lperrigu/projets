#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/09 12:09:41 by lperrigu          #+#    #+#              #
#    Updated: 2015/05/10 19:15:03 by lperrigu         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = libfts.a

#SRCS = ft_strlen.s ft_bzero.s

OBJS = ft_strlen.o ft_bzero.o ft_strcat.o ft_memset.o ft_memcpy.o ft_isalpha.o\
		ft_isdigit.o ft_isalnum.o ft_isascii.o ft_isprint.o ft_toupper.o\
		ft_tolower.o ft_strdup.o ft_puts.o ft_cat.o

all: $(NAME)

$(NAME):
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_strlen.s -o ft_strlen.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_bzero.s -o ft_bzero.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_strcat.s -o ft_strcat.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_memset.s -o ft_memset.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_memcpy.s -o ft_memcpy.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_isalpha.s -o ft_isalpha.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_isdigit.s -o ft_isdigit.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_isalnum.s -o ft_isalnum.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_isascii.s -o ft_isascii.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_isprint.s -o ft_isprint.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_toupper.s -o ft_toupper.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_tolower.s -o ft_tolower.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_strdup.s -o ft_strdup.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_puts.s -o ft_puts.o
	 ~/.brew/Cellar/nasm/2.11.06/bin/nasm -f macho64 ft_cat.s -o ft_cat.o
	ar rc $(NAME) $(OBJS)
	ranlib $(NAME)

clean:
	rm -f $(OBJS)

fclean: clean
	rm -f $(NAME)

re: fclean all
