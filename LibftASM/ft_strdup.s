
section .text
	global	_ft_strdup
	extern	_malloc
	extern _ft_strlen

_ft_strdup:
	mov rax, 0
	cmp rdi, 0
	je end
	push rdi
	call _ft_strlen
	inc rax
	push rax
	call _malloc

	pop rcx
	mov rdi, rax
	pop rsi
	cld
	rep movsb
end:
	ret

