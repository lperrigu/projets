// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 12:59:23 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 21:29:44 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"

int		main( void )
{
	Bureaucrat	b1( "George", 149 );
	Bureaucrat	b2( "Claude", 2 );

	std::cout << b1 << std::endl;
	b1.DecreaseGrade();
	std::cout << b1 << std::endl;
	try
	{
		b1.DecreaseGrade();
	}
	catch (Bureaucrat::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << b1 << std::endl;

	std::cout << b2 << std::endl;
	b2.IncreaseGrade();
	std::cout << b2 << std::endl;
	try
	{
		b2.IncreaseGrade();
	}
	catch (Bureaucrat::GradeTooHighException e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << b2 << std::endl;
	return 0;
}
