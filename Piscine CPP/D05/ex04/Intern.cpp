// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Intern.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/23 19:42:36 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 20:33:58 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Intern.hpp"


Intern::Intern()
{

}

Form	*Intern::MakeForm( std::string form_name, std::string target ) const
{
	Form *ret = NULL;

	if ( form_name == "robotomy request" )
	{
		RobotomyRequestForm *robo = new RobotomyRequestForm( target );

		ret = robo;
		std::cout << "Intern creates " << *ret << std::endl;
	}
	else if ( form_name == "shrubbery creation" )
	{
		ShrubberyCreationForm *shrub = new ShrubberyCreationForm( target );
		ret = shrub;
		std::cout << "Intern creates " << *ret << std::endl;
	}
	else if ( form_name == "presidential pardon" )
	{
		PresidentialPardonForm *pres = new PresidentialPardonForm( target );

		ret = pres;
		std::cout << "Intern creates " << *ret << std::endl;
	}
	else
		std::cout << "Intern don't know this form" << std::endl;
	return ret;
}

Intern::~Intern()
{
	
}
