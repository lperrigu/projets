// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   OfficeBlock.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/23 20:38:16 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 21:50:19 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "OfficeBlock.hpp"


OfficeBlock::OfficeBlock( void ) : _intern( NULL ), _signer( NULL ),
								   _executor( NULL )
{

}

OfficeBlock::OfficeBlock( Intern *I, Bureaucrat *S, Bureaucrat *E ) :
_intern( I ), _signer( S ), _executor( E )
{

}

OfficeBlock::~OfficeBlock( void )
{
	
}

void	OfficeBlock::setIntern( Intern &I )
{
	this->_intern = &I;
}
void	OfficeBlock::setSigner( Bureaucrat &S )
{
	this->_signer = &S;
}

void	OfficeBlock::setExecutor( Bureaucrat &E )
{
	this->_executor = &E;
}

void	OfficeBlock::doBureaucracy( std::string form_name, std::string target )
{
	Form	*f;

	if ( this->_intern == NULL || this->_signer == NULL || this->_executor == NULL )
	{
		std::cout << "The office block is not complete"
				  << "to make some Bureaucracy stuff" << std::endl;
		return ;
	}
	f = this->_intern->MakeForm( form_name, target );
	if (f == NULL)
		return ;
	f->beSigned( *( this->_signer ) );
	f->execute( *( this->_executor ) );
}
