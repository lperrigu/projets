// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   PresidentialPardonForm.hpp                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 17:12:08 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 19:10:59 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PRESIDENTIALPARDONFORM_HPP
#define PRESIDENTIALPARDONFORM_HPP

#include "Form.hpp"

class PresidentialPardonForm : public Form
{
private:
	std::string	_target;

	PresidentialPardonForm( void );
	PresidentialPardonForm( PresidentialPardonForm const &src );
	PresidentialPardonForm& operator=( PresidentialPardonForm const &src );

public:
	PresidentialPardonForm( std::string target );
	virtual ~PresidentialPardonForm( void );

	void	execute( Bureaucrat const &executor ) const;

};


#endif
