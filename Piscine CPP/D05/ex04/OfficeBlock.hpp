// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   OfficeBlock.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/23 20:35:24 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 21:36:34 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef OFFICEBLOCK_HPP
#define OFFICEBLOCK_HPP

#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"

class OfficeBlock
{
private:
	Intern	*_intern;
	Bureaucrat	*_signer;
	Bureaucrat	*_executor;

	OfficeBlock(OfficeBlock const &);
	OfficeBlock& operator=(OfficeBlock const &);

public:
	OfficeBlock( Intern *I, Bureaucrat *S, Bureaucrat *E );
	OfficeBlock( void );
	virtual ~OfficeBlock( void );

	void	setIntern( Intern &I );
	void	setSigner( Bureaucrat &S );
	void	setExecutor( Bureaucrat &E );

	void	doBureaucracy( std::string form_name, std::string target );

};


#endif
