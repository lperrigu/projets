// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 12:59:23 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 21:45:55 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Form.hpp"
#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"
#include "OfficeBlock.hpp"

int		main( void )
{
	Bureaucrat	b1( "George", 1 );
	Bureaucrat	b2( "Claude", 1 );
	Intern		i;
	Bureaucrat	*b3 = new Bureaucrat( "John", 100 );
	Bureaucrat	*b4 = new Bureaucrat( "Little mac", 50 );
	Intern		*i2 = new Intern();
	OfficeBlock	OB1(i2, b3, b4);
	OfficeBlock	OB2;

	srand( time( NULL ) );
	OB1.doBureaucracy( "shrubbery creation", "WallStreet" );
	try
	{
		OB1.doBureaucracy( "presidential pardon", "Joseph Staline" );
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}
	OB2.setIntern(i);
	OB2.setSigner(b1);
	OB2.setExecutor(b2);
	OB2.doBureaucracy( "shrubbery creation", "Budapest" );
	OB2.doBureaucracy( "robotomy request", "Ridley" );
	OB2.doBureaucracy( "robotomy request", "Megaman" );
	OB2.doBureaucracy( "robotomy request", "Steve Austin" );
	OB2.doBureaucracy( "presidential pardon", "petrole" );
	OB2.doBureaucracy( "Don't exist", "Jean Francois Coppe" );
	return 0;
}
