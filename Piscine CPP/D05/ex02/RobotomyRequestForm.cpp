// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   RobotomyRequestForm.cpp                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 17:10:57 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 19:37:53 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "RobotomyRequestForm.hpp"

RobotomyRequestForm::RobotomyRequestForm( std::string target ) :
	Form( "RobotomyRequestForm", 72, 45 ), _target( target )
{

}

void	RobotomyRequestForm::execute( Bureaucrat const &executor ) const
{
	if ( this->getSigned() == true )
	{
		if ( executor.getGrade() > this->getGradeToSign() )
			throw Form::GradeTooLowException();
		if ( executor.executeForm( *this ) )
		{
			std::cout << "* vrrrr vrrrr vrrrr *" << std::endl;
			if (rand() % 2 == 0)
				std::cout << this->_target
						  << " has been robotomized successfully."
						  << std::endl;
			else
				std::cout << "It's a failure." << std::endl;
		}
	}
}

RobotomyRequestForm::~RobotomyRequestForm()
{
	
}
