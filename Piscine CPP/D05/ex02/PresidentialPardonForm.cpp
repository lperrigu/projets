// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   PresidentialPardonForm.cpp                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 17:14:04 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 19:38:11 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm( std::string target) :
	Form( "PresidentialPardonForm", 25, 5 ), _target( target )
{

}

void	PresidentialPardonForm::execute( Bureaucrat const &executor ) const
{
	if ( this->getSigned() == true )
	{
		if ( executor.getGrade() > this->getGradeToSign() )
			throw Form::GradeTooLowException();
		if ( executor.executeForm( *this ) )
		{
			std::cout << this->_target
					  << " has been pardoned by Zafod Beeblebrox."
					  << std::endl;
		}
	}
}

PresidentialPardonForm::~PresidentialPardonForm()
{
	
}
