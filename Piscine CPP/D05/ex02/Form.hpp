// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Form.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 13:29:37 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 17:40:49 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FORM_HPP
#define FORM_HPP

#include <iostream>
#include "Bureaucrat.hpp"

class Bureaucrat;

class Form
{
private:
	std::string const	_name;
	bool				_signed;
	int const			_gradeToSign;
	int const			_gradeToExec;

	Form( void );

public:
	Form( std::string const name, int gradeToSign, int gradeToExec );
	virtual ~Form( void );
	Form( Form const &src );

	std::string		getName( void ) const;
	bool			getSigned( void ) const;
	int				getGradeToSign( void ) const;
	int				getGradeToExec( void ) const;

	void			beSigned( Bureaucrat &b );
	virtual void	execute( Bureaucrat const &executor ) const = 0;

	Form	&operator=( Form const &src );

	class	GradeTooHighException : public std::exception
	{
	private:
		GradeTooHighException	&operator=( GradeTooHighException const &src );

	public:
		GradeTooHighException( void );
		GradeTooHighException( GradeTooHighException const &src );
		virtual ~GradeTooHighException( void ) throw();
		virtual const char		*what() const throw();
	};

	class	GradeTooLowException : public std::exception
	{
	private:
		GradeTooLowException	&operator=( GradeTooLowException const &src );

	public:
		GradeTooLowException( void );
		GradeTooLowException( GradeTooLowException const &src );
		virtual ~GradeTooLowException( void ) throw();
		virtual const char		*what() const throw();
	};
};

std::ostream		&operator<<(std::ostream &o, Form const &src);

#endif
