// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 12:59:23 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 19:35:41 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
//#include "Form.hpp"

int		main( void )
{
	Bureaucrat	b1( "Claude", 40 );
	Bureaucrat	b2( "George", 1 );
	ShrubberyCreationForm	S( "End of time" );
	RobotomyRequestForm		R( "Ayla" );
	PresidentialPardonForm	P( "Edward Snowden" );

	srand( time ( NULL ) );
	S.beSigned( b2 );
	R.beSigned( b2 );
	P.beSigned( b2 );
	std::cout << S << std::endl;
	std::cout << R << std::endl;
	std::cout << P << std::endl;

	S.execute( b1 );
	R.execute( b1 );
	R.execute( b1 );
	R.execute( b1 );
	R.execute( b1 );
	try
	{
		P.execute( b1 );
	}
	catch (std::exception e)
	{
		std::cout << "Error" << std::endl;
	}
	P.execute( b2 );
	return 0;
}
