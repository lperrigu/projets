// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Bureaucrat.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 11:48:15 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 19:39:50 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat( void ) :
	_name( "default" ), _grade( 150 )
{
	std::cout << "Bureaucrat default constructor called" << std::endl;
}

Bureaucrat::~Bureaucrat( void )
{
	std::cout << "Bureaucrat default destructor called" << std::endl;
}

Bureaucrat::Bureaucrat( std::string const name, int grade ) :
	_name( name ), _grade( grade )
{
	std::cout << "Bureaucrate params constructor called" << std::endl;
}

Bureaucrat::Bureaucrat( Bureaucrat const &src ) :
	_name( src.getName() ), _grade( src.getGrade() )
{
	std::cout << "Bureaucrate copy constructor called" << std::endl;
}

Bureaucrat		&Bureaucrat::operator=( Bureaucrat const &src )
{
	this->_grade = src.getGrade();
	return *this;
}

Bureaucrat::GradeTooHighException::GradeTooHighException( void )
{
}

Bureaucrat::GradeTooHighException::GradeTooHighException
( GradeTooHighException const &src )
{
	*this = src;
}

Bureaucrat::GradeTooHighException	&Bureaucrat::GradeTooHighException::operator=
( Bureaucrat::GradeTooHighException const &src )
{
	(void)src;
	return *this;
}

Bureaucrat::GradeTooLowException	&Bureaucrat::GradeTooLowException::operator=
( Bureaucrat::GradeTooLowException const &src )
{
	(void)src;
	return *this;
}

Bureaucrat::GradeTooLowException::GradeTooLowException
( GradeTooLowException const &src )
{
	*this = src;
}

Bureaucrat::GradeTooHighException::~GradeTooHighException( void ) throw()
{

}

Bureaucrat::GradeTooLowException::GradeTooLowException( void )
{

}

Bureaucrat::GradeTooLowException::~GradeTooLowException( void ) throw()
{

}

const char		*Bureaucrat::GradeTooHighException::what() const throw()
{
	return "Too high";
}

const char		*Bureaucrat::GradeTooLowException::what() const throw()
{
	return "Too low";
}

std::string	Bureaucrat::getName( void ) const
{
	return (this->_name);
}

int			Bureaucrat::getGrade( void ) const
{
	return (this->_grade);
}

void		Bureaucrat::IncreaseGrade( void )
{
	if ( this->getGrade() == 1 )
		throw Bureaucrat::GradeTooHighException();
	--this->_grade;
}

void		Bureaucrat::DecreaseGrade( void )
{
	if ( this->getGrade() == 150 )
		throw Bureaucrat::GradeTooLowException();
	++this->_grade;
}

void		Bureaucrat::signForm( Form &f ) const
{
	if ( this->getGrade() > f.getGradeToSign() )
		std::cout << this->getName() << " cannot sign "
				  << f.getName() << " because his grade is too low"
				  << std::endl;
	else if ( f.getSigned() == true )
		std::cout << this->getName() << " cannot sign "
				  << f.getName() << " because the form is already signed"
				  << std::endl;
	else
		std::cout << this->getName() << " signs " << f.getName() << std::endl;
}

int			Bureaucrat::executeForm(Form const & f ) const
{
	if ( this->getGrade() > f.getGradeToExec() )
		std::cout << this->getName() << " cannot execute "
				  << f.getName() << " because his grade is too low"
				  << std::endl;
	else if ( f.getSigned() == false )
		std::cout << this->getName() << " cannot execute "
				  << f.getName() << " because the form is not signed"
				  << std::endl;
	else
	{
		std::cout << this->getName() << " executes " << f.getName() << std::endl;
		return (1);
	}
	return (0);
}

std::ostream	&operator<<(std::ostream &o, Bureaucrat const &src)
{
	o << src.getName() << ", bureaucrat grade " << src.getGrade();
	return o;
}
