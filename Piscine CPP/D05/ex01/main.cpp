// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 12:59:23 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 21:32:05 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"
#include "Form.hpp"

int		main( void )
{
	Bureaucrat	b( "George", 60 );
	Form		f1( "DDHC", 60, 29 );
	Form		f2( "DDHC2", 20, 29 );

	std::cout << b << std::endl;
	std::cout << f1 << std::endl;
	f1.beSigned( b );
	std::cout << f1 << std::endl;
	f1.beSigned( b );
	
	std::cout << f2 << std::endl;
	try
	{
		f2.beSigned( b );
	}
//	catch (std::exception e)
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << f2 << std::endl;
	return 0;
}
