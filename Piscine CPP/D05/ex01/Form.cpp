// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Form.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 14:16:03 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 20:20:53 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Form.hpp"

Form::Form( void ) : _name( "defaultForm" ), _signed( false ),
					 _gradeToSign( 150 ), _gradeToExec( 150 )
{
	std::cout << "Form default constructor called" << std::endl;
}

Form::~Form( void )
{
	std::cout << "Form default destructor called" << std::endl;
}

Form::Form( std::string const name, int gradeToSign, int gradeToExec ) :
	_name( name ), _signed( false ),
	_gradeToSign( gradeToSign ), _gradeToExec( gradeToExec )
{
	std::cout << "Form params constructor called" << std::endl;
}

Form::Form( Form const &src ) :
	_name( src.getName() ), _signed( src.getSigned() ),
	_gradeToSign( src.getGradeToSign() ), _gradeToExec( src.getGradeToExec() )
{
	std::cout << "Bureaucrate copy constructor called" << std::endl;
}

Form		&Form::operator=( Form const &src )
{
	(void)src;
//	this->_grade = src.getGrade();
	return *this;
}

Form::GradeTooHighException::GradeTooHighException( void )
{
}

Form::GradeTooHighException::GradeTooHighException
( GradeTooHighException const &src )
{
    *this = src;
}

Form::GradeTooHighException		&Form::GradeTooHighException::operator=
( Form::GradeTooHighException const &src )
{
    (void)src;
    return *this;
}

Form::GradeTooLowException		&Form::GradeTooLowException::operator=
( Form::GradeTooLowException const &src )
{
    (void)src;
    return *this;
}

Form::GradeTooLowException::GradeTooLowException
( GradeTooLowException const &src )
{
    *this = src;
}


Form::GradeTooHighException::~GradeTooHighException( void ) throw()
{
}

Form::GradeTooLowException::GradeTooLowException( void )
{
}

Form::GradeTooLowException::~GradeTooLowException( void ) throw()
{
}

const char		*Form::GradeTooHighException::what() const throw()
{
	return "Too high";
}

const char		*Form::GradeTooLowException::what() const throw()
{
	return "Too low";
}

std::string	Form::getName( void ) const
{
	return (this->_name);
}

bool		Form::getSigned( void ) const
{
	return this->_signed;
}

int			Form::getGradeToSign( void ) const
{
	return this->_gradeToSign;
}

int			Form::getGradeToExec( void ) const
{
	return this->_gradeToExec;
}

void		Form::beSigned( Bureaucrat &b)
{
//	if (this->getSigned() == false)
//	{
		if ( b.getGrade() > this->getGradeToSign() )
			throw Form::GradeTooLowException();
		b.signForm( *this );
		this->_signed = true;
//	}
}

std::ostream	&operator<<(std::ostream &o, Form const &src)
{
	o << src.getName() << ", form ";
	if (src.getSigned() == false)
		o << " not signed,";
	else
		o << " signed,";
	o << " require a grade of " << src.getGradeToSign()
			  << " to be signed and " << src.getGradeToExec()
			  << " to be executed.";
	return o;
}
