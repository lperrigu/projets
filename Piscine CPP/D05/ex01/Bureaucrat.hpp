// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Bureaucrat.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 11:07:06 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/22 16:29:28 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BUREAUCRAT_HPP
#define BUREAUCRAT_HPP

#include <iostream>
#include "Form.hpp"

class Form;

class Bureaucrat
{
private:
	std::string const	_name;
	int					_grade;

	Bureaucrat( void );

public:
	Bureaucrat( std::string const name, int grade );
	virtual ~Bureaucrat( void );
	Bureaucrat( Bureaucrat const &src );

	std::string	getName( void ) const;
	int			getGrade( void ) const;
	void		IncreaseGrade( void );
	void		DecreaseGrade( void );
	void		signForm( Form &f ) const;

	Bureaucrat	&operator=( Bureaucrat const &src );

	class	GradeTooHighException : public std::exception
	{
	private:

		GradeTooHighException	&operator=( GradeTooHighException const &src );

	public:
		GradeTooHighException( GradeTooHighException const &src );
		virtual ~GradeTooHighException( void ) throw();
		GradeTooHighException( void );
		virtual const char		*what() const throw();
	};

	class	GradeTooLowException : public std::exception
	{
	private:

		GradeTooLowException	&operator=( GradeTooLowException const &src );
	public:
		GradeTooLowException( GradeTooLowException const &src );
		virtual ~GradeTooLowException( void ) throw();
		GradeTooLowException( void );
		virtual const char		*what() const throw();
	};
};

std::ostream		&operator<<(std::ostream &o, Bureaucrat const &src);

#endif
