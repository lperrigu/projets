// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   RobotomyRequestForm.hpp                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 17:09:27 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 20:13:50 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ROBOTOMYREQUESTFORM_HPP
#define ROBOTOMYREQUESTFORM_HPP

#include "Form.hpp"

class RobotomyRequestForm : public Form
{
private:
	std::string	_target;

	RobotomyRequestForm(RobotomyRequestForm const &);
	RobotomyRequestForm( void );
	RobotomyRequestForm& operator=(RobotomyRequestForm const &);

public:
	RobotomyRequestForm( std::string target );
	virtual ~RobotomyRequestForm( void );
	void	execute( Bureaucrat const &executor ) const;
};

#endif
