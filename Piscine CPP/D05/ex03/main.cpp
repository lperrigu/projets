// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 12:59:23 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 20:33:20 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Form.hpp"
#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"

class Form;
class RobotomyRequestForm;
class ShrubberyCreationForm;
class PresidentialPardonForm;

int		main( void )
{
	Bureaucrat	b( "George", 1 );
	Intern		i;
	Form		*f;
	f = i.MakeForm( "robotomy request", "Frog" );
	f->beSigned( b );
	std::cout << *f << std::endl;
	f->execute( b );
	f = i.MakeForm( "shrubbery creation", "Frog" );
	f = i.MakeForm( "presidential pardon", "Frog" );
	f = i.MakeForm( "Unknow form", "Frog" );
	return 0;
}
