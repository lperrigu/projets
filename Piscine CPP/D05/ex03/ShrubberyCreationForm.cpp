// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/22 17:06:52 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 19:37:38 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ShrubberyCreationForm.hpp"

ShrubberyCreationForm::ShrubberyCreationForm( std::string target ) :
	Form( "ShrubberyCreationForm", 145, 137 ), _target( target )
{

}

void	ShrubberyCreationForm::execute( Bureaucrat const &executor ) const
{
	if ( this->getSigned() == true )
	{
		if ( executor.getGrade() > this->getGradeToSign() )
			throw Form::GradeTooLowException();
		if ( executor.executeForm( *this ) )
		{
			std::string	name_file = this->_target + "_shrubbery";
			std::ofstream ofs( name_file );
			ofs << "ASCII trees" << std::endl;
			ofs.close();
		}
	}
}

ShrubberyCreationForm::~ShrubberyCreationForm()
{
	
}
