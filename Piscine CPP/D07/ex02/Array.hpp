// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Array.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/24 17:48:25 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/25 17:22:37 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ARRAY_H
#define ARRAY_H

#include <cstring>

template<typename T>
class Array
{
private:
	T				*_array;
	unsigned int	_size;

public:
	Array( void );
	Array( unsigned int N );
	Array( Array const &src );
	virtual	~Array();

	size_t	size( void ) const;
	Array	&operator=( Array const &src );
	T		&operator[]( unsigned int i ) const;

};

#endif
