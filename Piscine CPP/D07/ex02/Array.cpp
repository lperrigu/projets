// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Array.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/24 17:49:51 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/25 17:56:25 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Array.hpp"
#include <iostream>

template<typename T>
Array<T>::Array( void )
{
	this->_array = new T[0];
	this->_size = 0;
}

template<typename T>
Array<T>::Array( unsigned int N )
{
	T				*tmp = new T();
	unsigned int	i;

	this->_array = new T[N];
	this->_size = N;
	for ( i = 0; i < N; ++i )
		this->_array[i] = *tmp;
	delete tmp;
}

template<typename T>
Array<T>::Array( Array const &src )
{
	this->_array = NULL;
	*this = src;
}

template<typename T>
size_t	Array<T>::size( void ) const
{
	return (this->_size);
}

template<typename T>
Array<T>	&Array<T>::operator=( Array const &rhs )
{
	unsigned int		i;

	delete this->_array;
	this->_array = new T[ rhs.size() ];
	for( i = 0 ; i < rhs.size(); ++i )
		this->_array[i] = rhs[i];
	this->_size = rhs.size();
	return (*this);
}

template<typename T>
T		&Array<T>::operator[]( unsigned int i ) const
{
	if ( i >= this->_size )
		throw std::exception();
	return ( this->_array[i] );
}

template<typename T>
Array<T>::~Array()
{
	delete [] this->_array;
}

int main( void )
{
	Array<int>  A1( 10 );
	Array<int>  A2( 15 );
	size_t i;

	for ( i = 0 ; i < 10; ++i )
		A1[i] = i + 10;
	std::cout << "On affiche A1" << std::endl;
	for ( i = 0 ; i < 10; ++i )
		std::cout << A1[i] << std::endl;
	Array<int>  A3( A1 );
	std::cout << std::endl << std::endl;;
	std::cout << "A3( A1 ), on affiche A3" << std::endl;
	for ( i = 0 ; i < 10; ++i )
		std::cout << A3[i] << std::endl;
	try
	{
		std::cout << "On essaye d'acceder a un element hors limites" << std::endl;
		A1[10];
	}
	catch (std::exception e)
	{
		std::cout << "ERROR" << std::endl;
	}
	std::cout << std::endl << std::endl;;

	for ( i = 0 ; i < 15; ++i )
		A2[i] = -i;
	std::cout << "On affiche A2" << std::endl;
	for ( i = 0; i < A2.size(); ++i )
		std::cout << A2[i] << std::endl;

	std::cout << std::endl << std::endl;;
	std::cout << "A2 = A1" << std::endl;
	A2 = A1;
	std::cout << "On affiche A2" << std::endl;
	for ( i = 0; i < A2.size(); ++i )
		std::cout << A2[i] << std::endl;

	std::cout << std::endl << std::endl;;
	Array<float>  A4( 10 );

	for ( i = 0; i < A4.size(); ++i )
		A4[i] = i * 2 + 0.42;
	for ( i = 0; i < A4.size(); ++i )
		std::cout << A4[i] << std::endl;
	return 0;
}
