// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   compare.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/24 11:31:20 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/24 11:40:37 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

namespace	compare
{
	template< typename T >
	void	swap( T &a, T &b )
	{
		T	buf;

		buf = b;
		b = a;
		a = buf;
	}

	template< typename T >
	T		&min( T &a, T &b )
	{
		if ( a > b )
			return ( b );
		else
			return ( a );
	}

	template< typename T >
	T		&max( T &a, T &b )
	{
		if ( a > b )
			return ( a );
		else
			return ( b );
	}
}
