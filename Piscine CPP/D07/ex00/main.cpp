// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/24 11:40:54 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/24 11:46:55 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "compare.cpp"
#include <iostream>

int main( void )
{
	int a = 2;
	int b = 3;
	compare::swap( a, b );
	std::cout << "a = " << a << ", b = " << b << std::endl;
	std::cout << "min( a, b ) = " << compare::min( a, b ) << std::endl;
	std::cout << "max( a, b ) = " << compare::max( a, b ) << std::endl << std::endl;

	std::string c = "chaine1";
	std::string d = "chaine2";
	compare::swap(c, d);
	std::cout << "c = " << c << ", d = " << d << std::endl;
	std::cout << "min( c, d ) = " << compare::min( c, d ) << std::endl;
	std::cout << "max( c, d ) = " << compare::max( c, d ) << std::endl << std::endl;

	float e = 1.7;
	float f = 8.2;
	compare::swap(e, f);
	std::cout << "e = " << e << ", f = " << f << std::endl;
	std::cout << "min( e, f ) = " << compare::min( e, f ) << std::endl;
	std::cout << "max( e, f ) = " << compare::max( e, f ) << std::endl;
	return 0;
}
