// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   iter.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/24 12:44:08 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/24 12:44:10 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>

template< typename T >
void	neg( T v )
{
	if ( v <= 0 )
		std::cout << v << std::endl;
}

template< typename T >
void	iter( T *tab, int size, void (*func)(T))
{
	int	i;

	for( i = 0; i < size; ++i )
		func( tab[i] );
}

int		main( void )
{
	int tab_int[] = { 1, -1, 14, 54, 0 };
	void (*func_int)(int);
	float tab_float[] = { 2.5, 8.042, -14, -5.444, -0.43 };
	void (*func_float)(float);
	double tab_double[] = { -131, -1, 14, 54, -120.3 };
	void (*func_double)(double);

	func_int = &neg;
	iter( tab_int, 5, func_int );
	std::cout << std::endl;

	func_float = &neg;
	iter( tab_float, 5, func_float );
	std::cout << std::endl;

	func_double = &neg;
	iter( tab_double, 5, func_double );
	return 0;
}
