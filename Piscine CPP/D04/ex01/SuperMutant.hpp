// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   SuperMutant.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 17:14:18 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 17:24:25 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SUPERMUTANT_HPP
#define SUPERMUTANT_HPP

#include <iostream>
#include "Enemy.hpp"

class SuperMutant : public Enemy
{
private:

public:
	SuperMutant( void );
	SuperMutant( SuperMutant const &src );
	~SuperMutant( void );

	SuperMutant	&operator=( SuperMutant const &src );

	void	takeDamage( int );
};

#endif
