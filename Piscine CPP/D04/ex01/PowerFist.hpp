// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   PowerFist.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 16:28:32 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 18:46:53 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef POWERFIST_HPP
#define POWERFIST_HPP

#include <iostream>
#include "AWeapon.hpp"

class PowerFist : public AWeapon
{
private:

public:
	PowerFist( void );
	virtual ~PowerFist( void );
	PowerFist( PowerFist const &src );

	virtual void	attack( void ) const;

	PowerFist		&operator=( PowerFist const &src );

};

#endif
