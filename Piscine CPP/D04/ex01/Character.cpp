// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Character.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 18:03:36 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 18:57:56 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Character.hpp"

Character::Character( void )
{

}

Character::~Character( void )
{

}

Character::Character( std::string const &name ) :
	_name( name ), _AP ( 40 ), _weapon ( NULL )
{

}

Character::Character( Character const &src ) :
	_name( src.getName() ), _AP ( src.getAP() ), _weapon ( src.getWeapon() )
{

}

Character		&Character::operator=( Character const &src )
{
	this->_name = src.getName();
	this->_AP = src.getAP();
	this->_weapon = src.getWeapon();
	return *this;
}

std::string	Character::getName( void ) const
{
	return ( this->_name );
}

int			Character::getAP( void ) const
{
	return ( this->_AP );
}

AWeapon		*Character::getWeapon( void ) const
{
	return ( this->_weapon );
}

void	Character::recoverAP( void )
{
	this->_AP += 10;
	if ( this->_AP > 40)
		this->_AP = 40;
}

void	Character::equip( AWeapon * weapon )
{
	this->_weapon = weapon;
}

void	Character::attack( Enemy * enemy )
{
	if ( this->_weapon != NULL)
	{
		if (this->_AP > this->_weapon->getAPCost())
		{
			this->_AP -= this->_weapon->getAPCost();
			std::cout << this->getName() << " attacks "
					  << enemy->getType() << " with a "
					  << this->_weapon->getName() << std::endl;
			this->_weapon->attack();
			enemy->takeDamage( this->_weapon->getDamage() );
			if ( enemy->getHP() <= 0 )
				delete enemy;
		}
	}
}

std::ostream	&operator<<( std::ostream &o, Character const &src )
{
	if (src.getWeapon() != NULL)
	o << src.getName() << " has " << src.getAP()
	  << " AP and wields a " << src.getWeapon()->getName() << std::endl;
	else
	o << src.getName() << " has " << src.getAP()
	  << " AP and is unarmed" << std::endl;
	return o;
}
