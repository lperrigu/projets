// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   AWeapon.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 12:08:55 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 13:58:48 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Aweapon.hpp"

AWeapon::AWeapon( void )
{
}


AWeapon::~AWeapon( void )
{
}

AWeapon::AWeapon( std::string const &name, int apcost, int damage) : 
	_name ( name ),
	_apcost( apcost ),
	_damage( damage )
{
}

AWeapon::AWeapon( AWeapon const &src ) : _name( src.getName() )
{
	*this = src;
}

AWeapon				&AWeapon::operator=( AWeapon const &src )
{
//	this->_name = src.getName();
	this->_apcost = src.getAPCost();
	this->_damage = src.getDamage();
	return ( *this );
}

std::string const	AWeapon::getName( void ) const
{
	return (this->_name);
}

int					AWeapon::getAPCost( void ) const
{
	return (this->_apcost);
}

int					AWeapon::getDamage( void ) const
{
	return (this->_damage);
}
