// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   PlasmaRifle.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 15:11:45 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 16:35:54 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "PlasmaRifle.hpp"

PlasmaRifle::PlasmaRifle( void ) : AWeapon( "Plasma Rifle", 5, 21 )
{
}

PlasmaRifle::~PlasmaRifle( void )
{
}

PlasmaRifle::PlasmaRifle( PlasmaRifle const &src ) : AWeapon( "Plasma Rifle", 5, 21 )
{
	*this = src;
}

void		PlasmaRifle::attack( void ) const
{
	std::cout << "* piouuu piouuu piouuu *" << std::endl;
}

PlasmaRifle     &PlasmaRifle::operator=( PlasmaRifle const &src )
{
	this->_apcost = src.getAPCost();
	this->_damage = src.getDamage();
	return ( *this );
}
