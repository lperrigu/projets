// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   SuperMutant.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 17:24:53 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 17:45:01 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "SuperMutant.hpp"

SuperMutant::SuperMutant( void ) : Enemy( 130, "Super Mutant" )
{
	std::cout << "Gaaah. Me want smash heads !" << std::endl;
}


SuperMutant::~SuperMutant( void )
{
	std::cout << "Aaargh ..." << std::endl;
}

SuperMutant::SuperMutant( SuperMutant const &src ) :
	Enemy( src.getHP(), src.getType() )
{
//	*this = src;
}

SuperMutant				&SuperMutant::operator=( SuperMutant const &src )
{
	this->_hp = src.getHP();
	return ( *this );
}

void	SuperMutant::takeDamage( int damage )
{
	damage -= 3;
	if ( damage >= 0)
		this->_hp -= damage;
}
