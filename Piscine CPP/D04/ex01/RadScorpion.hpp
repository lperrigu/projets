// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   RadSocrpion.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 17:46:45 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 17:51:25 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef RADSCORPION_HPP
#define RADSCORPION_HPP

#include <iostream>
#include "Enemy.hpp"

class RadScorpion : public Enemy
{
private:

public:
	RadScorpion( void );
	RadScorpion( RadScorpion const &src );
	~RadScorpion( void );

	RadScorpion	&operator=( RadScorpion const &src );
};

#endif
