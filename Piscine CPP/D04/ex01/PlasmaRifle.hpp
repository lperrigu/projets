// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   PlasmaRifle.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 14:00:14 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 18:46:36 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PLASMARIFLE_HPP
#define PLASMARIFLE_HPP

#include <iostream>
#include "AWeapon.hpp"

class PlasmaRifle : public AWeapon
{
private:

public:
	PlasmaRifle( void );
	virtual ~PlasmaRifle( void );
	PlasmaRifle( PlasmaRifle const &src );

	virtual void	attack( void ) const;

	PlasmaRifle		&operator=( PlasmaRifle const &src );

};

#endif
