// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 17:02:07 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 17:13:47 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <iostream>

class Enemy
{
private:

protected:
	int					_hp;
	std::string const	_type;
	Enemy( void );

public:
	Enemy( int hp, std::string const &type );
	virtual ~Enemy( void );
	Enemy( Enemy const &src );

	std::string const	getType( void ) const;
	int					getHP( void ) const;

	virtual void	takeDamage( int );

	Enemy		&operator=( Enemy const &src );

};

#endif
