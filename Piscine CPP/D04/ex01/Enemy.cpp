// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 17:06:41 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 17:27:47 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Enemy.hpp"

Enemy::Enemy( void )
{
}


Enemy::~Enemy( void )
{
}

Enemy::Enemy( int hp, std::string const &type ) : 
	_hp ( hp ),
	_type( type )
{
}

Enemy::Enemy( Enemy const &src ) : _type( src.getType() )
{
	*this = src;
}

Enemy				&Enemy::operator=( Enemy const &src )
{
	this->_hp = src.getHP();
	return ( *this );
}

std::string const	Enemy::getType( void ) const
{
	return (this->_type);
}

int					Enemy::getHP( void ) const
{
	return (this->_hp);
}

void	Enemy::takeDamage( int damage )
{
	if ( damage >= 0)
		this->_hp -= damage;
}
