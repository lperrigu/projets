// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   PowerFist.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 16:30:06 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 16:36:38 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "PowerFist.hpp"

PowerFist::PowerFist( void ) : AWeapon( "Power Fist", 8, 50 )
{
}

PowerFist::~PowerFist( void )
{
}

PowerFist::PowerFist( PowerFist const &src )  : AWeapon( "Power Fist", 8, 50 )
{
	*this = src;
}

void		PowerFist::attack( void ) const
{
	std::cout << "* pschhh... SBAM! *" << std::endl;
}

PowerFist     &PowerFist::operator=( PowerFist const &src )
{
	this->_apcost = src.getAPCost();
	this->_damage = src.getDamage();
	return ( *this );
}
