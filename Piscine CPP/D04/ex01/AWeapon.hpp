// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   AWeapon.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 12:00:45 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 16:21:56 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef AWEAPON_HPP
#define AWEAPON_HPP

#include <iostream>

class AWeapon
{
private:
	std::string const	_name;

protected:
	int					_apcost;
	int					_damage;

protected:
	AWeapon( void );

public:
	AWeapon( std::string const &name, int apcost, int damage);
	virtual ~AWeapon( void );
	AWeapon( AWeapon const &src );

	std::string const	getName( void ) const;
	int					getAPCost( void ) const;
	int					getDamage( void ) const;

	virtual void	attack( void ) const = 0;

	AWeapon		&operator=( AWeapon const &src );

};

#endif
