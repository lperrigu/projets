// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   RadSocrpion.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 17:49:39 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 18:57:13 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "RadScorpion.hpp"

RadScorpion::RadScorpion( void ) : Enemy( 80, "RadScorpion" )
{
	std::cout << "* click click click *" << std::endl;
}

RadScorpion::~RadScorpion( void )
{
	std::cout << "* SPROTCH *" << std::endl;
}

RadScorpion::RadScorpion( RadScorpion const &src ) :
	Enemy( src.getHP(), src.getType() )
{
//	*this = src;
}

RadScorpion				&RadScorpion::operator=( RadScorpion const &src )
{
	this->_hp = src.getHP();
	return ( *this );
}
