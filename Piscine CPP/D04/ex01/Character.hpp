// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Character.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 17:54:48 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 18:44:43 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include <iostream>
#include "AWeapon.hpp"
#include "Enemy.hpp"

class Character
{
private:
	std::string	_name;
	int			_AP;
	AWeapon		*_weapon;

	Character( void );

public:
	Character( std::string const &name );
	virtual ~Character( void );
	Character( Character const &src );

	Character	&operator=( Character const &src );

	std::string	getName( void ) const;
	int			getAP( void ) const;
	AWeapon		*getWeapon( void ) const;

	void	recoverAP( void );
	void	equip( AWeapon * );
	void	attack( Enemy *);

};

std::ostream		&operator<<( std::ostream &o, Character const &src );

#endif
