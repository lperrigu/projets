// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Sorcerer.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/19 17:01:12 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 13:41:25 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SORCERER_HPP
#define SORCERER_HPP

#include <iostream>
#include "Victim.hpp"

class Sorcerer
{
private:
	std::string _name;
	std::string _title;

	Sorcerer( void );

public:
	Sorcerer( std::string name, std::string title );
	virtual ~Sorcerer( void );
	Sorcerer( Sorcerer const &src );

	std::string	getName( void ) const;
	std::string	getTitle( void ) const;

	Sorcerer	&operator=( Sorcerer const &src );

	void		polymorph( Victim const &victim ) const;

};

std::ostream &operator<<( std::ostream &o, Sorcerer const &src );

#endif
