// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Peon.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 11:12:55 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 11:30:45 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PEON_HPP
#define PEON_HPP

#include <iostream>
#include "Victim.hpp"

class Peon : public Victim
{
private:
	Peon( void );

public:
	Peon( std::string name);
	virtual ~Peon( void );
	Peon( Peon const &src);

	Peon	&operator=( Peon const &src);

	void	getPolymorphed( void ) const;

};

#endif
