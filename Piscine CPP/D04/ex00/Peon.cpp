// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Peon.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 11:19:15 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 11:39:32 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Peon.hpp"

Peon::Peon( void )
{
	std::cout << "Peon default called" << std::endl;
}

Peon::Peon( std::string name ) : Victim( name )
{
	std::cout << "Zog zog." << std::endl;
}

Peon::~Peon( void )
{
	std::cout << "Bleuark..." << std::endl;
}

void		Peon::getPolymorphed( void ) const
{
	std::cout << this->getName()
			  << " has been turned into a pink pony !"
			  << std::endl;
}
