// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Sorcerer.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/19 17:19:44 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 13:51:56 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Sorcerer.hpp"

Sorcerer::Sorcerer( void )
{
	std::cout << "Sorcerer default called" << std::endl;
}

Sorcerer::Sorcerer( std::string name, std::string title ) : _name( name ),
															_title( title )
{
	std::cout << name << ", " << title
			  << ", is born !" << std::endl;
}

Sorcerer::Sorcerer( Sorcerer const &src )
{
	*this = src;
}

Sorcerer::~Sorcerer( void )
{
	std::cout << this->getName() << ", " << this->getTitle()
              << ", is dead. "
              << "Consequences will never be the same !" << std::endl;
}


std::string	Sorcerer::getName( void ) const
{
	return (this->_name);
}

std::string	Sorcerer::getTitle( void ) const
{
	return (this->_title);
}

void			Sorcerer::polymorph( Victim const &victim) const
{
	victim.getPolymorphed();
}

Sorcerer		&Sorcerer::operator=( Sorcerer const &src)
{
	this->_name = src.getName();
	this->_title = src.getTitle();

	return ( *this );
}

std::ostream	&operator<<( std::ostream &o, Sorcerer const &src )
{
	o << "I am " << src.getName() << ", " << src.getTitle()
	  << ", and I like ponies !" << std::endl;
	return o;
}
