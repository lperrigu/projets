// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Victim.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 10:40:39 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 11:41:40 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef VICTIM_HPP
#define VICTIM_HPP

#include <iostream>

class Victim
{
private:
	std::string _name;

protected:
	Victim( void );

public:
	Victim( std::string name );
	virtual ~Victim( void );
	Victim( Victim const &src );

	std::string	getName( void ) const;

	Victim	&operator=( Victim const &src );

	virtual void	getPolymorphed( void ) const;

};

std::ostream &operator<<( std::ostream &o, Victim const &src );

#endif
