// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Victim.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 10:42:28 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/20 11:45:09 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Victim.hpp"

Victim::Victim( void )
{
	std::cout << "Victim default called" << std::endl;
}

Victim::Victim( std::string name ) : _name( name )
{
	std::cout << "Some random victim called " << name
			  << " just popped !" << std::endl;
}

Victim::~Victim( void )
{
	std::cout << "Victim " << this->getName()
			  << " just died for no apparent reason !" << std::endl;
}

std::string	Victim::getName( void ) const
{
	return (this->_name);
}

void		Victim::getPolymorphed( void ) const
{
	std::cout << this->getName()
			  << " has been turned into a cute little sheep !"
			  << std::endl;
}

std::ostream	&operator<<( std::ostream &o, Victim const &src )
{
	o << "I'm " << src.getName()
	  << " and i like otters !" << std::endl;
	return o;
}
