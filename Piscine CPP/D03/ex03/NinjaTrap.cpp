// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   NinjaTrap.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/19 14:51:17 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 16:24:27 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap( void )
{
	std::cout << "NinjaTrap default constructor called" << std::endl;
}

NinjaTrap::~NinjaTrap( void )
{
	std::cout << "NinjaTrap default destructor called on "
			  << this->getName() << std::endl;
}

NinjaTrap::NinjaTrap( std::string name )
{
	this->setMaxHitPoint( 60 );
	this->setHitPoint( 60 );
	this->setMaxEnergyPoint( 120 );
	this->setEnergyPoint( 120 );
	this->setLevel( 1 );
	this->setName( name );
	this->setMeleeAttackDamage( 60 );
	this->setRangedAttackDamage( 5 );
	this->setArmorDamageReduction( 0 );
	srand( time( NULL ) );
	std::cout << "String constructor called on "
			  << name << std::endl;
}

NinjaTrap::NinjaTrap( NinjaTrap const &src )
{
	std::cout << "NinjaTrap copy constructor called" << std::endl;
	*this = src;
}

NinjaTrap	&NinjaTrap::operator=( NinjaTrap const &src)
{
	std::cout << "NinjaTrap assignation operator called" << std::endl;
	this->setMaxHitPoint( src.getMaxHitPoint() );
	this->setHitPoint( src.getHitPoint() );
	this->setMaxEnergyPoint( src.getMaxEnergyPoint() );
	this->setEnergyPoint( src.getEnergyPoint() );
	this->setLevel( src.getLevel() );
	this->setName( src.getName() );
	this->setMeleeAttackDamage( src.getMeleeAttackDamage() );
	this->setRangedAttackDamage( src.getRangedAttackDamage() );
	this->setArmorDamageReduction( src.getArmorDamageReduction() );
	return *this;
}

void		NinjaTrap::rangedAttack( std::string const &target ) const
{
	std::cout << "N1NJ4-TR4P " << this->getName()
			  << " attacks " << target
			  << " at range, causing " << this->getRangedAttackDamage()
			  << " points of damage !" << std::endl;
}

void		NinjaTrap::MeleeAttack( std::string const &target ) const
{
	std::cout << "N1NJ4-TR4P " << this->getName()
			  << " attacks " << target
			  << " at melee, causing " << this->getMeleeAttackDamage()
			  << " points of damage !" << std::endl;
}

void		NinjaTrap::takedDamage( unsigned int amount )
{
	if ( (int)amount <= this->getArmorDamageReduction() )
		std::cout << " YOU CAN'T EVEN PIERCE MY NINJA ARMOR !" << std::endl;
	else
	{
		amount -= this->getArmorDamageReduction();
		std::cout << "N1NJ4-TR4P " << this->getName()
				  << " takes " << amount
				  << " points of damage " << std::endl;
		this->setHitPoint( this->getHitPoint() - amount );
	std::cout << "N1NJ4-TR4P " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint" << std::endl;
	}
}

void		NinjaTrap::beRepaired( unsigned int amount )
{
	std::cout << "N1NJ4-TR4P " << this->getName()
			  << " is repair of " << amount
			  << " points" << std::endl;
	this->setHitPoint( this->getHitPoint() + amount );
	std::cout << "N1NJ4-TR4P " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint" << std::endl;
}

void		NinjaTrap::ninjaShoebox( ClapTrap const &clap ) const
{
	std::cout << "Insert something funny 1 "
			  << clap.getName() << std::endl;
}

void		NinjaTrap::ninjaShoebox( FragTrap const &frag ) const
{
	std::cout << "Insert something funny 2 "
			  << frag.getName() << std::endl;
}

void		NinjaTrap::ninjaShoebox( ScavTrap const &scav ) const
{
	std::cout << "Insert something funny 3 "
			  << scav.getName() << std::endl;
}

void		NinjaTrap::ninjaShoebox( NinjaTrap const &ninja ) const
{
	std::cout << "Insert something funny 4 "
			  << ninja.getName() << std::endl;
}
