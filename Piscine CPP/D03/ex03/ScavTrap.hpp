// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ScavTrap.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 21:15:28 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 13:25:58 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SCAVTRAP_HPP
#define SCAVTRAP_HPP

#include <iostream>
#include "ClapTrap.hpp"

class ScavTrap : public ClapTrap
{
private:
	ScavTrap( void );

public:
	ScavTrap( std::string name );
	~ScavTrap( void );
	ScavTrap( ScavTrap const &src );

	ScavTrap	&operator=( ScavTrap const &src );

	void		rangedAttack( std::string const &target ) const;
	void		MeleeAttack( std::string const &target ) const;
	void		takedDamage( unsigned int amount );
	void		beRepaired( unsigned int amount );
	void		challengeNewcomer( void ) const;

};

#endif
