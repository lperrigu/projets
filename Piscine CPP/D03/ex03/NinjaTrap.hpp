// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   NinjaTrap.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/19 14:37:27 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 15:03:43 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef NINJATRAP_HPP
#define NINJATRAP_HPP

#include <iostream>
#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap : ClapTrap
{
private:
	NinjaTrap( void );

public:
	~NinjaTrap( void );
	NinjaTrap( std::string name);
	NinjaTrap( NinjaTrap const &src );

	NinjaTrap	&operator=( NinjaTrap const &src );

    void		rangedAttack( std::string const &target ) const;
    void		MeleeAttack( std::string const &target ) const;
    void		takedDamage( unsigned int amount );
    void		beRepaired( unsigned int amount );
	void		ninjaShoebox( ClapTrap const &clap ) const;
	void		ninjaShoebox( FragTrap const &frag ) const;
	void		ninjaShoebox( ScavTrap const &scav ) const;
	void		ninjaShoebox( NinjaTrap const &ninja ) const;
};

#endif
