// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   FragTrap.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 18:07:51 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 13:25:04 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include <iostream>
#include "ClapTrap.hpp"

class FragTrap : public ClapTrap
{
private:
	FragTrap( void );

public:
	FragTrap( std::string name );
	~FragTrap( void );
	FragTrap( FragTrap const &src );

	FragTrap	&operator=( FragTrap const &src );

	void		rangedAttack( std::string const &target ) const;
	void		MeleeAttack( std::string const &target ) const;
	void		takedDamage( unsigned int amount );
	void		beRepaired( unsigned int amount );
	void		vaulthunter_dot_exe( std::string const & target);

};

#endif
