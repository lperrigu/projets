// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   FragTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 18:26:18 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 14:24:28 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "FragTrap.hpp"

FragTrap::FragTrap( void )
{
	std::cout << "FragTrap default constructor called" << std::endl;
}

FragTrap::~FragTrap( void )
{
	std::cout << "FragTrap default destructor called" << std::endl;
}

FragTrap::FragTrap( std::string name ) :
_HitPoint( 100 ),
_MaxHitPoint( 100 ),
_EnergyPoint( 100 ),
_MaxEnergyPoint( 100 ),
_Level( 1 ),
_Name( name ),
_MeleeAttackDamage( 30 ),
_RangedAttackDamage( 20 ),
_ArmorDamageReduction( 5 )
{
	srand( time( NULL ) );
	std::cout << "FragTrap string constructor called" << std::endl;
}

FragTrap::FragTrap( FragTrap const &src )
{
	std::cout << "FragTrap copy constructor called" << std::endl;
	*this = src;
}

FragTrap	&FragTrap::operator=( FragTrap const &src)
{
	std::cout << "FragTrap assignation operator called" << std::endl;
	this->_Name = src.getName();
	this->_HitPoint = src.getHitPoint();
	this->_MaxHitPoint = src.getMaxHitPoint();
	this->_EnergyPoint = src.getEnergyPoint();
	this->_MaxEnergyPoint = src.getMaxEnergyPoint();
	this->_Level = src.getLevel();
	this->_MeleeAttackDamage = src.getMeleeAttackDamage();
	this->_RangedAttackDamage = src.getRangedAttackDamage();
	this->_ArmorDamageReduction = src.getArmorDamageReduction();
	return *this;
}

void		FragTrap::rangedAttack( std::string const &target ) const
{
	std::cout << "FR4G-TP " << this->getName()
			  << " attacks " << target
			  << " at range, causing " << this->getRangedAttackDamage()
			  << " points of damage !" << std::endl;
}

void		FragTrap::MeleeAttack( std::string const &target ) const
{
	std::cout << "FR4G-TP " << this->getName()
			  << " attacks " << target
			  << " at melee, causing " << this->getMeleeAttackDamage()
			  << " points of damage !" << std::endl;
}

void		FragTrap::takedDamage( unsigned int amount )
{
	if ( (int)amount <= this->getArmorDamageReduction() )
		std::cout << "FOOL ! YOU CANNOT EVEN SCRATCH ME !" << std::endl;
	else
	{
		amount -= this->getArmorDamageReduction();
		std::cout << "FR4G-TP " << this->getName()
				  << " takes " << amount
				  << " points of damage" << std::endl;
		this->setHitPoint( this->getHitPoint() - amount );
	std::cout << "FR4G-TP " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint"<< std::endl;
	}
}

void		FragTrap::beRepaired( unsigned int amount )
{
	std::cout << "FR4G-TP " << this->getName()
			  << " is repair of " << amount
			  << " points" << std::endl;
	this->setHitPoint( this->getHitPoint() + amount );
	std::cout << "FR4G-TP " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint"<< std::endl;
}

void		FragTrap::vaulthunter_dot_exe( std::string const & target)
{
	if ( this->getEnergyPoint() >= 25 )
	{
		this->setEnergyPoint( this->getEnergyPoint() - 25);
		std::string	actions[5];
		int			i;

		actions[0] = "A disco ball floats shooting shock lasers";
		actions[1] = "A disco ball floats shooting fire lasers";
		actions[2] = "A disco ball floats shooting corrosion lasers";
		actions[3] = "A disco ball floats shooting BFG lasers";
		actions[4] = "A disco ball floats shooting ice lasers";
		i = rand() % 5;
		std::cout << actions[i]
				  << " dealing tons of damage to " << target << std::endl;
	}
	else
		std::cout << "Not enough energy point, minion" << std::endl;
}

int			FragTrap::getHitPoint( void ) const
{
	return (this->_HitPoint);
}
int			FragTrap::getMaxHitPoint( void ) const
{
	return (this->_MaxHitPoint);
}

int			FragTrap::getEnergyPoint( void ) const
{
	return (this->_EnergyPoint);
}

int			FragTrap::getMaxEnergyPoint( void ) const
{
	return (this->_MaxEnergyPoint);
}

int			FragTrap::getLevel( void ) const
{
	return (this->_Level);
}

std::string	FragTrap::getName( void ) const
{
	return (this->_Name);
}

int			FragTrap::getMeleeAttackDamage( void ) const
{
	return (this->_MeleeAttackDamage);
}

int			FragTrap::getRangedAttackDamage( void ) const
{
	return (this->_RangedAttackDamage);
}

int			FragTrap::getArmorDamageReduction( void ) const
{
	return (this->_ArmorDamageReduction);
}

void		FragTrap::setHitPoint( int hitpoint )
{
	if ( hitpoint >= this->getMaxHitPoint() )
		this->_HitPoint = this->getMaxHitPoint();
	else if ( hitpoint < 0)
		this->_HitPoint = 0;
	else
		this->_HitPoint = hitpoint;
}
void		FragTrap::setMaxHitPoint( int maxhitpoint )
{
	this->_MaxHitPoint = maxhitpoint;
}

void		FragTrap::setEnergyPoint( int energypoint )
{
	this->_EnergyPoint = energypoint;
}

void		FragTrap::setMaxEnergyPoint( int maxenergypoint )
{
	this->_MaxEnergyPoint = maxenergypoint;
}

void		FragTrap::setLevel( int level )
{
	this->_Level = level;
}

void		FragTrap::setName( std::string name )
{
	this->_Name = name;
}

void		FragTrap::setMeleeAttackDamage( int meleeattackdamage )
{
	this->_MeleeAttackDamage = meleeattackdamage;
}

void		FragTrap::setRangedAttackDamage( int attackdamage )
{
	this->_RangedAttackDamage = attackdamage;
}

void		FragTrap::setArmorDamageReduction( int armordamagereduction )
{
	this->_ArmorDamageReduction = armordamagereduction;
}
