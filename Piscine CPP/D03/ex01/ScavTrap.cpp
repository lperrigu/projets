// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ScavTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 21:14:29 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 14:26:09 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ScavTrap.hpp"

ScavTrap::ScavTrap( void )
{
	std::cout << "ScavTrap default constructor called" << std::endl;
}

ScavTrap::~ScavTrap( void )
{
	std::cout << "ScavTrap default destructor called" << std::endl;
}

ScavTrap::ScavTrap( std::string name ) :
_HitPoint( 100 ),
_MaxHitPoint( 100 ),
_EnergyPoint( 50 ),
_MaxEnergyPoint( 50 ),
_Level( 1 ),
_Name( name ),
_MeleeAttackDamage( 20 ),
_RangedAttackDamage( 15 ),
_ArmorDamageReduction( 3 )
{
	srand( time( NULL ) );
	std::cout << "ScavTrap string constructor called" << std::endl;
}

ScavTrap::ScavTrap( ScavTrap const &src )
{
	std::cout << "ScavTrap copy constructor called" << std::endl;
	*this = src;
}

ScavTrap	&ScavTrap::operator=( ScavTrap const &src)
{
	std::cout << "ScavTrap assignation operator called" << std::endl;
	this->_Name = src.getName();
	this->_HitPoint = src.getHitPoint();
	this->_MaxHitPoint = src.getMaxHitPoint();
	this->_EnergyPoint = src.getEnergyPoint();
	this->_MaxEnergyPoint = src.getMaxEnergyPoint();
	this->_Level = src.getLevel();
	this->_MeleeAttackDamage = src.getMeleeAttackDamage();
	this->_RangedAttackDamage = src.getRangedAttackDamage();
	this->_ArmorDamageReduction = src.getArmorDamageReduction();
	return *this;
}

void		ScavTrap::rangedAttack( std::string const &target ) const
{
	std::cout << "SC4V-TR4P " << this->getName()
			  << " attacks " << target
			  << " at range, causing " << this->getRangedAttackDamage()
			  << " points of damage !" << std::endl;
}

void		ScavTrap::MeleeAttack( std::string const &target ) const
{
	std::cout << "SC4V-TR4P " << this->getName()
			  << " attacks " << target
			  << " at melee, causing " << this->getMeleeAttackDamage()
			  << " points of damage !" << std::endl;
}

void		ScavTrap::takedDamage( unsigned int amount )
{
	if ( (int)amount <= this->getArmorDamageReduction() )
		std::cout << "DID NOT EVEN HURT ME !" << std::endl;
	else
	{
		amount -= this->getArmorDamageReduction();
		std::cout << "SC4V-TR4P " << this->getName()
				  << " takes " << amount
				  << " points of damage " << std::endl;
		this->setHitPoint( this->getHitPoint() - amount );
	std::cout << "SC4V-TR4P " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint" << std::endl;
	}
}

void		ScavTrap::beRepaired( unsigned int amount )
{
	std::cout << "SC4V-TR4P " << this->getName()
			  << " is repair of " << amount
			  << " points" << std::endl;
	this->setHitPoint( this->getHitPoint() + amount );
	std::cout << "SC4V-TR4P " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint" << std::endl;
}

void		ScavTrap::challengeNewcomer( void ) const
{
	std::string	challenges[5];
	int			i;

	challenges[0] = "do 200 pushups in 200 seconds";
	challenges[1] = "recognize 5 different deodorants only by smell";
	challenges[2] = "blow out 10 candles lined up in a row with one breath";
	challenges[3] = "drink 2 liters of mineral water within 1 minute";
	challenges[4] = "eat 5 bananas in 2 minutes";
	i = rand() % 5;
	std::cout << "I bet you can't " << challenges[i] << std::endl;
}

int			ScavTrap::getHitPoint( void ) const
{
	return (this->_HitPoint);
}
int			ScavTrap::getMaxHitPoint( void ) const
{
	return (this->_MaxHitPoint);
}

int			ScavTrap::getEnergyPoint( void ) const
{
	return (this->_EnergyPoint);
}

int			ScavTrap::getMaxEnergyPoint( void ) const
{
	return (this->_MaxEnergyPoint);
}

int			ScavTrap::getLevel( void ) const
{
	return (this->_Level);
}

std::string	ScavTrap::getName( void ) const
{
	return (this->_Name);
}

int			ScavTrap::getMeleeAttackDamage( void ) const
{
	return (this->_MeleeAttackDamage);
}

int			ScavTrap::getRangedAttackDamage( void ) const
{
	return (this->_RangedAttackDamage);
}

int			ScavTrap::getArmorDamageReduction( void ) const
{
	return (this->_ArmorDamageReduction);
}

void		ScavTrap::setHitPoint( int hitpoint )
{
	if ( hitpoint >= this->getMaxHitPoint() )
		this->_HitPoint = this->getMaxHitPoint();
	else if ( hitpoint < 0)
		this->_HitPoint = 0;
	else
		this->_HitPoint = hitpoint;
}
void		ScavTrap::setMaxHitPoint( int maxhitpoint )
{
	this->_MaxHitPoint = maxhitpoint;
}

void		ScavTrap::setEnergyPoint( int energypoint )
{
	this->_EnergyPoint = energypoint;
}

void		ScavTrap::setMaxEnergyPoint( int maxenergypoint )
{
	this->_MaxEnergyPoint = maxenergypoint;
}

void		ScavTrap::setLevel( int level )
{
	this->_Level = level;
}

void		ScavTrap::setName( std::string name )
{
	this->_Name = name;
}

void		ScavTrap::setMeleeAttackDamage( int meleeattackdamage )
{
	this->_MeleeAttackDamage = meleeattackdamage;
}

void		ScavTrap::setRangedAttackDamage( int attackdamage )
{
	this->_RangedAttackDamage = attackdamage;
}

void		ScavTrap::setArmorDamageReduction( int armordamagereduction )
{
	this->_ArmorDamageReduction = armordamagereduction;
}
