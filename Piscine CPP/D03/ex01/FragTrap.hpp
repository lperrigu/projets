// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   FragTrap.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 18:07:51 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/18 19:14:55 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include <iostream>

class FragTrap
{
private:
	int			_HitPoint;
	int			_MaxHitPoint;
	int			_EnergyPoint;
	int			_MaxEnergyPoint;
	int			_Level;
	std::string	_Name;
	int			_MeleeAttackDamage;
	int			_RangedAttackDamage;
	int			_ArmorDamageReduction;

	FragTrap( void );

public:
	FragTrap( std::string name );
	~FragTrap( void );
	FragTrap( FragTrap const &src );

	int			getHitPoint( void ) const;
	int			getMaxHitPoint( void ) const;
	int			getEnergyPoint( void ) const;
	int			getMaxEnergyPoint( void ) const;
	int			getLevel( void ) const;
	std::string	getName( void ) const;
	int			getMeleeAttackDamage( void ) const;
	int			getRangedAttackDamage( void ) const;
	int			getArmorDamageReduction( void ) const;

	void		setHitPoint( int hitpoint );
	void		setMaxHitPoint( int maxhitpoint );
	void		setEnergyPoint( int energypoint );
	void		setMaxEnergyPoint( int maxenergypoint );
	void		setLevel( int level );
	void		setName( std::string name );
	void		setMeleeAttackDamage( int meleeattackdamage );
	void		setRangedAttackDamage( int attackdamage );
	void		setArmorDamageReduction( int armordamagereduction );

	FragTrap	&operator=( FragTrap const &src );

	void		rangedAttack( std::string const &target ) const;
	void		MeleeAttack( std::string const &target ) const;
	void		takedDamage( unsigned int amount );
	void		beRepaired( unsigned int amount );
	void		vaulthunter_dot_exe( std::string const & target);

};

#endif
