// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 20:57:57 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 12:17:35 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int main( void )
{
	FragTrap f( "Claude" );
	ScavTrap s( "Emily" );

	std::cout << "************************FRAGTRAP OUTPUT***********************"
			  << std::endl << std::endl;
	f.rangedAttack("chien");
	f.MeleeAttack("chat");
	f.takedDamage(30);
	f.beRepaired(10);
	f.takedDamage( 300 );
	f.beRepaired( 1000 );
	f.takedDamage( 3 );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	std::cout << std::endl << std::endl;
	
	std::cout << "************************SCAVTRAP OUTPUT***********************"
			  << std::endl << std::endl;
	s.rangedAttack("chien");
	s.MeleeAttack("chat");
	s.takedDamage(30);
	s.beRepaired(10);
	s.takedDamage( 300 );
	s.beRepaired( 1000 );
	s.takedDamage( 3 );
	s.challengeNewcomer();
	s.challengeNewcomer();
	s.challengeNewcomer();
	s.challengeNewcomer();
	return 0;
}
