// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 20:57:57 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 10:57:42 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "FragTrap.hpp"

int main( void )
{
	FragTrap f( "Claude" );

	f.rangedAttack( "chien" );
	f.MeleeAttack( "chat" );
	f.takedDamage( 30 );
	f.beRepaired( 10 );
	f.takedDamage( 300 );
	f.beRepaired( 1000 );
	f.takedDamage( 3 );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	f.vaulthunter_dot_exe( "Bernard Menez" );
	return 0;
}
