// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ScavTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 21:14:29 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 16:00:28 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ScavTrap.hpp"

ScavTrap::ScavTrap( void )
{
	std::cout << "ScavTrap default constructor called" << std::endl;
}

ScavTrap::~ScavTrap( void )
{
	std::cout << "ScavTrap default destructor called on "
			  << this->getName() << std::endl;
}

ScavTrap::ScavTrap( std::string name )
{
	this->setMaxHitPoint( 100 );
	this->setHitPoint( 100 );
	this->setMaxEnergyPoint( 50 );
	this->setEnergyPoint( 50 );
	this->setLevel( 1 );
	this->setName( name );
	this->setMeleeAttackDamage( 20 );
	this->setRangedAttackDamage( 15 );
	this->setArmorDamageReduction( 3 );
	srand( time( NULL ) );
	std::cout << "String constructor called on "
			  << name << std::endl;
}

ScavTrap::ScavTrap( ScavTrap const &src )
{
	std::cout << "ScavTrap copy constructor called" << std::endl;
	*this = src;
}

ScavTrap	&ScavTrap::operator=( ScavTrap const &src)
{
	std::cout << "ScavTrap assignation operator called" << std::endl;
	this->setMaxHitPoint( src.getMaxHitPoint() );
	this->setHitPoint( src.getHitPoint() );
	this->setMaxEnergyPoint( src.getMaxEnergyPoint() );
	this->setEnergyPoint( src.getEnergyPoint() );
	this->setLevel( src.getLevel() );
	this->setName( src.getName() );
	this->setMeleeAttackDamage( src.getMeleeAttackDamage() );
	this->setRangedAttackDamage( src.getRangedAttackDamage() );
	this->setArmorDamageReduction( src.getArmorDamageReduction() );
	return *this;
}

void		ScavTrap::rangedAttack( std::string const &target ) const
{
	std::cout << "SC4V-TR4P " << this->getName()
			  << " attacks " << target
			  << " at range, causing " << this->getRangedAttackDamage()
			  << " points of damage !" << std::endl;
}

void		ScavTrap::MeleeAttack( std::string const &target ) const
{
	std::cout << "SC4V-TR4P " << this->getName()
			  << " attacks " << target
			  << " at melee, causing " << this->getMeleeAttackDamage()
			  << " points of damage !" << std::endl;
}

void		ScavTrap::takedDamage( unsigned int amount )
{
	if ( (int)amount <= this->getArmorDamageReduction() )
		std::cout << "DID NOT EVEN HURT ME !" << std::endl;
	else
	{
		amount -= this->getArmorDamageReduction();
		std::cout << "SC4V-TR4P " << this->getName()
				  << " takes " << amount
				  << " points of damage " << std::endl;
		this->setHitPoint( this->getHitPoint() - amount );
	std::cout << "SC4V-TR4P " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint" << std::endl;
	}
}

void		ScavTrap::beRepaired( unsigned int amount )
{
	std::cout << "SC4V-TR4P " << this->getName()
			  << " is repair of " << amount
			  << " points" << std::endl;
	this->setHitPoint( this->getHitPoint() + amount );
	std::cout << "SC4V-TR4P " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint" << std::endl;
}

void		ScavTrap::challengeNewcomer( void ) const
{
	std::string	challenges[5];
	int			i;

	challenges[0] = "do 200 pushups in 200 seconds";
	challenges[1] = "recognize 5 different deodorants only by smell";
	challenges[2] = "blow out 10 candles lined up in a row with one breath";
	challenges[3] = "drink 2 liters of mineral water within 1 minute";
	challenges[4] = "eat 5 bananas in 2 minutes";
	i = rand() % 5;
	std::cout << "I bet you can't " << challenges[i] << std::endl;
}
