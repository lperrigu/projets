// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ClapTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/19 12:33:04 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 15:57:27 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ClapTrap.hpp"

ClapTrap::ClapTrap( void )
{
	std::cout << "ClapTrap default constructor called" << std::endl;
}

ClapTrap::~ClapTrap( void )
{
	std::cout << "ClapTrap default destructor called on "
			  << this->getName() << std::endl;
}

ClapTrap::ClapTrap( std::string name ) :
_HitPoint( 100 ),
_MaxHitPoint( 100 ),
_EnergyPoint( 50 ),
_MaxEnergyPoint( 50 ),
_Level( 1 ),
_Name( name ),
_MeleeAttackDamage( 20 ),
_RangedAttackDamage( 15 ),
_ArmorDamageReduction( 3 )
{
	std::cout << "ClapTrap string constructor called on "
			  << name << std::endl;
}

ClapTrap::ClapTrap( ClapTrap const &src )
{
	std::cout << "ClapTrap copy constructor called" << std::endl;
	*this = src;
}

ClapTrap	&ClapTrap::operator=( ClapTrap const &src)
{
	std::cout << "ClapTrap assignation operator called" << std::endl;
	this->_Name = src.getName();
	this->_HitPoint = src.getHitPoint();
	this->_MaxHitPoint = src.getMaxHitPoint();
	this->_EnergyPoint = src.getEnergyPoint();
	this->_MaxEnergyPoint = src.getMaxEnergyPoint();
	this->_Level = src.getLevel();
	this->_MeleeAttackDamage = src.getMeleeAttackDamage();
	this->_RangedAttackDamage = src.getRangedAttackDamage();
	this->_ArmorDamageReduction = src.getArmorDamageReduction();
	return *this;
}

void		ClapTrap::rangedAttack( std::string const &target ) const
{
	std::cout << "CL4P-TR4P " << this->getName()
			  << " attacks " << target
			  << " at range, causing " << this->getRangedAttackDamage()
			  << " points of damage !" << std::endl;
}

void		ClapTrap::MeleeAttack( std::string const &target ) const
{
	std::cout << "CL4V-TR4P " << this->getName()
			  << " attacks " << target
			  << " at melee, causing " << this->getMeleeAttackDamage()
			  << " points of damage !" << std::endl;
}

void		ClapTrap::takedDamage( unsigned int amount )
{
	if ( (int)amount <= this->getArmorDamageReduction() )
		std::cout << "FOOL ! YOU CANNOT EVEN SCRATCH ME !" << std::endl;
	else
	{
		amount -= this->getArmorDamageReduction();
		std::cout << "CL4P-TR4P " << this->getName()
				  << " takes " << amount
				  << " points of damage " << std::endl;
		this->setHitPoint( this->getHitPoint() - amount );
	std::cout << "CL4P-TR4P " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint" << std::endl;
	}
}

void		ClapTrap::beRepaired( unsigned int amount )
{
	std::cout << "CL4P-TR4P " << this->getName()
			  << " is repair of " << amount
			  << " points" << std::endl;
	this->setHitPoint( this->getHitPoint() + amount );
	std::cout << "CL4P-TR4P " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint" << std::endl;
}

int			ClapTrap::getHitPoint( void ) const
{
	return (this->_HitPoint);
}
int			ClapTrap::getMaxHitPoint( void ) const
{
	return (this->_MaxHitPoint);
}

int			ClapTrap::getEnergyPoint( void ) const
{
	return (this->_EnergyPoint);
}

int			ClapTrap::getMaxEnergyPoint( void ) const
{
	return (this->_MaxEnergyPoint);
}

int			ClapTrap::getLevel( void ) const
{
	return (this->_Level);
}

std::string	ClapTrap::getName( void ) const
{
	return (this->_Name);
}

int			ClapTrap::getMeleeAttackDamage( void ) const
{
	return (this->_MeleeAttackDamage);
}

int			ClapTrap::getRangedAttackDamage( void ) const
{
	return (this->_RangedAttackDamage);
}

int			ClapTrap::getArmorDamageReduction( void ) const
{
	return (this->_ArmorDamageReduction);
}

void		ClapTrap::setHitPoint( int hitpoint )
{
	if ( hitpoint >= this->getMaxHitPoint() )
		this->_HitPoint = this->getMaxHitPoint();
	else if ( hitpoint < 0)
		this->_HitPoint = 0;
	else
		this->_HitPoint = hitpoint;
}
void		ClapTrap::setMaxHitPoint( int maxhitpoint )
{
	this->_MaxHitPoint = maxhitpoint;
}

void		ClapTrap::setEnergyPoint( int energypoint )
{
	this->_EnergyPoint = energypoint;
}

void		ClapTrap::setMaxEnergyPoint( int maxenergypoint )
{
	this->_MaxEnergyPoint = maxenergypoint;
}

void		ClapTrap::setLevel( int level )
{
	this->_Level = level;
}

void		ClapTrap::setName( std::string name )
{
	this->_Name = name;
}

void		ClapTrap::setMeleeAttackDamage( int meleeattackdamage )
{
	this->_MeleeAttackDamage = meleeattackdamage;
}

void		ClapTrap::setRangedAttackDamage( int attackdamage )
{
	this->_RangedAttackDamage = attackdamage;
}

void		ClapTrap::setArmorDamageReduction( int armordamagereduction )
{
	this->_ArmorDamageReduction = armordamagereduction;
}
