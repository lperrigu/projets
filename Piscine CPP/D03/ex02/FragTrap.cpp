// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   FragTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 18:26:18 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 15:59:46 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "FragTrap.hpp"

FragTrap::FragTrap( void )
{
	std::cout << "FragTrap default constructor called" << std::endl;
}

FragTrap::~FragTrap( void )
{
	std::cout << "FragTrap default destructor called on "
			  << this->getName() << std::endl;
}

FragTrap::FragTrap( std::string name )
{
	this->setMaxHitPoint( 100 );
	this->setHitPoint( 100 );
	this->setMaxEnergyPoint( 100 );
	this->setEnergyPoint( 100 );
	this->setLevel( 1 );
	this->setName( name );
	this->setMeleeAttackDamage( 30 );
	this->setRangedAttackDamage( 20 );
	this->setArmorDamageReduction( 5 );
	srand( time( NULL ) );
	std::cout << "FragTrap string constructor called on "
			  << name << std::endl;
}

FragTrap::FragTrap( FragTrap const &src )
{
	std::cout << "FragTrap copy constructor called" << std::endl;
	*this = src;
}

FragTrap	&FragTrap::operator=( FragTrap const &src)
{
	std::cout << "FragTrap assignation operator called" << std::endl;
	this->setMaxHitPoint( src.getMaxHitPoint() );
	this->setHitPoint( src.getHitPoint() );
	this->setMaxEnergyPoint( src.getMaxEnergyPoint() );
	this->setEnergyPoint( src.getEnergyPoint() );
	this->setLevel( src.getLevel() );
	this->setName( src.getName() );
	this->setMeleeAttackDamage( src.getMeleeAttackDamage() );
	this->setRangedAttackDamage( src.getRangedAttackDamage() );
	this->setArmorDamageReduction( src.getArmorDamageReduction() );
	return *this;
}

void		FragTrap::rangedAttack( std::string const &target ) const
{
	std::cout << "FR4G-TP " << this->getName()
			  << " attacks " << target
			  << " at range, causing " << this->getRangedAttackDamage()
			  << " points of damage !" << std::endl;
}

void		FragTrap::MeleeAttack( std::string const &target ) const
{
	std::cout << "FR4G-TP " << this->getName()
			  << " attacks " << target
			  << " at melee, causing " << this->getMeleeAttackDamage()
			  << " points of damage !" << std::endl;
}

void		FragTrap::takedDamage( unsigned int amount )
{
	if ( (int)amount <= this->getArmorDamageReduction() )
		std::cout << "FOOL ! YOU CANNOT EVEN SCRATCH ME !" << std::endl;
	else
	{
		amount -= this->getArmorDamageReduction();
		std::cout << "FR4G-TP " << this->getName()
				  << " takes " << amount
				  << " points of damage" << std::endl;
		this->setHitPoint( this->getHitPoint() - amount );
	std::cout << "FR4G-TP " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint"<< std::endl;
	}
}

void		FragTrap::beRepaired( unsigned int amount )
{
	std::cout << "FR4G-TP " << this->getName()
			  << " is repair of " << amount
			  << " points" << std::endl;
	this->setHitPoint( this->getHitPoint() + amount );
	std::cout << "FR4G-TP " << this->getName()
			  << " is now at " << this->getHitPoint()
			  << " HitPoint"<< std::endl;
}

void		FragTrap::vaulthunter_dot_exe( std::string const & target)
{
	if ( this->getEnergyPoint() >= 25 )
	{
		this->setEnergyPoint( this->getEnergyPoint() - 25);
		std::string	actions[5];
		int			i;

		actions[0] = "A disco ball floats shooting shock lasers";
		actions[1] = "A disco ball floats shooting fire lasers";
		actions[2] = "A disco ball floats shooting corrosion lasers";
		actions[3] = "A disco ball floats shooting BFG lasers";
		actions[4] = "A disco ball floats shooting ice lasers";
		i = rand() % 5;
		std::cout << actions[i]
				  << " dealing tons of damage to " << target << std::endl;
	}
	else
		std::cout << "Not enough energy point, minion" << std::endl;
}
