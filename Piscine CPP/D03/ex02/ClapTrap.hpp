// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ClapTrap.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/19 12:30:46 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/19 14:31:36 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CLAPTRAP_HPP
#define CLAPTRAP_HPP

#include <iostream>

class ClapTrap
{
private:
	int			_HitPoint;
	int			_MaxHitPoint;
	int			_EnergyPoint;
	int			_MaxEnergyPoint;
	int			_Level;
	std::string	_Name;
	int			_MeleeAttackDamage;
	int			_RangedAttackDamage;
	int			_ArmorDamageReduction;

protected:
	ClapTrap( void );

public:
	ClapTrap( std::string name );
	~ClapTrap( void );
	ClapTrap( ClapTrap const &src );

	int			getHitPoint( void ) const;
	int			getMaxHitPoint( void ) const;
	int			getEnergyPoint( void ) const;
	int			getMaxEnergyPoint( void ) const;
	int			getLevel( void ) const;
	std::string	getName( void ) const;
	int			getMeleeAttackDamage( void ) const;
	int			getRangedAttackDamage( void ) const;
	int			getArmorDamageReduction( void ) const;

	void		setHitPoint( int hitpoint );
	void		setMaxHitPoint( int maxhitpoint );
	void		setEnergyPoint( int energypoint );
	void		setMaxEnergyPoint( int maxenergypoint );
	void		setLevel( int level );
	void		setName( std::string name );
	void		setMeleeAttackDamage( int meleeattackdamage );
	void		setRangedAttackDamage( int attackdamage );
	void		setArmorDamageReduction( int armordamagereduction );

	ClapTrap	&operator=( ClapTrap const &src );

	void		rangedAttack( std::string const &target ) const;
	void		MeleeAttack( std::string const &target ) const;
	void		takedDamage( unsigned int amount );
	void		beRepaired( unsigned int amount );

};

#endif
