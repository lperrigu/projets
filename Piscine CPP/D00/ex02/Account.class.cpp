// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Account.class.cpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 18:30:01 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/15 21:22:17 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include <ctime>
#include "Account.class.hpp"

int		Account::_nbAccounts = 0;
int		Account::_totalAmount = 0;
int		Account::_totalNbDeposits = 0;
int		Account::_totalNbWithdrawals = 0;

Account::Account( int initial_deposit )
{
	Account::_displayTimestamp();
	this->_accountIndex = Account::_nbAccounts;
	++Account::_nbAccounts;
	this->_amount = initial_deposit;
	Account::_totalAmount += initial_deposit;
	this->_nbDeposits = 0;
	this->_nbWithdrawals = 0;
	std::cout << "index:" << this->_accountIndex
			  << ";amount:" << this->_amount
			  << ";created" << std::endl;
}

Account::~Account( void )
{
	Account::_displayTimestamp();
	std::cout << "index:" << this->_accountIndex
			  << ";amount:" << this->_amount
			  << ";closed" << std::endl;
}

void		Account::_displayTimestamp( void )
{
	const struct tm	*timeinfo;
	time_t			RawTime;
	int				to_display;

	RawTime = time(NULL);
	timeinfo = localtime(&RawTime);
	to_display = timeinfo->tm_year + 1900;
//	std::cout << '|' << to_display << '|' << std::endl;
	std::cout << '[' << to_display;
	to_display = timeinfo->tm_mon;
	if (to_display < 10)
		std::cout << '0' << to_display;
	else
		std::cout << to_display;
	to_display = timeinfo->tm_mday;
	if (to_display < 10)
		std::cout << '0' << to_display;
	else
		std::cout << to_display;
	std::cout << '_' << timeinfo->tm_hour
			  << timeinfo->tm_min
			  << timeinfo->tm_sec << "] ";
}

int			Account::getNbAccounts( void )
{
	return Account::_nbAccounts;
}

int			Account::getTotalAmount( void )
{
	return Account::_totalAmount;
}

int			Account::getNbDeposits( void )
{
	return Account::_totalNbDeposits;
}

int		Account::getNbWithdrawals( void )
{
	return Account::_totalNbWithdrawals;
}

void		Account::displayAccountsInfos( void )
{
	Account::_displayTimestamp();
	std::cout << "accounts:" << getNbAccounts()
			  << ";total:" << getTotalAmount()
			  << ";deposits:" << getNbDeposits()
			  << ";withdrawals:" << getNbWithdrawals() << std::endl;
}

void		Account::makeDeposit( int deposit)
{
	Account::_displayTimestamp();
	std::cout << "index:" << this->_accountIndex
			  << ";amount:" << this->_amount
			  << ";deposit" << deposit;
	this->_amount += deposit;
	Account::_totalAmount += deposit;
	++this->_nbDeposits;
	++Account::_totalAmount;
	std::cout << ";amount:" << this->_amount
			  << ";nb_deposits:" << this->_nbDeposits << std::endl;
}

bool		Account::makeWithdrawal( int withdrawal)
{
	Account::_displayTimestamp();
	std::cout << "index:" << this->_accountIndex
			  << ";amount:" << this->_amount
			  << ";withdrawal:";
	if (this->_amount >= withdrawal)
	{
		this->_amount -= withdrawal;
		Account::_totalAmount -= withdrawal;
		++this->_nbWithdrawals;
		--Account::_totalAmount;
		std::cout << withdrawal
				  << ";amount:" << this->_amount
				  << ";nb_widthdrawals:" << this->_nbWithdrawals << std::endl;
		return (true);
	}
	else
	{
		std::cout << "refused" << std::endl;
		return (false);
	}
}

int			Account::checkAmount( void) const
{
	return this->_amount;
}

void		Account::displayStatus( void) const
{
	Account::_displayTimestamp();
	std::cout << "index:" << this->_accountIndex
			  << ";amount:" << this->_amount
			  << ";deposit:" << this->_nbDeposits
			  << ";withdrawals:" << this->_nbWithdrawals << std::endl;
}

