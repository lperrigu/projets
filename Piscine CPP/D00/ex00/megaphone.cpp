// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   megaphone.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 10:43:59 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/15 11:51:08 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>

int main( int ac, char **av )
{
	int		i = 1;
	int		j;
	char	c;

	if ( ac == 1 )
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
	else
	{
		while ( i < ac )
		{
			j = 0;
			while ( av[i][j] != '\0')
			{
				c = toupper(av[i][j]);
				std::cout << c;
				++j;
			}
			++i;
		}
		std::cout << std::endl;
	}
	return (0);
}
