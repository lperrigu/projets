// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   contact.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 11:41:29 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/15 21:35:34 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "contact.hpp"
#include <iomanip>
#include <iostream>
#include <cstdlib>

contact::contact( void )
{
	this->_index = -1;
}

contact::~contact( void )
{
}

int		contact::add(int index)
{
	if (this->_index != -1)
		return (0);
	this->_index = index;
	std::cout << "First name of the contact to add : ";
	std::getline(std::cin, this->_first_name);
	if (this->_first_name.empty())
		exit(0);
	std::cout << "Last name of the contact to add : ";
	std::getline(std::cin, this->_last_name);
	if (this->_last_name.empty())
		exit(0);
	std::cout << "Nickname of the contact to add : ";
	std::getline(std::cin,this->_nickname);
	if (this->_nickname.empty())
		exit(0);
	std::cout << "Login of the contact to add : ";
	std::getline(std::cin, this->_login);
	if (this->_login.empty())
		exit(0);
	std::cout << "Postal address of the contact to add : ";
	std::getline(std::cin, this->_postal_address);
	if (this->_postal_address.empty())
		exit(0);
	std::cout << "Email address of the contact to add : ";
	std::getline(std::cin, this->_email_address);
	if (this->_email_address.empty())
		exit(0);
	std::cout << "Phone number of the contact to add : ";
	std::getline(std::cin, this->_phone_number);
	if (this->_phone_number.empty())
		exit(0);
	std::cout << "Birthday date of the contact to add : ";
	std::getline(std::cin, this->_birthday_date);
	if (this->_birthday_date.empty())
		exit(0);
	std::cout << "Favorite meal of the contact to add : ";
	std::getline(std::cin, this->_favorite_meal);
	if (this->_favorite_meal.empty())
		exit(0);
	std::cout << "Underwear color of the contact to add : ";
	std::getline(std::cin, this->_underwear_color);
	if (this->_underwear_color.empty())
		exit(0);
	std::cout << "Darkest secret of the contact to add : ";
	std::getline(std::cin, this->_darkest_secret);
	if (this->_darkest_secret.empty())
		exit(0);
	return (1);
}

void	contact::display(void) const
{
	int	i;

	if (this->_index == -1)
		return ;
	std::cout << std::setw(10);
	std::cout << this->_index;
	std::cout << std::setw(1);
	std::cout << '|';
	if (this->_first_name.size() > 10)
	{
		i = 0;
		while (i < 9)
		{
			std::cout << this->_first_name[i];
			++i;
		}
		std::cout << ".|";
	}
	else
	{
		std::cout << std::setw(10);
		std::cout << this->_first_name;
		std::cout << std::setw(1);
		std::cout << '|';
	}
	if (this->_last_name.size() > 10)
	{
		i = 0;
		while (i < 9)
		{
			std::cout << this->_last_name[i];
			++i;
		}
		std::cout << ".|";
	}
	else
	{
		std::cout << std::setw(10);
		std::cout << this->_last_name;
		std::cout << std::setw(1);
		std::cout << '|';
	}
	if (this->_nickname.size() > 10)
	{
		i = 0;
		while (i < 9)
		{
			std::cout << this->_nickname[i];
			++i;
		}
		std::cout << '.';
	}
	else
	{
		std::cout << std::setw(10);
		std::cout << this->_nickname;
	}
	std::cout << std::setw(1);
	std::cout << std::endl;
}

void	contact::print(void) const
{
	if (this->_index == -1)
		return ;
	std::cout << "First name: ";
	std::cout << this->_first_name << std::endl;
	std::cout << "Last name: ";
	std::cout << this->_last_name << std::endl;
	std::cout << "Nickname: ";
	std::cout << this->_nickname << std::endl;
	std::cout << "Login: ";
	std::cout << this->_login << std::endl;
	std::cout << "Postal address: ";
	std::cout << this->_postal_address << std::endl;
	std::cout << "Email address: ";
	std::cout << this->_email_address << std::endl;
	std::cout << "Phone number: ";
	std::cout << this->_phone_number << std::endl;
	std::cout << "Birthday date: ";
	std::cout << this->_birthday_date << std::endl;
	std::cout << "Favorite meal: ";
	std::cout << this->_favorite_meal << std::endl;
	std::cout << "Underwear color: ";
	std::cout << this->_underwear_color << std::endl;
	std::cout << "Darkest secret: ";
	std::cout << this->_darkest_secret << std::endl;
}
