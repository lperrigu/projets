// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   contact.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 11:42:09 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 12:10:51 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CONTACT_HPP
# define CONTACT_HPP

#include <string>

class contact
{
private:
	int			_index;
	std::string	_first_name;
	std::string	_last_name;
	std::string	_nickname;
	std::string	_login;
	std::string	_postal_address;
	std::string	_email_address;
	std::string	_phone_number;
	std::string	_birthday_date;
	std::string	_favorite_meal;
	std::string	_underwear_color;
	std::string	_darkest_secret;

public:

	contact ( void );
	~contact ( void );

	int		add( int index );
	void	display( void ) const;
	void	print( void ) const;
};

#endif
