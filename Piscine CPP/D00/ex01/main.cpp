// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 11:45:58 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 18:42:55 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include <cctype>
#include <cstdlib>
#include <sstream>
#include "contact.hpp"

static int	correct_param(std::string input)
{
	int					i;
	std::stringstream	convert(input);

	i = 0;
	while (input[i] != '\0')
	{
		if (isdigit(input[i]) != 1)
			return (-1);
		++i;
	}
	convert >> i;
	std::cout << i << std::endl;
	if (i >= 0 && i < 8)
		return (i);
	else
		return (-1);
}

int main( void )
{
	contact		contacts[8];
	std::string	input;
	int			i;

	while (1)
	{
		std::cout << "Enter ADD to add a contact,"
				  << " SEARCH to search an existing contact,"
				  << " or EXIT to quit the program" << std::endl;
		std::getline(std::cin, input);
		if (input.compare("ADD") == 0)
		{
			i = 0;
			while (contacts[i].add(i) == 0 && i < 8)
				++i;
			if (i == 8)
				std::cout << "Contacts full" << std::endl;
		}
		else if (input.compare("SEARCH") == 0)
		{
			i = 0;
			while (i < 8)
			{
				contacts[i].display();
				++i;
			}
			std::cout << "Enter the index of the desired entry: ";
			std::getline(std::cin, input);
			i = correct_param(input);
			if (i >= 0 && i < 8)
				contacts[i].print();
			else
				std::cout << "Unknow index" << std::endl;
		}
		else if (input.compare("EXIT") == 0)
			return 1;
		else if (input.empty())
			return -1;
		std::cout << std::endl;
	}
	return 0;
}
