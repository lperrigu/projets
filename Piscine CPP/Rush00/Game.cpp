// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Game.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 20:15:07 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/21 21:34:45 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Game.hpp"

Game::Game( int width, int height ) : _score( 0 ), _width( width ),
									  _height( height )
{
	srand( time( NULL ) );
	this->_head_list = new Player( width / 2, height - 1, '=' );
}

Game::~Game( void )
{
	Element	*tmp;
	Element	*tmp2;

	tmp = this->_head_list;
	tmp2 = tmp->getNext();
	while (tmp2 != NULL)
	{
//		delete tmp;//segfault
		tmp = tmp2;
		tmp2 = tmp2->getNext();
	}
//	delete tmp;//segfault
}

void	Game::play( void )
{
//	WINDOW	*my_win;

//	cbreak();
//	keypad(stdscr, TRUE);
//	refresh();
//	getmaxyx(stdscr, )
//	my_win = newwin();
//	my_win = create_newwin(height, width, grid);
	while (1)
	{
//		std::cout << "test" << std::endl;
//		std::cout << this->getWidth() << this->getHead()->getC()  << std::endl;
		this->display();
		if ( this->update() )
			return ;
		this->newEnemies();
	}
}
void	Game::display( void ) const//temp sans ncurses
{
	int x;
	int y;
	Element	*tmp;
	int		test;

	for ( y = 0; y < this->getHeight(); ++y )
	{
		for ( x = 0; x < this->getWidth(); ++x )
		{
			test = 0;
			tmp = this->getHead();
			while (tmp != NULL)
			{
				if ( tmp->getX() == x && tmp->getY() == y )
				{
					std::cout << tmp->getC();
					test = 1;
				}
				tmp = tmp->getNext();
			}
			if (test == 0)
				std::cout << ' ';
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}
/*
void	Game::display( void ) const
{
	Element	*tmp;

	tmp = this->getHead();
	while (tmp != NULL)
	{
		tmp->display();
		tmp = tmp->getNext();
	}
}
*/
void	Game::deleteElem( Element *toDelete, Element *prev)
{
	(void)toDelete;
	(void)prev;
//	prev->setNext( toDelete->getNext() );
//	std::cout<< "delelem " << (void *)toDelete << std::endl;
//	std::cout << "ALLO1" << std::endl;
//	delete toDelete;
//	std::cout << "ALLO2" << std::endl;
}

int		Game::update( void )
{
	Element	*tmp;
	Element	*tmp2;
	int		ret;

	tmp = this->getHead();
	tmp2 = NULL;
	while (tmp != NULL)
	{
		ret = tmp->update( this->getHead(), this->getWidth(), this->getHeight() );
		if ( ret == PLAYER_DEATH )
			return 1;
		else if ( ret == TO_DELETE )
		{
//			std::cout << tmp->getX() << tmp->getY() << std::endl;
			this->deleteElem( tmp, tmp2 );
		}
		tmp2 = tmp;
		tmp = tmp->getNext();
	}
	return 0;
}

void	Game::newEnemies( void )
{
	int		x = 0;
	Element	*tmp;

	tmp = this->getHead();
	while (tmp->getNext() != NULL)
		tmp = tmp->getNext();
	while ( x < this->getWidth() - 1 )
	{
		if ( rand() % 10 == 0 )
		{
//			tmp->SetNext( new Enemy( x, this->getHeight() - 1, 'o' ) );
			tmp->setNext( new Enemy( x, 0, 'o' ) );
			tmp = tmp->getNext();
//			std::cout << "bew enemy" << (void *)tmp << std::endl;
			tmp->setNext( NULL );
		}
		++x;
	}
}

int			Game::getScore( void ) const
{
	return this->_score;
}
int			Game::getWidth( void ) const
{
	return this->_width;
}
int			Game::getHeight( void ) const
{
	return this->_height;
}
Element		*Game::getHead( void ) const
{
	return this->_head_list;
}
