// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Element.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 15:42:36 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/21 19:01:40 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Element.hpp"

Element::Element( void ) : _c ( '?' )
{
}

Element::Element( int x, int y, char c ) : _x( x ), _y ( y ), _c( c ),
										   _next( NULL )
{
}

Element::~Element( void )
{
}

Element::Element( Element const &src ) : _c ( src.getC() )
{
	*this = src;
}

Element		&Element::operator=( Element const &src )
{
	this->setX( src.getX() );
	this->setY( src.getY() );
	return *this;
}

void		Element::display( void ) const
{

}

int			Element::update( Element *Head, int width, int height)
{
	(void)Head;
	(void)width;
	(void)height;
	return (0);
}

// ************************************************************************** //
// *********************************SET AND GET****************************** //
// ************************************************************************** //

int			Element::getX( void ) const
{
	return ( this->_x );
}

int			Element::getY( void ) const
{
	return ( this->_y );
}

char		Element::getC( void ) const
{
	return ( this->_c );
}

Element		*Element::getNext( void ) const
{
	return ( this->_next );
}

void	Element::setX( int x )
{
	this->_x = x;
}

void	Element::setY( int y )
{
	this->_y = y;
}

void	Element::setNext( Element *next )
{
	this->_next = next;
}

// *************************************END********************************** //
