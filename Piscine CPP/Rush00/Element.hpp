// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Element.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 15:12:19 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/21 18:47:59 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ELEMENT_HPP
#define ELEMENT_HPP

#define PLAYER_DEATH 1
#define TO_DELETE 2

//#include "Player.hpp"
#include <string>

class Element
{
private:
	int			_x;
	int			_y;
	char const	_c;
	Element		*_next;

public:
	Element( void );
	Element( int x, int y, char c );
	virtual ~Element( void );
	Element( Element const &src );

	int			getX( void ) const;
	int			getY( void ) const;
	char		getC( void ) const;
	Element		*getNext( void ) const;

	void		setX( int x );
	void		setY( int y );
	void		setNext( Element *E );

	Element		&operator=( Element const &src );

	virtual void	display( void ) const;
	virtual int		update( Element *Head, int width, int height );
};

#endif
