// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   maintest.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 20:20:20 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/21 21:34:25 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Game.hpp"
#include "Element.hpp"
#include "Player.hpp"
#include "Enemy.hpp"
//#include <ncurses.h>
//#include <iostream>

int main( void )
{
//	WINDOW  *my_win;
//	int		width;
//	int		height;
	Game	g( 70, 30 );

//	cbreak();
//	keypad(stdscr, TRUE);
//	refresh();
//	getmaxyx(stdscr, height, width);
//	my_win = newwin();
//	std::cout << width << " " << height << std::endl;
	g.play();
	return 0;
}
