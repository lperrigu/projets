// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Player.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/21 15:28:25 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/21 19:20:41 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Player.hpp"

Player::Player( void ) : Element( 0, 0, '=' )
{
}

Player::~Player( void )
{
}

Player::Player( int x, int y, char c ) : Element( x, y, c )
{
}
/*
Player::Player( Player const &src ) : _c ( src.getC() )
{
    *this = src;
}

Element		&Element::operator=( Element const &src )
{
    this->setX( src.getX() );
    this->setY( src.getY() );
    return *this;
}
*/
void	Player::display( void ) const
{
}

int		Player::update( Element *Head, int width, int height )
{
	Element *tmp;

	(void)width;
	(void)height;
	tmp = Head;
	while ( tmp != NULL )
		tmp = tmp->getNext();
	return ( 0 );
}
