// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/21 11:18:16 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/21 18:07:55 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ENEMY_HPP
#define ENEMY_HPP

#include "Element.hpp"

class Enemy : public Element
{
private:
	int	_hp;

public:
	Enemy( void );
	Enemy( int x, int y, char c );
	virtual ~Enemy( void );
	Enemy( Enemy const &src );

	Enemy		&operator=( Enemy const &src );

	void	display ( void ) const;
	int		update( Element *Head, int width, int height );
};

#endif
