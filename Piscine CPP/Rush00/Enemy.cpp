// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/21 17:50:49 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/21 19:47:06 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Enemy.hpp"

Enemy::Enemy( void ) : Element( 0, 0, '=' )
{
	this->_hp = 1;
}

Enemy::~Enemy( void )
{
}

Enemy::Enemy( int x, int y, char c) : Element( x, y, c )
{
}

void	Enemy::display( void ) const
{
}

int		Enemy::update( Element *Head, int width, int height )
{
//	Element *tmp;

	(void)width;
	if ( Head->getX() == this->getX() && Head->getY()  == this->getY() + 1 )
		return ( PLAYER_DEATH );
	this->setY( this->getY() + 1 );
//	if ( this->getX() >= height )
	if ( this->getY() >= height )
		return ( TO_DELETE );
	return (0);
//	tmp = Head;
//	while ( tmp != NULL )
//	{
//		if ()
//		tmp = tmp->getNext();
//	}
}
