// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Player.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 19:57:41 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/21 18:58:35 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Element.hpp"

class Player : public Element
{
private:
	int	_hp;
	Player( void );

public:
	Player( int x, int y, char c );
	virtual ~Player( void );
	Player( Player const &src );

	Player		&operator=( Player const &src );

	void	display ( void ) const;
	int		update( Element *Head, int width, int height );
};

#endif
