// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Game.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/20 20:08:02 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/21 20:24:33 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef GAME_HPP
#define GAME_HPP

//#include "Element.hpp"
#include "Player.hpp"
#include "Enemy.hpp"
#include <string>
#include <iostream>
#include <ncurses.h>

class Game
{
private:
	int		_score;
	int		_width;
	int		_height;
	Element	*_head_list;

	Game( void );
	Game( Game const &src );

	Game		&operator=( Game const &src );

public:
	Game( int width, int height );
	virtual ~Game( void );

	int			getScore( void ) const;
	int			getWidth( void ) const;
	int			getHeight( void ) const;
	Element		*getHead( void ) const;

	void	play( void );
	void	display( void ) const;
	void	deleteElem( Element *toDelete, Element *prev);
	int		update( void );
	void	newEnemies( void );
};

#endif
