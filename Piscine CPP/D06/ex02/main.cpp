// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/23 16:46:07 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/24 13:55:16 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>

class Base{ public: virtual ~Base( void ){} };
class A: public Base {};
class B: public Base {};
class C: public Base {};

Base	*generate( void )
{
	int		i = std::rand();
	

	if (i % 3 == 0)
	{
		std::cout << "A class generated" << std::endl;
		return ( new A() );
	}
	else if (i % 3 == 1)
	{
		std::cout << "B class generated" << std::endl;
		return ( new B() );
	}
	else
	{
		std::cout << "C class generated" << std::endl;
		return ( new C() );
	}
}

void identify_from_pointer( Base *p )
{
	A	*new_A = dynamic_cast<A *>(p);
	if (new_A == NULL)
	{
		B	*new_B = dynamic_cast<B *>(p);
		if (new_B == NULL)
			std::cout << 'C' << std::endl;
		else
			std::cout << 'B' << std::endl;
	}
	else
		std::cout << 'A' << std::endl;
}

void identify_from_reference( Base &p )
{
	try
	{
		A	&new_A = dynamic_cast<A &>(p);
		std::cout << 'A' << std::endl;
		static_cast<void>(new_A);
	}
	catch( std::bad_cast &bc )
	{
		try
		{
			B	&new_B = dynamic_cast<B &>(p);
			std::cout << 'B' << std::endl;
			static_cast<void>(new_B);
		}
		catch( std::bad_cast &bc )
		{
			std::cout << 'C' << std::endl;
		}
	}
}

int main( void )
{
	Base	*b;

	srand( time ( NULL ) );
	b = generate();
	Base	&r = *b;
	std::cout << "Call to identify_from_pointer : ";
	identify_from_pointer( b );
	std::cout << "Call to identify_from_reference : ";
	identify_from_reference( r );
	return 0;
}
