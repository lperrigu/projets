// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/23 15:21:34 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/24 17:28:41 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include <sstream>

struct Data { std::string s1; int n; std::string s2; };

void *serialize( void )
{
	char	c;
	int		i;
	int		*n;
	char	*ret = new char[ 8 + sizeof(int) + 8 ];
	char	alphanum[] =
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	for ( i = 0; i < 8; ++i )
	{
		c = alphanum
			[ static_cast<unsigned long>( std::rand() ) % sizeof( alphanum ) ];
		std::cout << c;
		ret[i] = c;
	}
	std::cout << std::endl;

	n =  reinterpret_cast<int*>(ret + 8);
	*n = rand();
	std::cout << *n << std::endl;

	for ( i = 12; i < 20; ++i )
	{
		c = alphanum
			[ static_cast<unsigned long>( std::rand() ) % sizeof( alphanum ) ];
		std::cout << c;
		ret[i] = c;
	}
	std::cout << std::endl;

	return ( static_cast<void *>(ret) );
}

Data *deserialize( void *raw )
{
	char	*tmp;
	Data	*ret = new Data();
	int		i;
	int		*n;

	tmp = reinterpret_cast<char *>(raw);
	for ( i = 0; i < 8; ++i )
		ret->s1 += tmp[i];
	n = reinterpret_cast<int *>(tmp + 8);
	ret->n = *n;
	for ( i = 12; i < 20; ++i )
		ret->s2 += tmp[i];
	return ret;
}

int main( void )
{
	void	*ret;
	Data	*D;

	srand( static_cast<unsigned int>( time( NULL ) ) );

	ret = serialize();
	std::cout << std::endl;
	D = deserialize( ret );
	std::cout << D->s1 << std::endl;
	std::cout << D->n << std::endl;
	std::cout << D->s2 << std::endl;
	return 0;
}
