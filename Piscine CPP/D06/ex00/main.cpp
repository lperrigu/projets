// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/23 12:40:54 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/24 18:13:43 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <cstdlib>
#include <iostream>
#include <limits>
#include "Convert.hpp"

int		main(int ac, char **av)
{
	double	input;
	float	f;
	int		i;
	char	c;

	if ( ac != 2 )
		return 0;
	input = atof( av[1] );
	Convert	C( input );
	if (av[1][1] == '\0')
		C.setValue( av[1][0] );

	f = C;
	i = C;
	c = C;
	std::cout << std::fixed;

	if ( C.getValue() != C.getValue() )
		std::cout << "char: impossible" << std::endl;
	else if ( std::numeric_limits<double>::infinity() == C.getValue() )
		std::cout << "char: impossible" << std::endl;
	else if ( -std::numeric_limits<double>::infinity() == C.getValue() )
		std::cout << "char: impossible" << std::endl;
	else if ( 32 <= c && c <= 126)
		std::cout << "char: '" << c << '\'' << std::endl;
	else
		std::cout << "char: Non displayable" << std::endl;

	if ( C.getValue() != C.getValue() )
		std::cout << "int: impossible" << std::endl;
	else if ( std::numeric_limits<double>::infinity() == C.getValue() )
		std::cout << "int: impossible" << std::endl;
	else if ( -std::numeric_limits<double>::infinity() == C.getValue() )
		std::cout << "int: impossible" << std::endl;
	else
		std::cout << "int: " << i << std::endl;

	if ( C.getDecimal() == 1 )
		std::cout << "float: " << f << 'f' << std::endl;
	else
		std::cout << "float: " << f << ".0f" << std::endl;

	if ( C.getDecimal() == 1 )
		std::cout << "double: " << C.getValue() << std::endl;
	else
		std::cout << "double: " << C.getValue() << ".0" << std::endl;
	return 0;
}
