// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Convert.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/23 14:09:30 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/23 14:14:25 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CONVERT_HPP
#define CONVERT_HPP

class Convert
{
private:
	double _value;

	Convert( void );
	Convert( Convert const & );
	Convert& operator=( Convert const & );

public:
	Convert( double value );
	virtual ~Convert( void );

	double	getValue( void ) const;
	void	setValue( double v );

	bool	getDecimal( void ) const;

	operator float();
	operator int();
	operator char();
};


#endif
