// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Convert.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/23 15:12:42 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/24 18:11:45 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Convert.hpp"

Convert::Convert( double value ): _value( value )
{
}

Convert::~Convert( void )
{
}

double	Convert::getValue( void ) const
{
	return this->_value;
}

void	Convert::setValue( double v )
{
	this->_value = v;
}

bool	Convert::getDecimal( void ) const
{
	int	i;
	double	deci;

	i = static_cast<int>(this->_value);
	deci = this->_value - static_cast<double>(i);
	return (deci != 0);
}

Convert::operator float()
{
	return static_cast<float>(this->_value);
}

Convert::operator int()
{
	return static_cast<int>(this->_value);
}

Convert::operator char()
{
	return static_cast<char>(this->_value);
}
