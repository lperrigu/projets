// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Weapon.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 17:17:21 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/17 11:15:06 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Weapon.hpp"

Weapon::Weapon( std::string type )
{
	this->_type = type;
}

Weapon::~Weapon( void )
{

}

const std::string	Weapon::getType( void ) const
{
	return (this->_type);
}

void				Weapon::setType( std::string type )
{	
	this->_type = type;
}
