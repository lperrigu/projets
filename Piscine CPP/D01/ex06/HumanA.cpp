// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   HumanA.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 17:35:23 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/17 10:36:10 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "HumanA.hpp"

HumanA::HumanA(std::string name, Weapon &weapon) : _name( name ), _weapon(weapon)
{
}

HumanA::~HumanA( void )
{

}

void	HumanA::attack( void )
{
	std::cout << this->_name << " attacks with his "
			  << this->_weapon.getType() << std::endl;
}
