// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Brain.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 16:15:20 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 16:50:50 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Brain.hpp"

Brain::Brain( void )
{
	this->_BrainAddress = this;
}

Brain::~Brain( void )
{

}

std::string Brain::identify( void ) const
{
	std::ostringstream oss;

	oss << this->_BrainAddress;
	std::string s(oss.str());
	return (s);
}
