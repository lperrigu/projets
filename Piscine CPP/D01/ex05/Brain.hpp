// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Brain.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 15:37:00 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 16:48:12 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BRAIN_HPP
#define BRAIN_HPP

#include <string>
#include <sstream>

class Brain
{
private:
	void	*_BrainAddress;

public:
	Brain( void );
	~Brain( void );

	std::string	identify( void ) const;
};

#endif
