// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Human.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 15:54:57 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 16:53:49 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef HUMAN_HPP
#define HUMAN_HPP

#include "Brain.hpp"
#include <string>

class Human
{
private:
	const Brain	_brain;

public:
	Human( void );
	~Human( void );

	std::string	identify( void ) const;
	const Brain		getBrain( void );
};

#endif
