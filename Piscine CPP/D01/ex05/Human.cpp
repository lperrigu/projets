// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Human.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 16:00:20 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 16:54:35 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Human.hpp"

Human::Human( void ) : _brain( Brain() )
{
}

Human::~Human( void )
{
}

std::string Human::identify( void ) const
{
	return this->_brain.identify();
}

const Brain	Human::getBrain( void )
{
	return this->_brain;
}
