// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Zombie.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 11:41:25 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 12:54:28 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ZOMBIE_HPP
#define ZOMBIE_HPP

//#include <iostream>
#include <string>

class Zombie
{
private:
	std::string _type;
	std::string _name;

public:
	Zombie( std::string name );
	~Zombie( void );

	void	announce( void ) const;
	void	setType( std::string type );
};

#endif
