// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 13:54:53 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 14:06:33 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Zombie.hpp"
#include "ZombieEvent.hpp"
#include <iostream>

int		main( void )
{
	ZombieEvent		Event = ZombieEvent();
	Zombie			*zombie;
	std::string input;

	while (1)
	{
		std::cout << "Entre un type de zombie: ";
		std::getline(std::cin, input);
		if (input.empty())
			return -1;
		Event.setZombieType(input);
		std::cout << "Entre un nom de zombie: ";
		std::getline(std::cin, input);
		if (input.empty())
			return -1;
		zombie = Event.newZombie(input);
		zombie->announce();
		delete zombie;
		std::cout << "Un zombie random : " << std::endl;
		Event.randomChump();
	}
	return (0);
}
