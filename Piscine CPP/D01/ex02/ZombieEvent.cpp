// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ZombieEvent.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 12:45:45 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 14:09:08 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ZombieEvent.hpp"
#include <string>
#include <cstdlib>
#include <ctime>

ZombieEvent::ZombieEvent( void )
{
}

ZombieEvent::~ZombieEvent( void )
{
}

Zombie	*ZombieEvent::newZombie( std::string name )
{
	Zombie	*newZombie = new Zombie(name);

	newZombie->setType(this->_ZombieType);
	return (newZombie);
}

void	ZombieEvent::setZombieType( std::string new_type )
{
	this->_ZombieType = new_type;
}

void	ZombieEvent::randomChump( void )
{
	std::string	randomName;
	int			i;

	i = 0;
	while (i < 13)
	{
		randomName += (std::rand() * time(NULL)) % 26 + 97;
		++i;
	}
	Zombie	randomZombie = Zombie(randomName);
	randomZombie.setType(this->_ZombieType);
	randomZombie.announce();
}
