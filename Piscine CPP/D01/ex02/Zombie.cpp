// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Zombie.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 11:42:56 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 14:07:10 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "zombie.hpp"

Zombie::Zombie(std::string name)
{
	this->_name = name;
	std::cout << "Zombie " << name << " created" << std::endl;
}

Zombie::~Zombie( void )
{
	std::cout << "Zombie " << this->_name << " really dead now" << std::endl;
}

void	Zombie::setType(std::string type)
{
	this->_type = type;
}

void	Zombie::announce(void) const
{
	std::cout << '<' << this->_name
			  << " (" << this->_type
			  << ")> Braiiiiiiinnnssss..." << std::endl;
}
