// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ZombieEvent.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 12:20:22 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 13:56:47 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ZOMBIEEVENT_HPP
#define ZOMBIEEVENT_HPP

#include <string>
#include "Zombie.hpp"

class ZombieEvent
{
private:
	std::string		_ZombieType;

public:
	ZombieEvent( void );
	~ZombieEvent( void );

	void			setZombieType( std::string new_type );
	Zombie			*newZombie( std::string name );
	void			randomChump( void );
};

#endif
