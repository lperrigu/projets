// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 18:30:22 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/17 19:58:27 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include <fstream>
#include <sstream>

std::string	replace(std::string file, std::string s1, std::string s2)
{
	int	i;

	i = 0;
	while( 1 )
	{
		i = file.find(s1, i);
		if ( (const size_t)i !=  std::string::npos)
		{
			file.replace(i, s1.length(), s2);
			i += s2.length();
		}
		else
			return (file);
	}
}

int		main( int ac, char **av )
{
	std::string			s1;
	std::string			s2;
	std::string			file;
	std::stringstream	buffer;
	std::ifstream		ifs(av[1]);

	if (ac < 4)
	{
		std::cout << "Pas assez d'arguments" << std::endl;
		return 0;
	}

	buffer << ifs.rdbuf();
	file = buffer.str();
	s1 = av[2];
	s2 = av[3];
	file = replace(file, s1, s2);

	s1 = av[1];
	s1 += ".replace";
	std::ofstream ofs(s1);
	ofs << file;
	ofs.close();

	return (0);
}
