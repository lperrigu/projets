// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Zombie.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 11:41:25 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 14:55:27 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ZOMBIE_HPP
#define ZOMBIE_HPP

#include <string>

class Zombie
{
private:
	std::string _type;
	std::string _name;

public:
	Zombie( void );
	~Zombie( void );

	void	announce( void ) const;
	void	setName( std::string name );
};

#endif
