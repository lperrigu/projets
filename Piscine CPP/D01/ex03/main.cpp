// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 14:43:06 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 14:53:17 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ZombieHorde.hpp"

int main( void )
{
	ZombieHorde	horde = ZombieHorde(10);

	horde.announce();
	return 0;
}
