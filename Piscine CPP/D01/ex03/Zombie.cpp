// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Zombie.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 11:42:56 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 14:59:53 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "zombie.hpp"

Zombie::Zombie( void )
{
	this->_type = "default";
	std::cout << "Zombie created" << std::endl;
}

Zombie::~Zombie( void )
{
	std::cout << "Zombie " << this->_name << " really dead now" << std::endl;
}

void	Zombie::setName(std::string name)
{
	this->_name = name;
}

void	Zombie::announce(void) const
{
	std::cout << '<' << this->_name
			  << " (" << this->_type
			  << ")> Braiiiiiiinnnssss..." << std::endl;
}
