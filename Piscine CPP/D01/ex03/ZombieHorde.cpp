// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ZombieHorde.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 14:12:03 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 14:59:15 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ZombieHorde.hpp"
#include <string>
#include <cstdlib>
#include <ctime>

ZombieHorde::ZombieHorde( int N )
{
	Zombie		*Zombies = new Zombie[N];
	int			i;
	int			j;
	std::string	randomName;

	this->_hordeSize = N;
	i = 0;
	while (i < N)
	{
		j = 0;
		while (j < 13)
		{
			randomName += (std::rand() * time(NULL)) % 26 + 97;
			++j;
		}
		Zombies[i].setName(randomName);
		randomName.clear();
		++i;
	}
	this->_Zombies = Zombies;
}

void	ZombieHorde::announce( void ) const
{
	int	i;

	i = 0;
	while (i < this->_hordeSize)
	{
		_Zombies[i].announce();
		++i;
	}
}

ZombieHorde::~ZombieHorde( void )
{
	delete [] this->_Zombies;
}
