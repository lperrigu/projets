// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ZombieHorde.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 14:12:29 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 14:54:17 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ZOMBIEHORDE_HPP
#define ZOMBIEHORDE_HPP

#include "Zombie.hpp"

class ZombieHorde
{
private:
	Zombie	*_Zombies;
	int		_hordeSize;

public:
	ZombieHorde( int N );
	~ZombieHorde( void );

	void	announce( void ) const;
};

#endif
