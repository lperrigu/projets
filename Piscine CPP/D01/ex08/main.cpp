// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 17:41:49 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/17 17:44:02 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Human.hpp"

int main( void )
{
	Human h;

	h.action("meleeAttack", "targetA");
	h.action("rangedAttack", "targetB");
	h.action("intimidatingShout", "targetC");
	h.action("Dontexist", "targetD");
	return 0;
}
