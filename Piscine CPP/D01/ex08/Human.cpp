// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Human.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 16:42:30 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/17 20:00:33 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Human.hpp"

void	Human::meleeAttack(std::string const & target)
{
	std::cout << "meleeAttack called on "
			  << target << std::endl;
}

void	Human::rangedAttack(std::string const & target)
{
	std::cout << "rangedAttack called on "
			  << target << std::endl;
}

void	Human::intimidatingShout(std::string const & target)
{
	std::cout << "inimidatingShout called on "
			  << target << std::endl;
}

void	Human::action(std::string const & action_name, std::string const & target)
{
	void 		(Human::* actions_func[3])(std::string const &);
	std::string	action_names[3];
	int			i;

	action_names[0] = "meleeAttack";
	action_names[1] = "rangedAttack";
	action_names[2] = "intimidatingShout";
	actions_func[0] = &Human::meleeAttack;
	actions_func[1] = &Human::rangedAttack;
	actions_func[2] = &Human::intimidatingShout;
	i = 0;
	while (i < 3)
	{
		if (action_name == action_names[i])
			(this->*actions_func[i])(target);
		++i;
	}
}
