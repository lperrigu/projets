// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Pony.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 10:44:59 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 11:08:04 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Pony.hpp"

Pony::Pony( std::string name )
{
	this->_name = name;
	std::cout << name << " is created" << std::endl;
}

Pony::~Pony( void )
{
	std::cout << this->_name << " is destructed" << std::endl;
}

void	Pony::doSomeStuff( void ) const
{
	std::cout << this->_name << " do some stuff" << std::endl;
}
