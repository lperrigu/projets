// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Pony.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 10:40:50 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 11:06:53 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PONY_HPP
#define PONY_HPP

#include <iostream>

class Pony
{
private:
	std::string _name;

public:
	Pony( std::string name );
	~Pony( void );

	void	doSomeStuff( void ) const;
};

#endif
