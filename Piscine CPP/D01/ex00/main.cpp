// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 10:56:15 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/16 11:07:17 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Pony.hpp"

void	ponyOnTheHeap( void )
{
	Pony	*p = new Pony("Fluttershy");

	p->doSomeStuff();
	delete p;
}

void	ponyOnTheStack( void )
{
	Pony	p = Pony("Rainbow Dash");

	p.doSomeStuff();
}

int		main( void )
{
	ponyOnTheHeap();
	ponyOnTheStack();
	return 0;
}
