// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Span.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/25 19:18:12 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/26 15:49:49 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SPAN_HPP
#define SPAN_HPP

#include <vector>
#include <cstdlib>
#include <climits>
#include <algorithm>

class Span
{
private:
	std::vector<int>	_data;
	unsigned int		_size_max;

	Span( void );
	Span( Span const & );
	Span& operator=( Span const & );

public:
	Span( unsigned int N );
	virtual ~Span( void );

	void	addNumber( int v );
	void	addInterval( int v, unsigned int size );
	void	addIntervalRandom( unsigned int size );
	int		shortestSpan( void );
	int		longestSpan( void );
};

#endif
