// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Span.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/25 19:22:07 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/26 15:50:45 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Span.hpp"

Span::Span( unsigned int N )
{
	this->_size_max = N;
}

Span::~Span( void )
{
	
}

void	Span::addNumber( int v )
{
	if ( this->_data.size() + 1 <= this->_size_max )
		this->_data.push_back( v );
	else
		throw std::exception();
}

void	Span::addInterval( int v, unsigned int size )
{
	unsigned int	i;

	if ( size <= 0)
		return ;
	if ( this->_data.size() + size <= this->_size_max )
	{
		for ( i = 0; i < size; ++i )
			this->_data.push_back( v + i );
	}
	else
		throw std::exception();
}

void	Span::addIntervalRandom( unsigned int size )
{
	unsigned int	i;

	if ( size <= 0)
		return ;
	if ( this->_data.size() + size <= this->_size_max )
	{
		for ( i = 0; i < size; ++i )
			this->_data.push_back( static_cast<int>( rand() ) );
	}
	else
		throw std::exception();
}

int		Span::shortestSpan( void )
{
	unsigned int	i;
	unsigned int	min;
	unsigned int	tmp;
	unsigned int	size;

	size = this->_data.size();
	if ( size <= 1)
		throw std::exception();
	std::sort( this->_data.begin(), this->_data.end() );
	min = UINT_MAX;
	for ( i = 0; i < size - 1; ++i )
	{
		tmp = this->_data[i + 1] - this->_data[i];
		if ( tmp < min )
			min = tmp;
	}
	return (min);
}

int		Span::longestSpan( void )
{
	unsigned int		size;

	std::sort( this->_data.begin(), this->_data.end() );
	size = this->_data.size();
	if ( size <= 1 )
		throw std::exception();
	return ( this->_data[size - 1] - this->_data[0] );
}
