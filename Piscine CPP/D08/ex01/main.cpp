// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/26 13:21:05 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/26 15:51:06 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "Span.hpp"

int		main( void )
{
	Span	S1( 5 );
	Span	S2( 100000 );
	Span	S3( 100000 );
//	int		i;

	srand( time ( NULL ) );
	try
	{
		std::cout << S1.shortestSpan() << std::endl;
	}
	catch (std::exception e)
	{
		std::cout << "ERROR" << std::endl;
	}
	std::cout << std::endl;
	S1.addNumber( 10 );
	S1.addNumber( 23 );
	S1.addNumber( 15 );
	S1.addNumber( 10 );
	S1.addNumber( 2 );

	std::cout << S1.shortestSpan() << std::endl;
	std::cout << S1.longestSpan() << std::endl;
	std::cout << std::endl;

	S2.addInterval( 20, 100000 );
	std::cout << S2.shortestSpan() << std::endl;
	std::cout << S2.longestSpan() << std::endl;
	std::cout << std::endl;

	S3.addIntervalRandom( 100000 );
	std::cout << S3.shortestSpan() << std::endl;
	std::cout << S3.longestSpan() << std::endl;
	return 0;
}
