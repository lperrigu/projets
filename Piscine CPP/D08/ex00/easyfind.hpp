// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   easyfind.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/25 11:14:26 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/25 19:09:05 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <algorithm>
#include <iostream>
#include <list>

template< typename T>
int		easyfind( T container, int to_find )
{
	typename	T::iterator	it;

	it = find( container.begin(), container.end(), to_find );
	if ( it != container.end() )
	{
		std::cout << *it << std::endl;
		return (*it);
	}
	else
	{
		std::cout << "Not found" << std::endl;
		throw std::exception();
	}
}
