// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/25 18:14:56 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/25 19:08:53 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "easyfind.hpp"

int main( void )
{
	std::list<int>	lst;
	int r;

	lst.push_back(10);
	lst.push_back(178);
	lst.push_back(1);
	lst.push_back(44);
	lst.push_back(-12);
	lst.push_back(0);

	r = easyfind(lst, 44);
	std::cout << r << std::endl;
	try
	{
		r = easyfind(lst, 42);
	}
	catch (std::exception e)
	{
		std::cout << "ERROR" << std::endl;
	}
	return 0;
}
