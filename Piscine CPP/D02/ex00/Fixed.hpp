// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Fixed.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 11:18:34 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/18 18:12:13 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FIXED_HPP
#define FIXED_HPP

#include <iostream>

class	Fixed
{
private:
	int					_value;
	static const int	_point;

public:
	Fixed( void );
	~Fixed( void );
	Fixed( Fixed const &src );

	Fixed	&operator=( Fixed const &src );

	int		getRawBits( void ) const;
	void	setRawBits( int const raw );
};

#endif
