// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Fixed.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 11:34:25 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/18 18:06:27 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Fixed.hpp"

Fixed::Fixed( void ): _value( 0 )
{
}

Fixed::Fixed( const int value ): _value( value << Fixed::_point)
{
}

Fixed::Fixed( const float value )
{
	float	tmp;

	this->setRawBits((int)value);
	tmp = value - (float)this->getRawBits();
	this->setRawBits(this->getRawBits() << Fixed::_point);
	tmp = tmp / (1.0 / 256.0);
	this->setRawBits(this->getRawBits() + (int)roundf(tmp));
}

Fixed::~Fixed( void )
{
}

Fixed::Fixed( Fixed const &src )
{
	*this = src;
}

Fixed	&Fixed::operator=( Fixed const &src )
{
	this->_value = src.getRawBits();
	return *this;
}

Fixed	Fixed::operator+( Fixed const &src )
{
	float	tmp;

	tmp = this->toFloat() + src.toFloat();
	return Fixed( tmp );
}

Fixed	Fixed::operator-( Fixed const &src )
{
	float	tmp;

	tmp = this->toFloat() - src.toFloat();
	return Fixed( tmp );
}

Fixed	Fixed::operator*( Fixed const &src )
{
	float	tmp;

	tmp = this->toFloat() * src.toFloat();
	return Fixed( tmp );
}

Fixed	Fixed::operator/( Fixed const &src )
{
	float	tmp;

	tmp = this->toFloat() / src.toFloat();
	return Fixed( tmp );
}

bool	Fixed::operator>( Fixed const &src ) const
{
	return ( this->_value > src.getRawBits() );
}

bool	Fixed::operator<( Fixed const &src ) const
{
	return ( this->_value < src.getRawBits() );
}

bool	Fixed::operator>=( Fixed const &src ) const
{
	return ( this->_value >= src.getRawBits() );
}

bool	Fixed::operator<=( Fixed const &src ) const
{
	return ( this->_value <= src.getRawBits() );
}
bool	Fixed::operator==( Fixed const &src ) const
{
	return ( this->_value == src.getRawBits() );
}

bool	Fixed::operator!=( Fixed const &src ) const
{
	return ( this->_value != src.getRawBits() );
}

Fixed	&Fixed::operator++( void )
{
	++this->_value;
	return ( *this );
}

Fixed	Fixed::operator++( int )
{
	Fixed	tmp( 0 );

	tmp.setRawBits( this->getRawBits() );
	++(*this);
	return (tmp);
}

Fixed	&Fixed::operator--( void )
{
	--this->_value;
	return ( *this );
}

Fixed	Fixed::operator--( int )
{
	Fixed	tmp( 0 );

	tmp.setRawBits( this->getRawBits() );
	--(*this);
	return (tmp);
}

Fixed		&Fixed::min( Fixed &A, Fixed &B )
{
	if (A > B)
		return ( B );
	else
		return ( A );
}

const Fixed	&Fixed::min( Fixed const &A, Fixed const &B )
{
	if (A > B)
		return ( B );
	else
		return ( A );
}

Fixed		&Fixed::max( Fixed &A, Fixed &B )
{
	if (A > B)
		return ( A );
	else
		return ( B );
}

const Fixed	&Fixed::max( Fixed const &A, Fixed const &B )
{
	if (A > B)
		return ( A );
	else
		return ( B );
}

int		Fixed::getRawBits( void ) const
{
	return this->_value;
}

void    Fixed::setRawBits( int const raw )
{
	this->_value = raw;
}

float	Fixed::toFloat( void )const
{
	float	ret;
	int		frac;

	ret = (float)this->toInt();
	frac = (255 & this->getRawBits());
	return (ret + (float)frac * (1.0 / 256.0));
}

int		Fixed::toInt( void )const
{
	return (this->getRawBits() >> Fixed::_point);
}

std::ostream    &operator<<( std::ostream &o, Fixed const &src )
{
	o << src.toFloat();
	return o;
}

const int		Fixed::_point = 8;
