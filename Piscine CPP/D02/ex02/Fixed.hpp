// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Fixed.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 11:18:34 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/18 18:12:39 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FIXED_HPP
#define FIXED_HPP

#include <iostream>
#include <cmath>

class	Fixed
{
private:
	int					_value;
	static const int	_point;

public:
	Fixed( void );
	Fixed( const int value );
	Fixed( const float value );
	~Fixed( void );
	Fixed( Fixed const &src );

	Fixed	&operator=( Fixed const &src );
	Fixed	operator+( Fixed const &src );
	Fixed	operator-( Fixed const &src );
	Fixed	operator*( Fixed const &src );
	Fixed	operator/( Fixed const &src );
	bool	operator>( Fixed const &src ) const;
	bool	operator<( Fixed const &src ) const;
	bool	operator>=( Fixed const &src ) const;
	bool	operator<=( Fixed const &src ) const;
	bool	operator==( Fixed const &src ) const;
	bool	operator!=( Fixed const &src ) const;
	Fixed	&operator++( void );
	Fixed	operator++( int );
	Fixed	&operator--( void );
	Fixed	operator--( int );

	static Fixed		&min( Fixed &A, Fixed &B );
	static const Fixed	&min( Fixed const &A, Fixed const &B );
	static Fixed		&max( Fixed &A, Fixed &B );
	static const Fixed	&max( Fixed const &A, Fixed const &B );
	float				toFloat( void ) const;
	int					toInt( void ) const;
	int					getRawBits( void ) const;
	void				setRawBits( int const raw );
};

std::ostream	&operator<<( std::ostream &o, Fixed const &src );

#endif
