// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Fixed.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 11:34:25 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/18 17:47:51 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Fixed.hpp"

Fixed::Fixed( void ): _value( 0 )
{
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed( const int value ): _value( value << Fixed::_point)
{
	std::cout << "Int constructor called" << std::endl;
}

Fixed::Fixed( const float value )
{
	float	tmp;

	std::cout << "Float constructor called" << std::endl;
	this->setRawBits((int)value);
	tmp = value - (float)this->getRawBits();
	this->setRawBits(this->getRawBits() << Fixed::_point);
	tmp = tmp / (1.0 / 256.0);
	this->setRawBits(this->getRawBits() + (int)roundf(tmp));
}

Fixed::~Fixed( void )
{
	std::cout << "Destructor called" << std::endl;
}

Fixed::Fixed( Fixed const &src )
{
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
}

Fixed   &Fixed::operator=( Fixed const &src )
{
	std::cout << "Assignation operator called" << std::endl;
	this->_value = src.getRawBits();
	return *this;
}

int		Fixed::getRawBits( void ) const
{
	return this->_value;
}

void    Fixed::setRawBits( int const raw )
{
	this->_value = raw;
}

float	Fixed::toFloat( void )const
{
	float	ret;
	int		frac;

	ret = (float)this->toInt();
	frac = (511 & this->getRawBits());
	return (ret + (float)frac * (1.0 / 256.0));
}

int		Fixed::toInt( void )const
{
	return (this->getRawBits() >> Fixed::_point);
}

std::ostream    &operator<<( std::ostream &o, Fixed const &src )
{
	o << src.toFloat();
	return o;
}

const int		Fixed::_point = 8;
