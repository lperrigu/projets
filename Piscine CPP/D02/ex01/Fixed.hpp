// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Fixed.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 11:18:34 by lperrigu          #+#    #+#             //
//   Updated: 2015/06/18 18:12:29 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FIXED_HPP
#define FIXED_HPP

#include <iostream>
#include <cmath>

class	Fixed
{
private:
	int					_value;
	static const int	_point;

public:
	Fixed( void );
	Fixed( const int value );
	Fixed( const float value );
	~Fixed( void );
	Fixed( Fixed const &src );

	Fixed	&operator=( Fixed const &src );

	float	toFloat( void ) const;
	int		toInt( void ) const;
	int		getRawBits( void ) const;
	void	setRawBits( int const raw );
};

std::ostream	&operator<<( std::ostream &o, Fixed const &src );

#endif
