// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Vm.cpp                                             :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/10 05:32:38 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 17:10:01 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Vm.hpp"

Vm::Vm()
{

	this->_iName[0] = "pop";
	this->_iName[1] = "dump";
	this->_iName[2] = "add";
	this->_iName[3] = "sub";
	this->_iName[4] = "mul";
	this->_iName[5] = "div";
	this->_iName[6] = "mod";
	this->_iName[7] = "print";
	this->_iName[8] = "exit";
	this->_iName[9] = "push";
	this->_iName[10] = "assert";

	this->_iType[0] = ipop;
	this->_iType[1] = idump;
	this->_iType[2] = iadd;
	this->_iType[3] = isub;
	this->_iType[4] = imul;
	this->_iType[5] = idiv;
	this->_iType[6] = imod;
	this->_iType[7] = iprint;
	this->_iType[8] = iexit;
	this->_iType[9] = ipush;
	this->_iType[10] = iassert;

	this->_iFunc[0] = &popFunc;
	this->_iFunc[1] = &dumpFunc;
	this->_iFunc[2] = &addFunc;
	this->_iFunc[3] = &subFunc;
	this->_iFunc[4] = &mulFunc;
	this->_iFunc[5] = &divFunc;
	this->_iFunc[6] = &modFunc;
	this->_iFunc[7] = &printFunc;
	this->_iFunc[8] = &exitFunc;
	this->_iFunc[9] = &pushFunc;
	this->_iFunc[10] = &assertFunc;
}

Vm::Vm(Vm const &instance)
{
	*this = instance;
}

Vm& Vm::operator=(Vm const &rhs)
{
	(void)rhs;
	return (*this);
}

Vm::~Vm()
{
}

void Vm::addInstruction(std::string instruction)
{
	int			i = 0;
	std::string	instr;
	std::string	value;
	size_t		space_pos;

	if (instruction.find_first_of(';') != std::string::npos)
		instruction.erase(instruction.find_first_of(';'));
	if (instruction[0] != '\0')
	{
		while (i < 9)
		{
			if (this->_iName[i] == instruction)
			{
				this->_instructions.push_back(
					new Instruction(this->_iType[i]) );
				return ;
			}
			++i;
		}
		space_pos = instruction.find(' ');
		instr = instruction.substr(0, space_pos);
		value = instruction.substr(space_pos + 1);
		while (i < 11)
		{
			if (this->_iName[i] == instr)
			{
				this->_instructions.push_back(
					new Instruction(this->_iType[i], value) );
				return ;
			}
			++i;
		}
		throw UnknownInstructionException();
	}
}

void Vm::addFile(char *path)
{
	std::ifstream       ifs(path);
	std::stringstream   buffer;
	std::string         file;
	std::string         line;
	size_t              current_pos;
	size_t              endline;

	buffer << ifs.rdbuf();
	file = buffer.str();
	current_pos = 0;
	endline = 0;
	while (endline != std::string::npos)
	{
		endline = file.find('\n', current_pos);
		if (endline != std::string::npos)
			line = file.substr(current_pos, endline - current_pos);
		else
			line = file.substr(current_pos);
		this->addInstruction(line);
		current_pos = endline + 1;
	}
}

void Vm::run()
{
	int		i;
	std::vector<Instruction const *>::iterator it;

	it = this->_instructions.begin();
	while (it != this->_instructions.end())
	{
		i = 0;
		while (i < 11)
		{
			if (this->_iType[i] == (*it)->getItype())
				this->_iFunc[i](*this, *it);
			++i;
		}
		++it;
	}
	throw NoExitException();
}

size_t Vm::stackSize() const
{
	return this->_stack.size();
}

void Vm::stackPopBack()
{
	this->_stack.pop_back();
}

void Vm::stackPrintElem(int i) const
{
	std::cout << this->_stack[i]->toString() << std::endl;
}

void Vm::stackPushBack(eOperandType t, std::string value)
{
	this->_stack.push_back( this->_fact.createOperand( t, value ) );
}

IOperand const	*Vm::newOperand(eOperandType t, std::string value) const
{
	return this->_fact.createOperand( t, value );
}

eOperandType	Vm::stackGetType(int i) const
{
	return this->_stack[i]->getType();
}

std::string		Vm::stackGetString(int i) const
{
	return this->_stack[i]->toString();
}

int				Vm::stackGetPrecision(int i) const
{
	return this->_stack[i]->getPrecision();
}

void			Vm::stackEraseElem(int i)
{
	this->_stack.erase(_stack.begin() + i);
}

IOperand const	*Vm::stackElem(int i) const
{
	return this->_stack[i];
}

IOperand const	&Vm::stackElemRef(int i) const
{
	return *(this->_stack[i]);
}

void popFunc(Vm &vm, Instruction const *instru)
{
	if (instru->getItype() != ipop)
		throw UnknownException();
	else if (vm.stackSize() == 0)
		throw PopException();
	else
		vm.stackPopBack();
}

void dumpFunc(Vm &vm, Instruction const *instru)
{
	int size = vm.stackSize() - 1;

	if (instru->getItype() != idump)
		throw UnknownException();
	while (size >= 0)
	{
		vm.stackPrintElem(size);
		--size;
	}
}

void addFunc(Vm &vm, Instruction const *instru)
{
	if (instru->getItype() != iadd)
		throw UnknownException();
	else if (vm.stackSize() < 2)
		throw NoValuesException();
	int				i1 = vm.stackSize() - 1;
	int				i2 = vm.stackSize() - 2;
	eOperandType	t;
	int				prec =
	std::max(vm.stackGetPrecision(i1), vm.stackGetPrecision(i2));
	long			valuei1 = std::stol(vm.stackGetString(i1), NULL, 10);
	long			valuei2 = std::stol(vm.stackGetString(i2), NULL, 10);
	double			valued1 = std::stod(vm.stackGetString(i1));
	double			valued2 = std::stod(vm.stackGetString(i2));

	if (vm.stackGetPrecision(i1) >= vm.stackGetPrecision(i2))
		t = vm.stackGetType(i1);
	else
		t = vm.stackGetType(i2);
	if (prec < 3)
		vm.stackPushBack(t, std::to_string(valuei1 + valuei2));
	else
		vm.stackPushBack(t, std::to_string(valued1 + valued2));
	vm.stackEraseElem(vm.stackSize() - 2);
	vm.stackEraseElem(vm.stackSize() - 2);
}

void subFunc(Vm &vm, Instruction const *instru)
{
	if (instru->getItype() != isub)
		throw UnknownException();
	else if (vm.stackSize() < 2)
		throw NoValuesException();
	int				i1 = vm.stackSize() - 1;
	int				i2 = vm.stackSize() - 2;
	eOperandType	t;
	int				prec =
	std::max(vm.stackGetPrecision(i1), vm.stackGetPrecision(i2));
	long			valuei1 = std::stol(vm.stackGetString(i1), NULL, 10);
	long			valuei2 = std::stol(vm.stackGetString(i2), NULL, 10);
	double			valued1 = std::stod(vm.stackGetString(i1));
	double			valued2 = std::stod(vm.stackGetString(i2));

	if (vm.stackGetPrecision(i1) >= vm.stackGetPrecision(i2))
		t = vm.stackGetType(i1);
	else
		t = vm.stackGetType(i2);
	if (prec < 3)
		vm.stackPushBack(t, std::to_string(valuei1 - valuei2));
	else
		vm.stackPushBack(t, std::to_string(valued1 - valued2));
	vm.stackEraseElem(vm.stackSize() - 2);
	vm.stackEraseElem(vm.stackSize() - 2);
}

void mulFunc(Vm &vm, Instruction const *instru)
{
	if (instru->getItype() != imul)
		throw UnknownException();
	else if (vm.stackSize() < 2)
		throw NoValuesException();
	int				i1 = vm.stackSize() - 1;
	int				i2 = vm.stackSize() - 2;
	eOperandType	t;
	int				prec =
	std::max(vm.stackGetPrecision(i1), vm.stackGetPrecision(i2));
	long			valuei1 = std::stol(vm.stackGetString(i1), NULL, 10);
	long			valuei2 = std::stol(vm.stackGetString(i2), NULL, 10);
	double			valued1 = std::stod(vm.stackGetString(i1));
	double			valued2 = std::stod(vm.stackGetString(i2));

	if (vm.stackGetPrecision(i1) >= vm.stackGetPrecision(i2))
		t = vm.stackGetType(i1);
	else
		t = vm.stackGetType(i2);
	if (prec < 3)
		vm.stackPushBack(t, std::to_string(valuei1 * valuei2));
	else
		vm.stackPushBack(t, std::to_string(valued1 * valued2));
	vm.stackEraseElem(vm.stackSize() - 2);
	vm.stackEraseElem(vm.stackSize() - 2);
}

void divFunc(Vm &vm, Instruction const *instru)
{
	if (instru->getItype() != idiv)
		throw UnknownException();
	else if (vm.stackSize() < 2)
		throw NoValuesException();
	int				i1 = vm.stackSize() - 1;
	int				i2 = vm.stackSize() - 2;
	eOperandType	t;
	int				prec =
	std::max(vm.stackGetPrecision(i1), vm.stackGetPrecision(i2));
	long			valuei1 = std::stol(vm.stackGetString(i1), NULL, 10);
	long			valuei2 = std::stol(vm.stackGetString(i2), NULL, 10);
	double			valued1 = std::stod(vm.stackGetString(i1));
	double			valued2 = std::stod(vm.stackGetString(i2));

	if (valuei1 == 0)
		throw DivisionException();
	if (vm.stackGetPrecision(i1) >= vm.stackGetPrecision(i2))
		t = vm.stackGetType(i1);
	else
		t = vm.stackGetType(i2);
	if (prec < 3)
		vm.stackPushBack(t, std::to_string(valuei2 / valuei1));
	else
		vm.stackPushBack(t, std::to_string(valued2 / valued1));
	vm.stackEraseElem(vm.stackSize() - 2);
	vm.stackEraseElem(vm.stackSize() - 2);
}

void modFunc(Vm &vm, Instruction const *instru)
{
	if (instru->getItype() != imod)
		throw UnknownException();
	else if (vm.stackSize() < 2)
		throw NoValuesException();
	int				i1 = vm.stackSize() - 1;
	int				i2 = vm.stackSize() - 2;
	eOperandType	t;
	int				prec =
	std::max(vm.stackGetPrecision(i1), vm.stackGetPrecision(i2));
	long			valuei1 = std::stol(vm.stackGetString(i1), NULL, 10);
	long			valuei2 = std::stol(vm.stackGetString(i2), NULL, 10);
	double			valued1 = std::stod(vm.stackGetString(i1));
	double			valued2 = std::stod(vm.stackGetString(i2));

	if (valuei1 == 0)
		throw DivisionException();
	if (vm.stackGetPrecision(i1) >= vm.stackGetPrecision(i2))
		t = vm.stackGetType(i1);
	else
		t = vm.stackGetType(i2);
	if (prec < 3)
		vm.stackPushBack(t, std::to_string(valuei2 % valuei1));
	else
		vm.stackPushBack(t, std::to_string(fmod(valued2, valued1)));
	vm.stackEraseElem(vm.stackSize() - 2);
	vm.stackEraseElem(vm.stackSize() - 2);
}

void printFunc(Vm &vm, Instruction const *instru)
{
	int	i = vm.stackSize() - 1;

	(void)instru;
	if (instru->getItype() != iprint)
		throw UnknownException();
	else if (vm.stackSize() < 1)
		throw AssertException();
	else if (vm.stackGetType(i) != Int8)
		throw AssertException();
	char c = std::stoi(vm.stackGetString(i), NULL, 10);
	std::cout << c;
}

void exitFunc(Vm &vm, Instruction const *instru)
{
	(void)vm;
	if (instru->getItype() != iexit)
		throw UnknownException();
	exit(0);
}

void pushFunc(Vm &vm, Instruction const *instru)
{
	if (instru->getItype() != ipush)
		throw UnknownException();
	vm.stackPushBack(instru->getOtype(), instru->getValue());
}

void assertFunc(Vm &vm, Instruction const *instru)
{
	IOperand const *param;
	int	i = vm.stackSize() - 1;

	if (instru->getItype() != iassert)
		throw UnknownException();
	else if (vm.stackSize() < 1)
		throw AssertException();
	param = vm.newOperand(instru->getOtype(), instru->getValue());
	if (param->getType() != vm.stackGetType(i))
		throw AssertException();
	else if (param->toString() != vm.stackGetString(i))
		throw AssertException();
	delete  param;
}
