// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Vm.hpp                                             :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/10 05:32:01 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/26 22:49:26 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef VM_HPP
#define VM_HPP

#include "Factory.hpp"
#include "Instruction.hpp"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>

class Vm
{
private:
	std::vector<IOperand const *>		_stack;
	std::vector<Instruction const *>	_instructions;
	Factory								_fact;
	std::string							_iName[11];
	eInstruction						_iType[11];
	void								(* _iFunc[11])
	(Vm &, Instruction const *);

public:
	Vm();
	Vm(Vm const &);
	Vm& operator=(Vm const &);
	virtual ~Vm();

	void			addInstruction(std::string);
	void			addFile(char *path);
	void			run();
	size_t			stackSize() const;
	void			stackPopBack();
	void			stackPrintElem(int) const;
	void			stackPushBack(eOperandType, std::string);
	IOperand const *newOperand(eOperandType, std::string) const;
	eOperandType	stackGetType(int i) const;
	std::string		stackGetString(int i) const;
	int				stackGetPrecision(int i) const;
	void			stackEraseElem(int i);
	IOperand const *stackElem(int i) const;
	IOperand const &stackElemRef(int i) const;
};

void	popFunc(Vm&, Instruction const *);
void	dumpFunc(Vm&, Instruction const *);
void	addFunc(Vm&, Instruction const *);
void	subFunc(Vm&, Instruction const *);
void	mulFunc(Vm&, Instruction const *);
void	divFunc(Vm&, Instruction const *);
void	modFunc(Vm&, Instruction const *);
void	printFunc(Vm&, Instruction const *);
void	exitFunc(Vm&, Instruction const *);
void	pushFunc(Vm&, Instruction const *);
void	assertFunc(Vm&, Instruction const *);


#endif
