// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   FloatC.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/08 19:49:10 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 15:59:29 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FLOATC_HPP
#define FLOATC_HPP

#include "IOperand.hpp"
#include "Exception.hpp"
#include <climits>

class FloatC : public IOperand
{
private:
	float		_data;
	std::string	_string;

public:
	FloatC();
	FloatC( std::string const & value );
	FloatC(FloatC const &);
	FloatC& operator=(FloatC const &);
	virtual ~FloatC();

	void	setData( float newData );
	float	getData( void ) const;

	virtual int					getPrecision( void ) const;
	virtual eOperandType		getType( void ) const;
	virtual IOperand const *	operator+( IOperand const & rhs ) const;
	virtual IOperand const *	operator-( IOperand const & rhs ) const;
	virtual IOperand const *	operator*( IOperand const & rhs ) const;
	virtual IOperand const *	operator/( IOperand const & rhs ) const;
	virtual IOperand const *	operator%( IOperand const & rhs ) const;

	virtual std::string const &	toString( void ) const;	
};

#endif
