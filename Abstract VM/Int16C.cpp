// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Int16C.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/08 17:24:54 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 15:53:34 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Int16C.hpp"

Int16C::Int16C()
{

}

Int16C::Int16C(std::string const & value)
{
	int	i = std::stoi(value, NULL, 10);

	this->setData(i);
}

Int16C::Int16C(Int16C const &instance)
{
	*this = instance;
}

Int16C& Int16C::operator=(Int16C const &rhs)
{
	this->_data = rhs.getData();
	return (*this);
}

int		Int16C::getData(void) const
{
    return this->_data;
}

void	Int16C::setData(int newData)
{
    if (newData > 32767)
        throw OverflowException();
    else if (newData < -32768)
        throw UnderflowException();
    else
	{
        this->_data = newData;
		this->_string = std::to_string(this->_data);
	}
}

Int16C::~Int16C()
{
	
}

int Int16C::getPrecision( void ) const
{
	return 1;
}

eOperandType Int16C::getType( void ) const
{
	return Int16;
}

IOperand const * Int16C::operator+( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int16C::operator-( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int16C::operator*( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int16C::operator/( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int16C::operator%( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

std::string const & Int16C::toString( void ) const
{
	return this->_string;
}
