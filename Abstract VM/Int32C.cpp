// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Int32C.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/08 18:56:58 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 16:17:45 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Int32C.hpp"

Int32C::Int32C()
{

}

Int32C::Int32C(std::string const & value)
{
	long	i = std::stol(value, NULL, 10);

	this->setData(i);
}

Int32C::Int32C(Int32C const &instance)
{
	*this = instance;
}

Int32C& Int32C::operator=(Int32C const &rhs)
{
	this->_data = rhs.getData();
	return (*this);
}

long	Int32C::getData(void) const
{
	return this->_data;
}

void	Int32C::setData(long newData)
{
	if (newData > 2147483647)
		throw OverflowException();
	else if (newData < -2147483648)
		throw UnderflowException();
	else
	{
		this->_data = newData;
		this->_string = std::to_string(this->_data);
	}
}

Int32C::~Int32C()
{
	
}

int Int32C::getPrecision( void ) const
{
	return 2;
}

eOperandType Int32C::getType( void ) const
{
	return Int32;
}

IOperand const * Int32C::operator+( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int32C::operator-( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int32C::operator*( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int32C::operator/( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int32C::operator%( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

std::string const & Int32C::toString( void ) const
{
	return this->_string;
}
