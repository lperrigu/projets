// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Instruction.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/14 16:57:30 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 17:02:25 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Instruction.hpp"

Instruction::Instruction()
{

}

Instruction::Instruction(eInstruction i)
{
	this->_itype = i;
}

void Instruction::check_type(std::string type)
{
	int		i = 0;

	while (i < 6)
	{
		if (i == 5)
			throw LexicalOrSyntacticException();
		if (this->_typeStringTab[i] == type)
		{
			this->_otype = this->_typeTab[i];
			i = 6;
		}
		++i;
	}
}

void Instruction::check_value(std::string s)
{
	int		i;
	int		first_digit;
	int		second_digit;

	if (s[0] != '-')
		i = 0;
	else
		i = 1;
	first_digit = 0;
	second_digit = 0;
	while (s[i] != '\0')
	{
		if (s[i] >= '0' && s[i] <= '9')
		{
			first_digit = 1;
			++i;
		}
		else if (s[i] == '.')
		{
			++i;
			while (s[i] != '\0')
			{
				if (s[i] >= '0' && s[i] <= '9')
				{
					second_digit = 1;
					++i;
				}
				else
					throw LexicalOrSyntacticException();
			}
		}
		else
			throw LexicalOrSyntacticException();
	}
	if (first_digit && !second_digit)
	{
		if (this->_otype == Float || this->_otype == Double)
			throw LexicalOrSyntacticException();
		else
			this->_value = s;
	}
	else if (first_digit && second_digit)
	{
		if (this->_otype == Float || this->_otype == Double)
			this->_value = s;
		else
			throw LexicalOrSyntacticException();
	}
	else
		throw LexicalOrSyntacticException();
}

void	Instruction::initTabs()
{
	this->_typeStringTab[0] = "int8";
	this->_typeStringTab[1] = "int16";
	this->_typeStringTab[2] = "int32";
	this->_typeStringTab[3] = "float";
	this->_typeStringTab[4] = "double";

	this->_typeTab[0] = Int8;
	this->_typeTab[1] = Int16;
	this->_typeTab[2] = Int32;
	this->_typeTab[3] = Float;
	this->_typeTab[4] = Double;
}

Instruction::Instruction(eInstruction i, std::string s)
{
	this->_itype = i;
	int pos1 = s.find('(');
	int pos2 = s.find(')');
	std::string type = s.substr(0, pos1);
	std::string value = s.substr(pos1 + 1, pos2 - pos1 - 1);
	std::string nul = s.substr(pos2 + 1);

	this->initTabs();
	this->check_type(type);
	this->check_value(value);
	if (nul != "")
		throw LexicalOrSyntacticException();
	this->_value = value;
}

Instruction::Instruction(Instruction const &instance)
{
	*this = instance;
}

Instruction& Instruction::operator=(Instruction const &rhs)
{
	this->_itype = rhs.getItype();
	return (*this);
}

Instruction::~Instruction()
{}

eInstruction Instruction::getItype() const
{
	return this->_itype;
}

eOperandType Instruction::getOtype() const
{
	return this->_otype;
}

std::string Instruction::getValue() const
{
	return this->_value;
}
