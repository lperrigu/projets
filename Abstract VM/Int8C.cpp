// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Int8C.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/12/20 07:34:39 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 15:53:13 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Int8C.hpp"

Int8C::Int8C()
{

}

Int8C::Int8C(std::string const & value)
{
	int i = std::stoi(value, NULL, 10);

	this->setData(i);
}

Int8C::Int8C(Int8C const &instance)
{
	*this = instance;
}

Int8C	&Int8C::operator=(Int8C const &rhs)
{
	this->_data = rhs.getData();
	return (*this);
}

int		Int8C::getData(void) const
{
	return this->_data;
}

void	Int8C::setData(int newData)
{
	if (newData > 127)
		throw OverflowException();
	else if (newData < -128)
		throw UnderflowException();
	else
	{
		this->_data = newData;
		this->_string = std::to_string(this->_data);
	}
}

Int8C::~Int8C()
{
	
}

int Int8C::getPrecision( void ) const
{
	return 0;
}

eOperandType Int8C::getType( void ) const
{
	return Int8;
}

IOperand const * Int8C::operator+( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int8C::operator-( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int8C::operator*( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int8C::operator/( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * Int8C::operator%( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

std::string const & Int8C::toString( void ) const
{
	return this->_string;
}
