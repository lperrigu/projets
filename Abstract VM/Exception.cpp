// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Exception.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/12/20 02:29:05 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/19 22:45:36 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Exception.hpp"

const char*		LexicalOrSyntacticException::what() const throw()
{
	return "Error : Lexical or syntactic";
}

const char*		UnknownInstructionException::what() const throw()
{
	return "Error : Unknown instruction";
}

const char*		OverflowException::what() const throw()
{
	return "Error : Overflow";
}

const char*		UnderflowException::what() const throw()
{
	return "Error : Underflow";
}

const char*		PopException::what() const throw()
{
	return "Error : Instruction pop on an empty stack";
}

const char*		DivisionException::what() const throw()
{
	return "Error : Division or modulo by zero";
}

const char*		NoExitException::what() const throw()
{
	return "Error : No exit instruction";
}

const char*		AssertException::what() const throw()
{
	return "Error : Assert instruction is not true";
}

const char*		NoValuesException::what() const throw()
{
	return "Error : Not enough values for an arithmetic instruction";
}

const char*		UnknownException::what() const throw()
{
	return "Error : Something went wrong";
}
