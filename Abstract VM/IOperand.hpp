// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   IOperand.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/29 15:36:45 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/19 18:41:35 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef OPERAND_HPP
#define OPERAND_HPP

//#include "abstractvm.hpp"
#include <string>

enum eOperandType { Int8, Int16, Int32, Float, Double };

class IOperand {

public:

	virtual int getPrecision( void ) const = 0;
	virtual eOperandType getType( void ) const = 0;
	virtual IOperand const * operator+( IOperand const & rhs ) const = 0;
	virtual IOperand const * operator-( IOperand const & rhs ) const = 0;
	virtual IOperand const * operator*( IOperand const & rhs ) const = 0;
	virtual IOperand const * operator/( IOperand const & rhs ) const = 0;
	virtual IOperand const * operator%( IOperand const & rhs ) const = 0;

	virtual std::string const & toString( void ) const = 0;

	virtual ~IOperand( void ) {}

};

#endif
