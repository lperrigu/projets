// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   FloatC.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/08 20:44:39 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 17:20:32 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "FloatC.hpp"

FloatC::FloatC()
{

}

FloatC::FloatC(std::string const & value)
{
	float	i = std::stof(value);

	this->setData(i);
}

FloatC::FloatC(FloatC const &instance)
{
	*this = instance;
}

FloatC& FloatC::operator=(FloatC const &rhs)
{
	this->_data = rhs.getData();
	return (*this);
}

float		FloatC::getData(void) const
{
    return this->_data;
}

void	FloatC::setData(float newData)
{
	if (newData >= std::numeric_limits<float>::max())
		throw OverflowException();
	else if (newData <= std::numeric_limits<float>::lowest())
		throw UnderflowException();
	else
	{
		this->_data = newData;
		this->_string = std::to_string(this->_data);
	}
}

FloatC::~FloatC()
{
	
}

int FloatC::getPrecision( void ) const
{
	return 3;
}

eOperandType FloatC::getType( void ) const
{
	return Float;
}

IOperand const * FloatC::operator+( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * FloatC::operator-( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * FloatC::operator*( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * FloatC::operator/( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * FloatC::operator%( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

std::string const & FloatC::toString( void ) const
{
	return this->_string;
}
