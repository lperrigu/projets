// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   DoubleC.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/10 00:23:18 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 16:02:07 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef DOUBLEC_HPP
#define DOUBLEC_HPP

#include "IOperand.hpp"
#include "Exception.hpp"
#include "Int16C.hpp"
#include <climits>

class DoubleC : public IOperand
{
private:
	double		_data;
	std::string	_string;

public:
	DoubleC();
	DoubleC( std::string const & value );
	DoubleC(DoubleC const &);
	DoubleC& operator=(DoubleC const &);
	virtual ~DoubleC();

	void	setData( double newData );
	double	getData( void ) const;

	virtual int					getPrecision( void ) const;
	virtual eOperandType		getType( void ) const;
	virtual IOperand const *	operator+( IOperand const & rhs ) const;
	virtual IOperand const *	operator-( IOperand const & rhs ) const;
	virtual IOperand const *	operator*( IOperand const & rhs ) const;
	virtual IOperand const *	operator/( IOperand const & rhs ) const;
	virtual IOperand const *	operator%( IOperand const & rhs ) const;

	virtual std::string const &	toString( void ) const;	
};

#endif
