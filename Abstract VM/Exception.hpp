// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Exception.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/12/20 02:01:17 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/19 22:43:47 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

#include <exception>

class	LexicalOrSyntacticException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

class	UnknownInstructionException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

class	OverflowException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

class	UnderflowException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

class	PopException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

class	DivisionException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

class	NoExitException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

class	AssertException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

class	NoValuesException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

class	UnknownException : public std::exception
{
public:
	virtual const char	*what() const throw();
};

#endif
