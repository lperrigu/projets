// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   DoubleC.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/09 23:40:50 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 17:21:13 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "DoubleC.hpp"

DoubleC::DoubleC()
{

}

DoubleC::DoubleC(std::string const & value)
{
	double	i = std::stod(value);

	this->setData(i);
}

DoubleC::DoubleC(DoubleC const &instance)
{
	*this = instance;
}

DoubleC& DoubleC::operator=(DoubleC const &rhs)
{
	this->_data = rhs.getData();
	return (*this);
}

double	DoubleC::getData(void) const
{
    return this->_data;
}

void	DoubleC::setData(double newData)
{
	if (newData >= std::numeric_limits<double>::max())
		throw OverflowException();
	else if (newData <= std::numeric_limits<double>::lowest())
		throw UnderflowException();
	else
	{
		this->_data = newData;
		this->_string = std::to_string(this->_data);
	}
}

DoubleC::~DoubleC()
{
	
}

int	DoubleC::getPrecision( void ) const
{
	return 4;
}

eOperandType DoubleC::getType( void ) const
{
	return Double;
}

IOperand const * DoubleC::operator+( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * DoubleC::operator-( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * DoubleC::operator*( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * DoubleC::operator/( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

IOperand const * DoubleC::operator%( IOperand const & rhs ) const
{
	static_cast<void>(rhs);
	return this;
}

std::string const & DoubleC::toString( void ) const
{
	return this->_string;
}
