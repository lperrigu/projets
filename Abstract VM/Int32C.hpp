// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Int32C.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/08 18:52:22 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 15:54:07 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef INT32C_HPP
#define INT32C_HPP

#include "IOperand.hpp"
#include "Exception.hpp"

class Int32C : public IOperand
{
private:
	long		_data;
	std::string	_string;

public:
	Int32C();
	Int32C( std::string const & value );
	Int32C(Int32C const &);
	Int32C& operator=(Int32C const &);
	virtual ~Int32C();

	void	setData( long newData );
	long	getData( void ) const;

	virtual int					getPrecision( void ) const;
	virtual eOperandType		getType( void ) const;
	virtual IOperand const *	operator+( IOperand const & rhs ) const;
	virtual IOperand const *	operator-( IOperand const & rhs ) const;
	virtual IOperand const *	operator*( IOperand const & rhs ) const;
	virtual IOperand const *	operator/( IOperand const & rhs ) const;
	virtual IOperand const *	operator%( IOperand const & rhs ) const;

	virtual std::string const &	toString( void ) const;	
};

#endif
