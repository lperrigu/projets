// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Factory.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/12/20 09:46:41 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 17:29:48 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //


#ifndef FACTORY_HPP
#define FACTORY_HPP

#include "Int8C.hpp"
#include "Int16C.hpp"
#include "Int32C.hpp"
#include "FloatC.hpp"
#include "DoubleC.hpp"

class Factory
{

private:
	IOperand const * createInt8( std::string const & value ) const;
	IOperand const * createInt16( std::string const & value ) const;
	IOperand const * createInt32( std::string const & value ) const;
	IOperand const * createFloat( std::string const & value ) const;
	IOperand const * createDouble( std::string const & value ) const;

public:
	IOperand const * createOperand( eOperandType type, std::string const & value ) const;

};

#endif
