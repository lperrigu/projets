// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Instruction.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/14 16:57:42 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 16:46:58 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef INSTRUCTION_HPP
#define INSTRUCTION_HPP

#include "IOperand.hpp"
#include "Exception.hpp"

enum eInstruction { ipush, ipop, idump, iassert, iadd, isub, imul, idiv,
imod, iprint, iexit };

class Instruction
{
private:
	eInstruction	_itype;
	eOperandType	_otype;
	std::string		_value;
	std::string		_typeStringTab[5];
	eOperandType	_typeTab[5];

	void			initTabs();
	void			check_type(std::string);
	void			check_value(std::string);

public:
	Instruction();
	Instruction(eInstruction);
	Instruction(eInstruction, std::string);
	Instruction(Instruction const &);
	Instruction& operator=(Instruction const &);
	virtual ~Instruction();

	eInstruction getItype() const;
	eOperandType getOtype() const;
	std::string getValue() const;
};

#endif
