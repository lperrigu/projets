// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <marvin@42.fr>                    +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/07/21 16:11:57 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 14:58:47 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Vm.hpp"

static void usage( char *ex )
{
	std::cout << "Usage: " << ex << " [input_file]" << std::endl;
}

static int stop(std::string s)
{
	int	i;

	i = 0;
	while (s[i + 1] != '\0')
	{
		if (s[i] == ';' && s[i + 1] == ';')
			return (1);
		++i;
	}
	return (0);
}

void	addInstructionsAndRunVm(Vm &vm, std::vector<std::string> instructions)
{
	std::vector<std::string>::iterator	it;

	it = instructions.begin();
	while (it != instructions.end())
	{
		vm.addInstruction(*it);
		++it;
	}
	vm.run();
}

void handle_eptr(std::exception_ptr eptr)
{
	try
	{
		if (eptr)
		{
			std::rethrow_exception(eptr);
		}
	}
	catch(const std::exception& e)
	{
		std::cout << e.what() << std::endl;
		exit(0);
	}
}

int		main( int ac, char **av )
{
	std::string					input;
	std::vector<std::string>	inputs;
	int							ret;
	Vm							vm;
	std::exception_ptr			eptr;

	if ( ac == 1 )
		while (1)
		{
			ret = std::getline(std::cin, input);
			if (ret == 0)
				exit(0);
			else
				inputs.push_back(input);
			if ( stop( input ) )
			{
				try
				{
					addInstructionsAndRunVm(vm, inputs);
				}
				catch (...)
				{
					eptr = std::current_exception();
				}
				handle_eptr(eptr);
			}
		}
	else if ( ac == 2 )
	{
		try
		{
			vm.addFile(av[1]);
			vm.run();
		}
		catch(...)
		{
			eptr = std::current_exception();
		}
		handle_eptr(eptr);
	}
	else
		usage(av[0]);
	return ( 0 );
}
