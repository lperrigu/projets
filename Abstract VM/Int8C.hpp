// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Int8C.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/12/20 07:33:39 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 15:26:24 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef INT8C_HPP
#define INT8C_HPP

#include "IOperand.hpp"
#include "Exception.hpp"

class Int8C : public IOperand
{
private:
	int			_data;
	std::string	_string;

public:
	Int8C();
	Int8C( std::string const & value );
	Int8C(Int8C const &);
	Int8C& operator=(Int8C const &);
	virtual ~Int8C();

	void	setData( int newData );
	int		getData( void ) const;

	virtual int					getPrecision( void ) const;
	virtual eOperandType		getType( void ) const;
	virtual IOperand const *	operator+( IOperand const & rhs ) const;
	virtual IOperand const *	operator-( IOperand const & rhs ) const;
	virtual IOperand const *	operator*( IOperand const & rhs ) const;
	virtual IOperand const *	operator/( IOperand const & rhs ) const;
	virtual IOperand const *	operator%( IOperand const & rhs ) const;

	virtual std::string const &	toString( void ) const;

};

#endif
