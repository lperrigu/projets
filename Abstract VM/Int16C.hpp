// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Int16C.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/08 17:18:33 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/27 15:52:46 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef INT16C_HPP
#define INT16C_HPP

#include "IOperand.hpp"
#include "Exception.hpp"

class Int16C : public IOperand
{
private:
	int			_data;
	std::string	_string;

public:
	Int16C();
	Int16C( std::string const & value );
	Int16C(Int16C const &);
	Int16C& operator=(Int16C const &);
	virtual ~Int16C();

	void	setData( int newData );
	int		getData( void ) const;

	virtual int					getPrecision( void ) const;
	virtual eOperandType		getType( void ) const;
	virtual IOperand const *	operator+( IOperand const & rhs ) const;
	virtual IOperand const *	operator-( IOperand const & rhs ) const;
	virtual IOperand const *	operator*( IOperand const & rhs ) const;
	virtual IOperand const *	operator/( IOperand const & rhs ) const;
	virtual IOperand const *	operator%( IOperand const & rhs ) const;

	virtual std::string const &	toString( void ) const;	
};

#endif
