// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Factory.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: lperrigu <lperrigu@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/12/20 09:47:14 by lperrigu          #+#    #+#             //
//   Updated: 2016/01/26 18:38:39 by lperrigu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Factory.hpp"

IOperand const * Factory::createInt8( std::string const & value ) const
{
	return new Int8C(value);
}

IOperand const * Factory::createInt16( std::string const & value ) const
{
	return new Int16C(value);
}

IOperand const * Factory::createInt32( std::string const & value ) const
{
	return new Int32C(value);
}

IOperand const * Factory::createFloat( std::string const & value ) const
{
	return new FloatC(value);
}

IOperand const * Factory::createDouble( std::string const & value ) const
{
	return new DoubleC(value);
}

IOperand const * Factory::createOperand( eOperandType type, std::string const & value ) const
{
	if (type == Int8)
		return this->createInt8( value );
	else if (type == Int16)
		return this->createInt16( value );
	else if (type == Int32)
		return this->createInt32( value );
	else if (type == Float)
		return this->createFloat( value );
	else if (type == Double)
		return this->createDouble( value );
	else
		return NULL;
}
