let help () = 
	Printf.printf "usage: ft_turing [-h] jsonfile input\n\n";
	Printf.printf "positional arguments:\n";
	Printf.printf "\tjsonfile\tjson descriptiion of the machine\n\n";
	Printf.printf "\tinput\t\tinput of the machine\n\n";
	Printf.printf "optional arguments:\n";
	Printf.printf "\t-h, --help\tshow this help message and exit\n";
	exit(0)
