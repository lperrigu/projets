#!/bin/sh

NAME=ft_turing

docker rm $NAME

docker build -t $NAME .

mkdir /Users/Shared/Documents
cp -R ../ft_turing /Users/Shared/Documents

cd /Users/Shared/Documents/ft_turing
# subl /Users/Shared/Documents/ft_turing
# interactive

docker run -it -h ft_turing -v /Users/Shared/Documents:/home/Documents --name $NAME $NAME

cd -
rm -rf /Users/Shared/Documents/ft_turing
