(* Global Variables *)
let argc = ref 0
let argv = ref []
let fcolor = ref 0
let n_states = ref 0

type action = LEFT | RIGHT

let get_name		( name, _, _, _, _ )		= name
let get_read		( _, read, _, _, _ )		= read
let get_to_state	( _, _, to_state, _, _ )	= to_state
let get_write		( _, _, _, write, _ )		= write
let get_action		( _, _, _, _, action )		= action
let get_transition_name ( transition_name, _ ) = transition_name

let string_of_action action = match action with
	| LEFT	-> "LEFT"
	| RIGHT -> "RIGHT"

let action_of_string str = match str with
	| "LEFT"	-> LEFT
	| "RIGHT"	-> RIGHT
	| _			-> raise (Failure "Error: action_of_string: invalid string")


let rec print_list = function
	| [] -> ()
	| head::[] -> print_string (head^" ")
	| head::tail ->
		print_string (head^", ");
		print_list tail

let print_field_list str liste =
	print_string (str^": [ ");
	print_list liste;
	print_endline "]"

let print_stuff alphabet states initial finals transitions =
	print_field_list "Alphabet" alphabet;
	print_field_list "States" states;
	print_endline ("Initial : "^initial);
	print_field_list "Finals" finals;
	let rec print_transitions liste = match liste with
		| [] -> ()
		| head::tail ->
			print_endline ("("^(get_name head)^", "^(get_read head)^
			") -> ("^(get_to_state head)^", "^(get_write head)^", "^
			(string_of_action (get_action head))^")");
			print_transitions tail
	in print_transitions transitions

(* TODO a factoriser *)
let rec get_new_state current_state read liste = match liste with
	| [] -> raise (Failure ("No transition for this <character> in this state"))
	| head::tail ->
		if current_state = (get_name head) && read = (get_read head) then
			get_to_state head
		else
			get_new_state current_state read tail


let rec get_new_write current_state read liste = match liste with
	| [] -> raise (Failure "No transition for this <character> in this state")
	| head::tail ->
		if current_state = (get_name head) && read = (get_read head) then
			get_write head
		else
			get_new_write current_state read tail


let rec get_new_action current_state read liste = match liste with
	| [] -> raise (Failure "No transition for this <character> in this state")
	| head::tail ->
		if current_state = (get_name head) && read = (get_read head) then
			get_action head
		else
			get_new_action current_state read tail
(* end *)


let print_stack i stack =
	print_char '[';
	let rec print_elems current_elem head size stack =
		if current_elem = size then
			print_char ']'
		else if current_elem = head then
begin
			if fcolor = ref 0 then
				print_char '<'
			else
				print_string "\027[31m";
			print_string stack.(current_elem);
			if fcolor = ref 0 then
				print_char '>'
			else
				print_string "\027[0m";
			print_elems (current_elem + 1) head size stack
end
		else
begin
			print_string stack.(current_elem);
			print_elems (current_elem + 1) head size stack
end
	in print_elems 0 i (Array.length stack) stack


let print_action current_state read new_state write action =
	print_endline ("("^current_state^", "^read^") -> ("^new_state^", "^
	write^", "^(string_of_action action)^")")

let rec not_final state liste = match liste with
	| [] -> true
	| head::tail ->
		if state = head then
			false
		else
			not_final state tail

let new_pos head action = match action with
	| LEFT ->
		if head = 0 then
			0
		else
			head - 1
	| RIGHT -> head + 1

let new_stack write stack head_pos action blank =
	stack.(head_pos) <- write;
	let change_size stack head_pos action blank = match action with
		| LEFT ->
			if head_pos = 0 then
				Array.append [|blank|] stack
			else
				stack
		| RIGHT ->
			if head_pos = (Array.length stack) - 1 then
				Array.append stack [|blank|]
			else
				stack
	in change_size stack head_pos action blank

let rec turing_machine head_pos current_state stack blank finals transitions =
	n_states := !n_states + 1;
	print_stack head_pos stack;
	if (not_final current_state finals) then
	begin
		let new_state = (get_new_state current_state stack.(head_pos) transitions) in
		let write = (get_new_write current_state stack.(head_pos) transitions) in
		let action = (get_new_action current_state stack.(head_pos) transitions) in
		print_action current_state (stack.(head_pos)) new_state write action;
		turing_machine (new_pos head_pos action) new_state(new_stack write stack head_pos action blank) blank finals transitions
	end
	else
		print_char '\n'

let init_tab s blank =
	let rec loop i n s tab =
		if i = n then
			tab
		else
			loop (i + 1) n s (Array.append tab [| Char.escaped( String.get s i ) |])
	in Array.append ( loop 0 ( String.length s ) s [||] )
	( Array.make 10 blank )

let print_line () =
	print_endline "********************************************************************************"
let print_empty_line () =
	print_endline "*                                                                              *"
let print_name (name) =
	print_char('*');
	let half = (80 - 2 - (String.length name)) / 2 in
	let modulo = (80 - 2 - (String.length name)) mod 2 in
	let rec print_space (index) = match index with
		| 0 -> ()
		| _ -> print_char(' '); print_space (index - 1) in
	print_space half;
	print_string name;
	print_space half;
	if modulo = 1 then
		print_char(' ');
	print_endline("*")

let begin_turing_machine name alphabet blank states initial finals transitions input =
	print_line();
	print_empty_line();
	print_name name;
	print_empty_line();
	print_line();
	print_stuff alphabet states initial finals transitions;
	print_line();
	turing_machine 0 initial (init_tab input blank) blank finals transitions

let size_list list =
	let rec loop list i = match list with
		| [] -> i
		| head::tail -> loop tail (i + 1)
	in loop list 0

let myindex list i =
	let rec loop list i index = match list with
		| [] -> raise (Failure "No element with this index")
		| head::tail ->
	  		if i = index then
				head
			else
				loop tail (i + 1) index
	in loop list 0 i

let get_transitions name read to_state write action list size =
	let rec loop name read to_state write action list i size =
		if i = size then
			list
		else
		      	loop name read to_state write action
			((name, (myindex read i), (myindex to_state i), (myindex write i),
			(action_of_string (myindex action i)))::list)
			(i + 1) size
	in loop name read to_state write action list 0 size

let rec is_not_final state finals = match finals with
	| [] -> true
	| head::tail ->
		if head = state then false
		else is_not_final state tail

open Core.Std

let rec find_string_in_list liste str = match liste with
	| [] -> 0
	| head::tail -> if head = str then
						1
					else
						find_string_in_list tail str

let func_transition trans list_ret transition finals =
	if is_not_final transition finals then
begin
	let open Yojson.Basic.Util in
	let transitions3	= trans |> member transition |> to_list in
	let read = List.map transitions3 ~f:(fun json -> member "read" json |> to_string) in
	let to_state = List.map transitions3 ~f:(fun json -> member "to_state" json |> to_string) in
	let write = List.map transitions3 ~f:(fun json -> member "write" json |> to_string) in
	let action = List.map transitions3 ~f:(fun json -> member "action" json |> to_string) in


	get_transitions transition read to_state write action list_ret (size_list to_state)
end
	else
		list_ret

let rec func_transitions trans states finals list_ret = match states with
	| [] -> list_ret
	| head::tail ->
		func_transitions trans tail finals (func_transition trans list_ret head finals)

(* let check_name (name) =
	if (String.compare name "") = 0 then
		print_endline "Error: name is empty."
	() *)

(* needs a sorted list *)
let rec find_duplicates = function
	| [] -> ()
	| a::[] -> ()
	| a::b::tail -> if String.compare a b <> 0 then
						find_duplicates ([b]@tail)
					else
						raise (Failure ("In : "^a^" has a duplicate in the field"))

let sort_and_remove_duplicates l = 
  let sl = List.sort compare l in
  let rec go l acc = match l with
    | [] -> List.rev acc
    | [x] -> List.rev (x::acc) 
    | (x1::x2::xs) -> 
      if x1 = x2
      then go (x2::xs) acc
      else go (x2::xs) (x1::acc)
  in go sl []

let rec explode str length index acc =
	if index < length then
		explode str length (index + 1) (acc@[(Char.to_string (String.get str index))])
	else
		acc

let rec find_letter_in alphabet letter = match alphabet with
	| [] -> false
	| a::lst ->	if (String.compare letter a) = 0 then
					true
				else
					find_letter_in lst letter

let rec check_letters_in_alphabet = function
	| [] -> ()
	| head::tail -> if String.length head = 1 then
						check_letters_in_alphabet tail
					else
						raise (Failure ("In alphabet, value "^head^" is not a single character"))

let check_blank alphabet blank =
	if String.length blank <> 1 then
		raise (Failure ("In blank, value \""^blank^"\" is not a single character"));
	if find_letter_in alphabet blank = false then
		raise (Failure ("In blank, value \""^blank^"\" is not in alphabet"))

let check_states states =
	find_duplicates (List.sort String.compare states)

let check_initial states initial =
	if find_letter_in states initial = false then
		raise (Failure ("In initial, value \""^initial^"\" is not in states"))

let rec check_finals states finals = match finals with
	| [] -> ()
	| final::lst -> 
				if find_letter_in states final = false then
					raise (Failure ("In finals, value \""^final^"\" is not in states"))
				else
					check_finals states lst

let main () = 
	let	json = Yojson.Basic.from_file (List.nth_exn !argv 0) in
	let open Yojson.Basic.Util in

	let name 			= json |> member "name" 		|> to_string in

	(* Alphabet *)
	let alphabet		= json |> member "alphabet" 	|> to_list |> filter_string in
	check_letters_in_alphabet(alphabet);
	find_duplicates (List.sort String.compare alphabet);

	(* Blank *)
	let blank			= json |> member "blank" 		|> to_string in
	check_blank alphabet blank;

	(* States *)
	let states			= json |> member "states" 		|> to_list |> filter_string in
	check_states states;

	(* Initials *)
	let initial			= json |> member "initial" 		|> to_string in
	check_initial states initial;

	(* Finals *)
	let finals			= json |> member "finals" 		|> to_list |> filter_string in
	check_finals states finals;

	(* Transitions *)
	let transitions		= json |> member "transitions" in
	let transitions_assoc = transitions |> to_assoc in
	let rec transitions_name transitions acc = match transitions with
	| [] -> acc
	| tupple::lst ->  	print_endline(get_transition_name tupple);
						transitions_name lst acc@[(get_transition_name tupple)]
	in
	let transition_list = transitions_name transitions_assoc [] in
	(* check_transition_list transition_list *)
	


	let transitions2 = (func_transitions transitions states finals []) in

	(* Input *)
	let input = List.nth_exn !argv 1 in
	let rec check_input = function
		| [] -> print_endline "The input is correct"
		| a::lst -> if a <> blank && find_letter_in alphabet a then
						check_input lst
					else
						raise (Failure ("Invalid input character "^a))
	in check_input (explode input (String.length input) 0 []);

(* 	List.iter print_endline transition_list; *)
	let rec print_transition_list transition_list = match transition_list with
	| a::lst -> print_endline a; print_transition_list lst
	| [] -> ()
	in print_transition_list transition_list;
	find_duplicates (List.sort String.compare transition_list);
	
	

(* 	let number_of_states = List.length states in

	let rec transitions_name_list list newlist = match list with
		| [] -> sort_and_remove_duplicates newlist
		| head::tail -> transitions_name_list tail newlist@[(get_name head)] in

	let states_in_transitions = transitions_name_list transitions2 [] in
	let number_of_states_in_transitions = List.length states_in_transitions in

	let rec transition_check transitions = match transitions with
	| [] -> ()
	| a -> List.iter to_string (to_assoc a) 
	in transition_check transitions; *)

	

	



(* Printf.printf "mytest: [ %s ]\n" (String.concat ~sep:", " mytest); *)

(* 	
	print_int(number_of_states); print_endline(" -> number_of_states");
	print_int(number_of_states_in_transitions); print_endline(" -> number_of_states_in_transitions"); *)


(* 	let rec findDuplicate l = 
        match l with 
         | []                -> [] 
         | x :: []          -> x :: [] 
         | x :: y :: rest -> 
               if x = y then deleteDuplicate (y :: rest) 
               else x :: deleteDuplicate (y :: rest)  *)

	(* Debug Json *)
	(* let rec print_transitions list = match list with
	| [] -> print_char '\n'
	| head::tail ->
		print_string "name : ";
		print_endline (get_name head);
		print_string "read :";
		print_endline (get_read head);
		print_string "to_state :";
		print_endline (get_to_state head);
		print_string "write :";
		print_endline (get_write head);
		print_string "action :";
		print_endline (string_of_action (get_action head));
		print_transitions tail in
	print_endline (name); *)
(* 	Printf.printf "Input: [ %s ]\n" (String.concat ~sep:", " (String.to_list input)); *)
(* 	Printf.printf "Alphabet: [ %s ]\n" (String.concat ~sep:", " alphabet); *)
	(* Printf.printf "Blank: %s\n" blank;
	Printf.printf "States: [ %s ]\n" (String.concat ~sep:", " states);
	Printf.printf "Initial: %s\n" initial;
	Printf.printf "Finals: [ %s ]\n" (String.concat ~sep:", " finals);
	print_transitions transitions2; *)

	
	begin_turing_machine name alphabet blank states initial finals transitions2 input;
	printf "number of step: ";
	print_int !n_states;
	print_endline " ";
	(* print_transitions transitions2; *)
	(* print_endline (List.nth_exn !argv 1); *)
	print_endline "Done"

let color () = 
	fcolor := 1

let check_cmdline () = 
	if (argc <> ref 2) then
		begin
			print_endline("Error: ft_turing needs 2 arguments only");
			exit(0)
		end
		else if (Sys.file_exists (List.nth_exn !argv 0)) = `No then
		begin
			print_endline ("Error: file does not exist");
		end

let () =
	try
		let specs = 
		[
		  ( 'h', "help",    Some Help.help, None);
		  ( 'c', "color",	Some color, None)
		] in
		let init_args (current_argv) =
		argc := !argc + 1;
		argv := List.rev (current_argv::!argv);
		() in
		Getopt.parse_cmdline specs init_args;
		check_cmdline();
		main ();
	with 
		(* Yojson Exceptions *)
		| Yojson.Json_error explanation -> print_endline ("Json_error:\n"^explanation);
		(* | Yojson.Basic.Util.Type_error explanation -> print_endline ("Type_error\n"); *)
		(* | Yojson.Basic.Util.Undefined explanation -> print_endline ("Undefined\n"); *)

		(* Core Exceptions *)
		| Failure explanation -> print_endline (" Failure: "^explanation);
		| Getopt.Error explanation -> print_endline ("Error:\n"^explanation);
		| Match_failure explanation -> print_endline ("Match_failure:\n");
		| Assert_failure explanation -> print_endline ("Assert_failure:\n");
		| Invalid_argument explanation -> print_endline ("Invalid_argument:\n"^explanation);
		| Division_by_zero -> print_endline ("Division_by_zero");
		| Not_found -> print_endline ("Not_found");
		| _ -> print_endline (" Error: ???");
	Printf.printf ""

